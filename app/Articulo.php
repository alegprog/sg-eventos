<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Articulo extends Model
{
    use SoftDeletes;

    public function evento()
    {
        return $this->belongsTo('App\Evento','evento_id','id_evento');
    }

    public function persona()
    {
        return $this->belongsTo('App\Persona','persona_id','id_persona');
    }

    public function coautor_principal()
    {
        return $this->belongsTo('App\Persona','coautor_1_id','id_persona');
    }

    public function coautor_segundario()
    {
        return $this->belongsTo('App\Persona','coautor_2_id','id_persona');
    }
}
