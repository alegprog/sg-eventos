<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Requerimiento extends Model
{
    use SoftDeletes;

    public function evento()
    {
        return $this->belongsTo('App\Evento','evento_id','id_evento');
    }

    public function tipo_requerimiento()
    {
        return $this->belongsTo('App\TipoRequerimiento','tipo_requerimiento_id','id_tipo_requerimiento');
    }

    public function responsable()
    {
        return $this->belongsTo('App\Responsable','responsable_id','id_responsable');
    }
}
