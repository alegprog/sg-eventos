<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoEvento extends Model
{
   use SoftDeletes;
   protected $table='tipo_eventos';

   public function eventos()
   {
       return $this->hasMany('App\Evento');
   }

   public function getBloqueadoAttribute(){
     if(!$this->deleted_at==Null){
       return false;
     }else{
       return true;
     }
   }


}
