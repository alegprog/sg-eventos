<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Persona extends Model
{
    use SoftDeletes;

    public function documento(){
      return $this->belongsTo('App\TipoIdentificacion','tipo_identificacion_id','id_tipo_identificacion');
    }

    public function nacionalidad(){
      return $this->belongsTo('App\Nacionalidad','nacionalidad_id','id_nacionalidad');
    }
}
