<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dependencia extends Model
{
    use SoftDeletes;

    public function eventos()
    {
        return $this->hasMany('App\Evento');
    }

    public function firma_principal()
    {
        return $this->belongsTo('App\Persona','firma_1_id','id_persona');
    }

    public function firma_segundaria()
    {
        return $this->belongsTo('App\Persona','firma_2_id','id_persona');
    }
}
