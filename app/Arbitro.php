<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Arbitro extends Model
{
    use SoftDeletes;

    public function tipo_arbitro()
    {
        return $this->belongsTo('App\TipoArbitro','tipo_arbitro_id','id_tipo_arbitro');
    }

    public function evento()
    {
        return $this->belongsTo('App\Evento','evento_id','id_evento');
    }

    public function modalidad()
    {
        return $this->belongsTo('App\Modalidad','modalidad_id','id_modalidad');
    }

    public function persona()
    {
        return $this->belongsTo('App\Persona','persona_id','id_persona');
    }
}
