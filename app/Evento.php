<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Date\Date;
use Carbon\Carbon;

class Evento extends Model
{
   use SoftDeletes;

    protected $dates = ['fecha_inicio_evento',];

   public function tipo_evento()
   {
       return $this->belongsTo('App\TipoEvento','tipo_evento_id','id_tipo_evento');
   }

   public function lugar()
   {
       return $this->belongsTo('App\Lugar','lugar_id','id_lugar');
   }

   public function dependencia()
   {
       return $this->belongsTo('App\Dependencia','dependencia_id','id_dependencia');
   }

   public function setFechaFinalEventoAttribute($value)
   {
        $this->attributes['fecha_final_evento'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
   }

   public function getFechaFinalEventoAttribute($value)
   {
        return Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
   }

   public function setFechaInicioEventoAttribute($value)
   {
        $this->attributes['fecha_inicio_evento'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
   }

   public function getFechaInicioAttribute()
   {
        return new Date($this->fecha_inicio_evento);
   }

   public function getFechaInicioEventoAttribute($value)
   {
        return Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
   }



   public function setHoraFinalAttribute($value)
   {
        if(!empty($value)){
          $arrayHoraFinal=explode(' ',$value);
          $arrayHora=explode(':',$arrayHoraFinal[0]);
          if($arrayHoraFinal[1]=='PM'){
            $hora=($arrayHora[0]+12).':'.$arrayHora[1];
          }else{
            $hora=$arrayHoraFinal[0];
          }
        }else{
          $hora=$value;
        }

        $this->attributes['hora_final_evento'] = $hora;
   }


   public function getHoraFinalAttribute($value)
   {
        $arrayHora=explode(':',$value);
        if($arrayHora[0]>12){
          $hora=($arrayHora[0]-12).':'.$arrayHora[1].' PM';
        }elseif($arrayHora[0]==12){
          $hora=$arrayHora[0].':'.$arrayHora[1].' M';
        }else{
          $hora=$arrayHora[0].':'.$arrayHora[1].' AM';
        }

        return $hora;
   }



   public function setHoraInicioEventoAttribute($value)
   {
     if(!empty($value)){
       $arrayHoraInicio=explode(' ',$value);
       $arrayHora=explode(':',$arrayHoraInicio[0]);
       if($arrayHoraInicio[1]=='PM'){
         $hora=($arrayHora[0]+12).':'.$arrayHora[1];
       }else{
         $hora=$arrayHoraInicio[0];
       }
     }else{
       $hora=$value;
     }

     $this->attributes['hora_inicio_evento'] = $hora;
   }

   public function getHoraFinalEventoAttribute($value)
   {
        $arrayHora=explode(':',$value);
        if($arrayHora[0]>12){
          $hora=($arrayHora[0]-12).':'.$arrayHora[1].' PM';
        }elseif($arrayHora[0]==12){
          $hora=$arrayHora[0].':'.$arrayHora[1].' M';
        }else{
          $hora=$arrayHora[0].':'.$arrayHora[1].' AM';
        }

        return $hora;
   }
}
