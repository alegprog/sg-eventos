<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoIdentificacion extends Model
{
    use SoftDeletes;
    
    protected $table='tipo_identificaciones';

    public function personas()
    {
        return $this->hasMany('App\Persona');
    }
}
