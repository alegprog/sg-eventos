<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('login', ['as' =>'login', 'uses' => 'DashboardController@getLogin']);
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'DashboardController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'DashboardController@getLogout']);

Route::get('registro', ['as' =>'registro.create', 'uses' => 'DashboardController@getRegistro']);
Route::post('registro', ['as' =>'registro.store', 'uses' => 'DashboardController@postRegistro']);


Route::group(['middleware' => 'auth'], function () {

      Route::get('dashboard', [
          'as' => 'dashboard', 'uses' => 'DashboardController@index'
      ]);

      Route::get('dashboard/cambio-clave', [
          'as' => 'dashboard.cambio_clave', 'uses' => 'DashboardController@getCambioClave'
      ]);

      Route::post('dashboard/cambio-clave', [
          'as' => 'dashboard.cambio_clave', 'uses' => 'DashboardController@postCambioClave'
      ]);

      Route::get('dashboard/ajuste', [
          'as' => 'dashboard.ajuste', 'uses' => 'DashboardController@getAjuste'
      ]);

      Route::post('dashboard/ajuste', [
          'as' => 'dashboard.ajuste', 'uses' => 'DashboardController@postAjuste'
      ]);

      /***** Inicio Tipo Evento *****/

      Route::get('dashboard/tipo-evento', [
          'as' => 'tipo_evento.listado', 'uses' => 'TipoEventoController@listado'
      ]);

      Route::get('dashboard/tipo-evento/agregar', [
          'as' => 'tipo_evento.crear', 'uses' => 'TipoEventoController@crear'
      ]);

      Route::post('dashboard/tipo-evento/agregar', [
          'as' => 'tipo_evento.guardar', 'uses' => 'TipoEventoController@guardar'
      ]);

      Route::get('dashboard/tipo-evento/editar/{id}', [
          'as' => 'tipo_evento.editar', 'uses' => 'TipoEventoController@editar'
      ]);

      Route::post('dashboard/tipo-evento/actualizar/{id}', [
          'as' => 'tipo_evento.actualizar', 'uses' => 'TipoEventoController@actualizar'
      ]);

      Route::get('dashboard/tipo-evento/eliminar/{id}', [
          'as' => 'tipo_evento.eliminar', 'uses' => 'TipoEventoController@eliminar'
      ]);

      Route::get('dashboard/tipo-evento/activar/{id}', [
          'as' => 'tipo_evento.activar', 'uses' => 'TipoEventoController@activar'
      ]);

      /***** Fin Tipo Evento *****/

      /* ----------------------------------------------------------- */

      /***** Inicio Tipo Invitado *****/

      Route::get('dashboard/tipo-invitado', [
          'as' => 'tipo_invitado.listado', 'uses' => 'TipoInvitadoController@listado'
      ]);

      Route::get('dashboard/tipo-invitado/agregar', [
          'as' => 'tipo_invitado.crear', 'uses' => 'TipoInvitadoController@crear'
      ]);

      Route::post('dashboard/tipo-invitado/agregar', [
          'as' => 'tipo_invitado.guardar', 'uses' => 'TipoInvitadoController@guardar'
      ]);

      Route::get('dashboard/tipo-invitado/editar/{id}', [
          'as' => 'tipo_invitado.editar', 'uses' => 'TipoInvitadoController@editar'
      ]);

      Route::post('dashboard/tipo-invitado/actualizar/{id}', [
          'as' => 'tipo_invitado.actualizar', 'uses' => 'TipoInvitadoController@actualizar'
      ]);

      Route::get('dashboard/tipo-invitado/eliminar/{id}', [
          'as' => 'tipo_invitado.eliminar', 'uses' => 'TipoInvitadoController@eliminar'
      ]);

      Route::get('dashboard/tipo-invitado/activar/{id}', [
          'as' => 'tipo_invitado.activar', 'uses' => 'TipoInvitadoController@activar'
      ]);

      /***** Fin Tipo Invitado *****/

      /* ----------------------------------------------------------- */


      /* ----------------------------------------------------------- */

      /***** Inicio Nacionalidad *****/

      Route::get('dashboard/nacionalidad', [
          'as' => 'nacionalidad.listado', 'uses' => 'NacionalidadController@listado'
      ]);

      Route::get('dashboard/nacionalidad/agregar', [
          'as' => 'nacionalidad.crear', 'uses' => 'NacionalidadController@crear'
      ]);

      Route::post('dashboard/nacionalidad/agregar', [
          'as' => 'nacionalidad.guardar', 'uses' => 'NacionalidadController@guardar'
      ]);

      Route::get('dashboard/nacionalidad/editar/{id}', [
          'as' => 'nacionalidad.editar', 'uses' => 'NacionalidadController@editar'
      ]);

      Route::post('dashboard/nacionalidad/actualizar/{id}', [
          'as' => 'nacionalidad.actualizar', 'uses' => 'NacionalidadController@actualizar'
      ]);

      Route::get('dashboard/nacionalidad/eliminar/{id}', [
          'as' => 'nacionalidad.eliminar', 'uses' => 'NacionalidadController@eliminar'
      ]);

      Route::get('dashboard/nacionalidad/activar/{id}', [
          'as' => 'nacionalidad.activar', 'uses' => 'NacionalidadController@activar'
      ]);

      /***** Fin Tipo Nacionalidad *****/

      /* ----------------------------------------------------------- */


      /***** Inicio Tipo Arbitro *****/

      Route::get('dashboard/tipo_arbitro', [
          'as' => 'tipo_arbitro.listado', 'uses' => 'TipoArbitroController@listado'
      ]);

      Route::get('dashboard/tipo-arbitro/agregar', [
          'as' => 'tipo_arbitro.crear', 'uses' => 'TipoArbitroController@crear'
      ]);

      Route::post('dashboard/tipo-arbitro/agregar', [
          'as' => 'tipo_arbitro.guardar', 'uses' => 'TipoArbitroController@guardar'
      ]);

      Route::get('dashboard/tipo-arbitro/editar/{id}', [
          'as' => 'tipo_arbitro.editar', 'uses' => 'TipoArbitroController@editar'
      ]);

      Route::post('dashboard/tipo-arbitro/actualizar/{id}', [
          'as' => 'tipo_arbitro.actualizar', 'uses' => 'TipoArbitroController@actualizar'
      ]);

      Route::get('dashboard/tipo-arbitro/eliminar/{id}', [
          'as' => 'tipo_arbitro.eliminar', 'uses' => 'TipoArbitroController@eliminar'
      ]);

      Route::get('dashboard/tipo-arbitro/activar/{id}', [
          'as' => 'tipo_arbitro.activar', 'uses' => 'TipoArbitroController@activar'
      ]);

      /***** Fin Tipo Arbitro *****/

      /* ----------------------------------------------------------- */

      /***** Inicio Tipo Requerimiento *****/

      Route::get('dashboard/tipo_requerimiento', [
          'as' => 'tipo_requerimiento.listado', 'uses' => 'TipoRequerimientoController@listado'
      ]);

      Route::get('dashboard/tipo_requerimiento/agregar', [
          'as' => 'tipo_requerimiento.crear', 'uses' => 'TipoRequerimientoController@crear'
      ]);

      Route::post('dashboard/tipo_requerimiento/agregar', [
          'as' => 'tipo_requerimiento.guardar', 'uses' => 'TipoRequerimientoController@guardar'
      ]);

      Route::get('dashboard/tipo_requerimiento/editar/{id}', [
          'as' => 'tipo_requerimiento.editar', 'uses' => 'TipoRequerimientoController@editar'
      ]);

      Route::post('dashboard/tipo_requerimiento/actualizar/{id}', [
          'as' => 'tipo_requerimiento.actualizar', 'uses' => 'TipoRequerimientoController@actualizar'
      ]);

      Route::get('dashboard/tipo_requerimiento/eliminar/{id}', [
          'as' => 'tipo_requerimiento.eliminar', 'uses' => 'TipoRequerimientoController@eliminar'
      ]);

      Route::get('dashboard/tipo_requerimiento/activar/{id}', [
          'as' => 'tipo_requerimiento.activar', 'uses' => 'TipoRequerimientoController@activar'
      ]);

      /***** Fin Tipo Requerimiento *****/

      /* ----------------------------------------------------------- */

      /***** Inicio Tipo Identificacion *****/

      Route::get('dashboard/tipo_identificacion', [
          'as' => 'tipo_identificacion.listado', 'uses' => 'TipoIdentificacionController@listado'
      ]);

      Route::get('dashboard/tipo_identificacion/agregar', [
          'as' => 'tipo_identificacion.crear', 'uses' => 'TipoIdentificacionController@crear'
      ]);

      Route::post('dashboard/tipo_identificacion/agregar', [
          'as' => 'tipo_identificacion.guardar', 'uses' => 'TipoIdentificacionController@guardar'
      ]);

      Route::get('dashboard/tipo_identificacion/editar/{id}', [
          'as' => 'tipo_identificacion.editar', 'uses' => 'TipoIdentificacionController@editar'
      ]);

      Route::post('dashboard/tipo_identificacion/actualizar/{id}', [
          'as' => 'tipo_identificacion.actualizar', 'uses' => 'TipoIdentificacionController@actualizar'
      ]);

      Route::get('dashboard/tipo_identificacion/eliminar/{id}', [
          'as' => 'tipo_identificacion.eliminar', 'uses' => 'TipoIdentificacionController@eliminar'
      ]);

      Route::get('dashboard/tipo_identificacion/activar/{id}', [
          'as' => 'tipo_identificacion.activar', 'uses' => 'TipoIdentificacionController@activar'
      ]);

      /***** Fin Tipo Identificacion *****/

      /* ----------------------------------------------------------- */

      /***** Inicio Responsable *****/

      Route::get('dashboard/responsable', [
          'as' => 'responsable.listado', 'uses' => 'ResponsableController@listado'
      ]);

      Route::get('dashboard/responsable/agregar', [
          'as' => 'responsable.crear', 'uses' => 'ResponsableController@crear'
      ]);

      Route::post('dashboard/responsable/agregar', [
          'as' => 'responsable.guardar', 'uses' => 'ResponsableController@guardar'
      ]);

      Route::get('dashboard/responsable/editar/{id}', [
          'as' => 'responsable.editar', 'uses' => 'ResponsableController@editar'
      ]);

      Route::post('dashboard/responsable/actualizar/{id}', [
          'as' => 'responsable.actualizar', 'uses' => 'ResponsableController@actualizar'
      ]);

      Route::get('dashboard/responsable/eliminar/{id}', [
          'as' => 'responsable.eliminar', 'uses' => 'ResponsableController@eliminar'
      ]);

      Route::get('dashboard/responsable/activar/{id}', [
          'as' => 'responsable.activar', 'uses' => 'ResponsableController@activar'
      ]);

      /***** Fin Tipo Responsable *****/


      /* ----------------------------------------------------------- */

      /***** Inicio Modalidad *****/

      Route::get('dashboard/modalidad', [
          'as' => 'modalidad.listado', 'uses' => 'ModalidadController@listado'
      ]);

      Route::get('dashboard/modalidad/agregar', [
          'as' => 'modalidad.crear', 'uses' => 'ModalidadController@crear'
      ]);

      Route::post('dashboard/modalidad/agregar', [
          'as' => 'modalidad.guardar', 'uses' => 'ModalidadController@guardar'
      ]);

      Route::get('dashboard/modalidad/editar/{id}', [
          'as' => 'modalidad.editar', 'uses' => 'ModalidadController@editar'
      ]);

      Route::post('dashboard/modalidad/actualizar/{id}', [
          'as' => 'modalidad.actualizar', 'uses' => 'ModalidadController@actualizar'
      ]);

      Route::get('dashboard/modalidad/eliminar/{id}', [
          'as' => 'modalidad.eliminar', 'uses' => 'ModalidadController@eliminar'
      ]);

      Route::get('dashboard/modalidad/activar/{id}', [
          'as' => 'modalidad.activar', 'uses' => 'ModalidadController@activar'
      ]);

      /***** Fin Tipo Modalidad *****/

      /* ----------------------------------------------------------- */


      /* ----------------------------------------------------------- */

      /***** Inicio Dependencia *****/

      Route::get('dashboard/dependencia', [
          'as' => 'dependencia.listado', 'uses' => 'DependenciaController@listado'
      ]);

      Route::get('dashboard/dependencia/agregar', [
          'as' => 'dependencia.crear', 'uses' => 'DependenciaController@crear'
      ]);

      Route::post('dashboard/dependencia/agregar', [
          'as' => 'dependencia.guardar', 'uses' => 'DependenciaController@guardar'
      ]);

      Route::get('dashboard/dependencia/editar/{id}', [
          'as' => 'dependencia.editar', 'uses' => 'DependenciaController@editar'
      ]);

      Route::post('dashboard/dependencia/actualizar/{id}', [
          'as' => 'dependencia.actualizar', 'uses' => 'DependenciaController@actualizar'
      ]);

      Route::get('dashboard/dependencia/eliminar/{id}', [
          'as' => 'dependencia.eliminar', 'uses' => 'DependenciaController@eliminar'
      ]);

      Route::get('dashboard/dependencia/activar/{id}', [
          'as' => 'dependencia.activar', 'uses' => 'DependenciaController@activar'
      ]);

      /***** Fin Dependencia *****/

      /* ----------------------------------------------------------- */


      /***** Inicio Lugar *****/

      Route::get('dashboard/lugar', [
          'as' => 'lugar.listado', 'uses' => 'LugarController@listado'
      ]);

      Route::get('dashboard/lugar/agregar', [
          'as' => 'lugar.crear', 'uses' => 'LugarController@crear'
      ]);

      Route::post('dashboard/lugar/agregar', [
          'as' => 'lugar.guardar', 'uses' => 'LugarController@guardar'
      ]);

      Route::get('dashboard/lugar/editar/{id}', [
          'as' => 'lugar.editar', 'uses' => 'LugarController@editar'
      ]);

      Route::post('dashboard/lugar/actualizar/{id}', [
          'as' => 'lugar.actualizar', 'uses' => 'LugarController@actualizar'
      ]);

      Route::get('dashboard/lugar/eliminar/{id}', [
          'as' => 'lugar.eliminar', 'uses' => 'LugarController@eliminar'
      ]);

      Route::get('dashboard/lugar/activar/{id}', [
          'as' => 'lugar.activar', 'uses' => 'LugarController@activar'
      ]);

      /***** Fin Lugar *****/

      /* ----------------------------------------------------------- */

      /***** Inicio Estatu *****/

      Route::get('dashboard/estatu', [
          'as' => 'estatu.listado', 'uses' => 'EstatuController@listado'
      ]);

      Route::get('dashboard/estatu/agregar', [
          'as' => 'estatu.crear', 'uses' => 'EstatuController@crear'
      ]);

      Route::post('dashboard/estatu/agregar', [
          'as' => 'estatu.guardar', 'uses' => 'EstatuController@guardar'
      ]);

      Route::get('dashboard/estatu/editar/{id}', [
          'as' => 'estatu.editar', 'uses' => 'EstatuController@editar'
      ]);

      Route::post('dashboard/estatu/actualizar/{id}', [
          'as' => 'estatu.actualizar', 'uses' => 'EstatuController@actualizar'
      ]);

      Route::get('dashboard/estatu/eliminar/{id}', [
          'as' => 'estatu.eliminar', 'uses' => 'EstatuController@eliminar'
      ]);

      Route::get('dashboard/estatu/activar/{id}', [
          'as' => 'estatu.activar', 'uses' => 'EstatuController@activar'
      ]);

      /***** Fin Lugar *****/

      /* ----------------------------------------------------------- */

      /***** Inicio Persona *****/

      Route::get('dashboard/persona', [
          'as' => 'persona.listado', 'uses' => 'PersonaController@listado'
      ]);

      Route::get('dashboard/persona/agregar', [
          'as' => 'persona.crear', 'uses' => 'PersonaController@crear'
      ]);

      Route::post('dashboard/pesona/agregar', [
          'as' => 'persona.guardar', 'uses' => 'PersonaController@guardar'
      ]);

      Route::get('dashboard/persona/editar/{id}', [
          'as' => 'persona.editar', 'uses' => 'PersonaController@editar'
      ]);

      Route::get('dashboard/persona/detalle/{id}', [
          'as' => 'persona.detalle', 'uses' => 'PersonaController@detalle'
      ]);

      Route::post('dashboard/persona/actualizar/{id}', [
          'as' => 'persona.actualizar', 'uses' => 'PersonaController@actualizar'
      ]);

      Route::get('dashboard/persona/eliminar/{id}', [
          'as' => 'persona.eliminar', 'uses' => 'PersonaController@eliminar'
      ]);

      Route::get('dashboard/persona/activar/{id}', [
          'as' => 'persona.activar', 'uses' => 'PersonaController@activar'
      ]);

      /***** Fin Persona *****/

      /* ----------------------------------------------------------- */

      /***** Inicio Usuario *****/

      Route::get('dashboard/usuario', [
          'as' => 'usuario.listado', 'uses' => 'UsuarioController@listado'
      ]);

      Route::get('dashboard/usuario/agregar', [
          'as' => 'usuario.crear', 'uses' => 'UsuarioController@crear'
      ]);

      Route::post('dashboard/usuario/agregar', [
          'as' => 'usuario.guardar', 'uses' => 'UsuarioController@guardar'
      ]);

      Route::get('dashboard/usuario/editar/{id}', [
          'as' => 'usuario.editar', 'uses' => 'UsuarioController@editar'
      ]);

      Route::get('dashboard/usuario/detalle/{id}', [
          'as' => 'usuario.detalle', 'uses' => 'UsuarioController@detalle'
      ]);

      Route::post('dashboard/usuario/actualizar/{id}', [
          'as' => 'usuario.actualizar', 'uses' => 'UsuarioController@actualizar'
      ]);

      Route::get('dashboard/usuario/eliminar/{id}', [
          'as' => 'usuario.eliminar', 'uses' => 'UsuarioController@eliminar'
      ]);

      Route::get('dashboard/usuario/activar/{id}', [
          'as' => 'usuario.activar', 'uses' => 'UsuarioController@activar'
      ]);

      /***** Fin Usuario *****/

      /* ----------------------------------------------------------- */

      /***** Inicio Evento *****/

      Route::get('dashboard/evento', [
          'as' => 'evento.listado', 'uses' => 'EventoController@listado'
      ]);

      Route::get('dashboard/evento/agregar', [
          'as' => 'evento.crear', 'uses' => 'EventoController@crear'
      ]);

      Route::post('dashboard/evento/agregar', [
          'as' => 'evento.guardar', 'uses' => 'EventoController@guardar'
      ]);

      Route::get('dashboard/evento/editar/{id}', [
          'as' => 'evento.editar', 'uses' => 'EventoController@editar'
      ]);

      Route::post('dashboard/evento/actualizar/{id}', [
          'as' => 'evento.actualizar', 'uses' => 'EventoController@actualizar'
      ]);

      Route::get('dashboard/evento/eliminar/{id}', [
          'as' => 'evento.eliminar', 'uses' => 'EventoController@eliminar'
      ]);

      Route::get('dashboard/evento/detalle/{id}', [
          'as' => 'evento.detalle', 'uses' => 'EventoController@detalle'
      ]);

      Route::get('dashboard/evento/activar/{id}', [
          'as' => 'evento.activar', 'uses' => 'EventoController@activar'
      ]);

      Route::get('dashboard/evento/download/{archivo}', [
          'as' => 'evento.download', 'uses' => 'EventoController@download'
      ]);

      /***** Fin Evento *****/


      /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++*/



      /***** Inicio Invitado *****/

      Route::get('dashboard/invitado', [
          'as' => 'invitado.listado', 'uses' => 'InvitadoController@listado'
      ]);

      Route::get('dashboard/invitado/agregar', [
          'as' => 'invitado.crear', 'uses' => 'InvitadoController@crear'
      ]);

      Route::post('dashboard/invitado/agregar', [
          'as' => 'invitado.guardar', 'uses' => 'InvitadoController@guardar'
      ]);

      Route::get('dashboard/invitado/editar/{id}', [
          'as' => 'invitado.editar', 'uses' => 'InvitadoController@editar'
      ]);

      Route::post('dashboard/invitado/actualizar/{id}', [
          'as' => 'invitado.actualizar', 'uses' => 'InvitadoController@actualizar'
      ]);

      Route::get('dashboard/invitado/eliminar/{id}', [
          'as' => 'invitado.eliminar', 'uses' => 'InvitadoController@eliminar'
      ]);

      Route::get('dashboard/invitado/detalle/{id}', [
          'as' => 'invitado.detalle', 'uses' => 'InvitadoController@detalle'
      ]);

      Route::get('dashboard/invitado/activar/{id}', [
          'as' => 'invitado.activar', 'uses' => 'InvitadoController@activar'
      ]);

      Route::get('dashboard/invitado/download/{archivo}', [
          'as' => 'invitado.download', 'uses' => 'InvitadoController@download'
      ]);

      /***** Fin Invitado *****/



      /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++*/



      /***** Inicio Arbitro *****/

      Route::get('dashboard/arbitro', [
          'as' => 'arbitro.listado', 'uses' => 'ArbitroController@listado'
      ]);

      Route::get('dashboard/arbitro/agregar', [
          'as' => 'arbitro.crear', 'uses' => 'ArbitroController@crear'
      ]);

      Route::post('dashboard/arbitro/agregar', [
          'as' => 'arbitro.guardar', 'uses' => 'ArbitroController@guardar'
      ]);

      Route::get('dashboard/arbitro/editar/{id}', [
          'as' => 'arbitro.editar', 'uses' => 'ArbitroController@editar'
      ]);

      Route::post('dashboard/arbitro/actualizar/{id}', [
          'as' => 'arbitro.actualizar', 'uses' => 'ArbitroController@actualizar'
      ]);

      Route::get('dashboard/arbitro/eliminar/{id}', [
          'as' => 'arbitro.eliminar', 'uses' => 'ArbitroController@eliminar'
      ]);

      Route::get('dashboard/arbitro/detalle/{id}', [
          'as' => 'arbitro.detalle', 'uses' => 'ArbitroController@detalle'
      ]);

      Route::get('dashboard/arbitro/activar/{id}', [
          'as' => 'arbitro.activar', 'uses' => 'ArbitroController@activar'
      ]);

      /***** Fin Arbitro *****/

      /******************* ********************/

      /***** Inicio Articulo *****/

      Route::get('dashboard/articulo', [
          'as' => 'articulo.listado', 'uses' => 'ArticuloController@listado'
      ]);

      Route::get('dashboard/articulo/agregar', [
          'as' => 'articulo.crear', 'uses' => 'ArticuloController@crear'
      ]);

      Route::post('dashboard/articulo/agregar', [
          'as' => 'articulo.guardar', 'uses' => 'ArticuloController@guardar'
      ]);

      Route::get('dashboard/articulo/editar/{id}', [
          'as' => 'articulo.editar', 'uses' => 'ArticuloController@editar'
      ]);

      Route::post('dashboard/articulo/actualizar/{id}', [
          'as' => 'articulo.actualizar', 'uses' => 'ArticuloController@actualizar'
      ]);

      Route::get('dashboard/articulo/eliminar/{id}', [
          'as' => 'articulo.eliminar', 'uses' => 'ArticuloController@eliminar'
      ]);

      Route::get('dashboard/articulo/detalle/{id}', [
          'as' => 'articulo.detalle', 'uses' => 'ArticuloController@detalle'
      ]);

      Route::get('dashboard/articulo/activar/{id}', [
          'as' => 'articulo.activar', 'uses' => 'ArticuloController@activar'
      ]);

      /***** Fin Articulo *****/


      /******************* ********************/

      /***** Inicio Arbitraje *****/

      Route::get('dashboard/arbitraje', [
          'as' => 'arbitraje.listado', 'uses' => 'ArbitrajeController@listado'
      ]);

      Route::get('dashboard/arbitraje/agregar', [
          'as' => 'arbitraje.crear', 'uses' => 'ArbitrajeController@crear'
      ]);

      Route::post('dashboard/arbitraje/agregar', [
          'as' => 'arbitraje.guardar', 'uses' => 'ArbitrajeController@guardar'
      ]);

      Route::get('dashboard/arbitraje/editar/{id}', [
          'as' => 'arbitraje.editar', 'uses' => 'ArbitrajeController@editar'
      ]);

      Route::post('dashboard/arbitraje/actualizar/{id}', [
          'as' => 'arbitraje.actualizar', 'uses' => 'ArbitrajeController@actualizar'
      ]);

      Route::get('dashboard/arbitraje/eliminar/{id}', [
          'as' => 'arbitraje.eliminar', 'uses' => 'ArbitrajeController@eliminar'
      ]);

      Route::get('dashboard/arbitraje/detalle/{id}', [
          'as' => 'arbitraje.detalle', 'uses' => 'ArbitrajeController@detalle'
      ]);

      Route::get('dashboard/arbitraje/activar/{id}', [
          'as' => 'arbitraje.activar', 'uses' => 'ArbitrajeController@activar'
      ]);

      /***** Fin Arbitraje *****/


      /******************* ********************/

      /***** Inicio Requerimiento *****/

      Route::get('dashboard/requerimiento', [
          'as' => 'requerimiento.listado', 'uses' => 'RequerimientoController@listado'
      ]);

      Route::get('dashboard/requerimiento/agregar', [
          'as' => 'requerimiento.crear', 'uses' => 'RequerimientoController@crear'
      ]);

      Route::post('dashboard/requerimiento/agregar', [
          'as' => 'requerimiento.guardar', 'uses' => 'RequerimientoController@guardar'
      ]);

      Route::get('dashboard/requerimiento/editar/{id}', [
          'as' => 'requerimiento.editar', 'uses' => 'RequerimientoController@editar'
      ]);

      Route::post('dashboard/requerimiento/actualizar/{id}', [
          'as' => 'requerimiento.actualizar', 'uses' => 'RequerimientoController@actualizar'
      ]);

      Route::get('dashboard/requerimiento/eliminar/{id}', [
          'as' => 'requerimiento.eliminar', 'uses' => 'RequerimientoController@eliminar'
      ]);

      Route::get('dashboard/requerimiento/detalle/{id}', [
          'as' => 'requerimiento.detalle', 'uses' => 'RequerimientoController@detalle'
      ]);

      Route::get('dashboard/requerimiento/activar/{id}', [
          'as' => 'requerimiento.activar', 'uses' => 'RequerimientoController@activar'
      ]);

      /***** Fin Requerimiento *****/


      /***** Inicio Reporte *****/

      Route::get('dashboard/reporte/invitado', [
          'as' => 'reporte.invitado', 'uses' => 'ReporteController@invitado'
      ]);

      Route::post('dashboard/reporte/invitado', [
          'as' => 'reporte.rpt_invitado', 'uses' => 'ReporteController@reporteInvitado'
      ]);

      Route::get('dashboard/reporte/invitado-constancia', [
          'as' => 'reporte.invitado.constancia', 'uses' => 'ReporteController@invitadoConstancia'
      ]);

      Route::post('dashboard/reporte/invitado-constancia', [
          'as' => 'reporte.rpt_invitado.constancia', 'uses' => 'ReporteController@reporteInvitadoConstancia'
      ]);

      Route::get('dashboard/constancia-invitado/{id}', [
          'as' => 'reporte.constanciaInvitado', 'uses' => 'ReporteController@generarConstanciaInvitado'
      ]);

      Route::get('dashboard/certificado-invitado/{id}', [
          'as' => 'reporte.certificadoInvitado', 'uses' => 'ReporteController@generarCertificadoInvitado'
      ]);

      Route::get('dashboard/reporte/arbitro', [
          'as' => 'reporte.arbitro', 'uses' => 'ReporteController@arbitro'
      ]);

      Route::post('dashboard/reporte/arbitro', [
          'as' => 'reporte.rpt_arbitro', 'uses' => 'ReporteController@reporteArbitro'
      ]);

      Route::get('dashboard/reporte/arbitro-constancia', [
          'as' => 'reporte.arbitro.constancia', 'uses' => 'ReporteController@arbitroConstancia'
      ]);

      Route::post('dashboard/reporte/arbitro-constancia', [
          'as' => 'reporte.rpt_arbitro.constancia', 'uses' => 'ReporteController@reporteArbitroConstancia'
      ]);

      Route::get('dashboard/constancia-arbitro/{id}', [
          'as' => 'reporte.constanciaArbitro', 'uses' => 'ReporteController@generarConstanciaArbitro'
      ]);

      Route::get('dashboard/certificado-arbitro/{id}', [
          'as' => 'reporte.certificadoArbitro', 'uses' => 'ReporteController@generarCertificadoArbitro'
      ]);

      Route::get('dashboard/reporte/requerimiento', [
          'as' => 'reporte.requerimiento', 'uses' => 'ReporteController@requerimiento'
      ]);

      Route::post('dashboard/reporte/requerimiento', [
          'as' => 'reporte.rpt_requerimiento', 'uses' => 'ReporteController@reporteRequerimiento'
      ]);

      Route::get('dashboard/reporte/arbitraje', [
          'as' => 'reporte.arbitraje', 'uses' => 'ReporteController@arbitraje'
      ]);

      Route::post('dashboard/reporte/arbitraje', [
          'as' => 'reporte.rpt_arbitraje', 'uses' => 'ReporteController@reporteArbitraje'
      ]);

      Route::get('dashboard/{tipo}-invitado/pdf/{id}', [
          'as' => 'reporte.pdfConstanciaInvitado', 'uses' => 'ReporteController@pdfConstanciaInvitado'
      ]);

      /*Route::get('dashboard/{tipo}-invitado/pdf/{id}', [
          'as' => 'reporte.pdfCertificadoInvitado', 'uses' => 'ReporteController@pdfConstanciaInvitado'
      ]);*/

      Route::get('dashboard/reporte/articulo-constancia', [
          'as' => 'reporte.articulo.constancia', 'uses' => 'ReporteController@articuloConstancia'
      ]);

      Route::post('dashboard/reporte/articulo-constancia', [
          'as' => 'reporte.rpt_articulo.constancia', 'uses' => 'ReporteController@reporteArticuloConstancia'
      ]);

      Route::get('dashboard/constancia-articulo/{id}', [
          'as' => 'reporte.constanciaArticulo', 'uses' => 'ReporteController@generarConstanciaArticulo'
      ]);

      Route::get('dashboard/constancia-articulo/pdf/{id}', [
          'as' => 'reporte.pdfConstanciaInvitado', 'uses' => 'ReporteController@pdfConstanciaArticulo'
      ]);

      Route::get('dashboard/constancia-arbitro/pdf/{id}', [
          'as' => 'reporte.pdfConstanciaArbitro', 'uses' => 'ReporteController@pdfConstanciaArbitro'
      ]);

      Route::get('dashboard/constancia-por-modalidad-arbitro/pdf/{id}', [
          'as' => 'reporte.pdfConstanciaModalidadArbitro', 'uses' => 'ReporteController@pdfConstanciaModalidadArbitro'
      ]);

      Route::get('dashboard/{tipo}/pdf/{id}', [
          'as' => 'reporte.pdfReporte', 'uses' => 'ReporteController@pdfReporte'
      ]);


});
