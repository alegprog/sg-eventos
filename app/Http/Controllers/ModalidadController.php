<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Modalidad;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ModalidadController extends Controller
{
  public function listado()
  {
      $modalidades= Modalidad::withTrashed()->get();
      return view('modalidad.listado',compact('modalidades'));
  }

  public function crear()
  {
      return view('modalidad.crear');
  }

  public function guardar(Request $request)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:modalidades,nombre_modalidad,null,id_modalidad|max:255'
    ]);

    $modalidad = new Modalidad();
    $modalidad->nombre_modalidad=$request->nombre;
    $modalidad->save();

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('modalidad.listado')->with('mensaje_ok',$ok);

  }

  public function editar($id)
  {
    $modalidad= Modalidad::withTrashed()->where('id_modalidad','=',$id)->first();
    return view('modalidad.editar',compact('modalidad'));
  }

  public function actualizar(Request $request, $id)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:modalidades,nombre_modalidad,'.$id.',id_modalidad|max:255'
    ]);

    /*$modalidad = Modalidad::withTrashed()->where('id','=',$id)->first();
    $modalidad->nombre_modalidad=$request->nombre;
    $modalidad->save();*/

    $modalidad = Modalidad::withTrashed()->where('id_modalidad','=',$id)
        ->update(['nombre_modalidad' => $request->nombre]);

    $ok='Excelente! se ha actualizado el registro satisfactoriamente';
    return redirect()->route('modalidad.listado')->with('mensaje_ok',$ok);
  }

  public function eliminar($id){
    $modalidad = Modalidad::withTrashed()->where('id_modalidad', $id)->delete();
    //$modalidad->delete();
    $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
    return redirect()->route('modalidad.listado')->with('mensaje_ok',$ok);
  }

  public function activar($id){
    $modalidad = Modalidad::withTrashed()->where('id_modalidad', $id)->restore();
    $ok='Excelente! se ha activado el registro satisfactoriamente';
    return redirect()->route('modalidad.listado')->with('mensaje_ok',$ok);
  }
}
