<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Responsable;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ResponsableController extends Controller
{
  public function listado()
  {
      $responsables= Responsable::withTrashed()->get();
      return view('responsable.listado',compact('responsables'));
  }

  public function crear()
  {
      return view('responsable.crear');
  }

  public function guardar(Request $request)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:responsables,nombre_responsable,null,id_responsable|max:255'
    ]);

    $responsable = new Responsable();
    $responsable->nombre_responsable=$request->nombre;
    $responsable->save();

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('responsable.listado')->with('mensaje_ok',$ok);

  }

  public function editar($id)
  {
    $responsable= Responsable::withTrashed()->where('id_responsable','=',$id)->first();
    return view('responsable.editar',compact('responsable'));
  }

  public function actualizar(Request $request, $id)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:responsables,nombre_responsable,'.$id.',id_responsable|max:255'
    ]);

    /*$responsable = Responsable::withTrashed()->where('id','=',$id)->first();
    $responsable->nombre_responsable=$request->nombre;
    $responsable->save();*/

    $responsable = Responsable::withTrashed()->where('id_responsable','=',$id)
        ->update(['nombre_responsable' => $request->nombre]);

    $ok='Excelente! se ha actualizado el registro satisfactoriamente';
    return redirect()->route('responsable.listado')->with('mensaje_ok',$ok);
  }

  public function eliminar($id){
    $responsable = Responsable::withTrashed()->where('id_responsable', $id)->delete();
    //$responsable->delete();
    $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
    return redirect()->route('responsable.listado')->with('mensaje_ok',$ok);
  }

  public function activar($id){
    $responsable = Responsable::withTrashed()->where('id_responsable', $id)->restore();
    $ok='Excelente! se ha activado el registro satisfactoriamente';
    return redirect()->route('responsable.listado')->with('mensaje_ok',$ok);
  }
}
