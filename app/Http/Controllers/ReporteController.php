<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Evento;
use App\Invitado;
use App\Arbitro;
use App\Requerimiento;
use App\Articulo;
use App\Arbitraje;
use Jenssegers\Date\Date;
use Carbon\Carbon;
use Anam\PhantomMagick\Converter;
use View;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use PDF;

class ReporteController extends Controller
{

    public function invitado()
    {
        $eventos=Evento::select('id_evento as id','nombre_evento as nombre')->lists('nombre','id');
        return view('reporte.invitado.buscar',compact('eventos'));
    }

    public function invitadoConstancia(){
      $eventos=Evento::select('id_evento as id','nombre_evento as nombre')->lists('nombre','id');
      return view('reporte.invitado.constancia',compact('eventos'));
    }

    public function reporteInvitado(Request $request)
    {
      $this->validate($request, [
        'evento_id' => 'required'
      ]);
      $invitados= Invitado::with(['persona' => function ($query) {
            $query->withTrashed();
            $query->with(['nacionalidad' => function ($query) {
                  $query->withTrashed();
            }]);
          },'evento' => function ($query) {
            $query->withTrashed();
          },'tipo_invitado' => function ($query) {
            $query->withTrashed();
          },'modalidad' => function ($query) {
            $query->withTrashed();
          }
      ])->where('evento_id', $request->evento_id)->get();

      $evento= Evento::withTrashed()->where('id_evento','=',$request->evento_id)->first();

      return view('reporte.invitado.resultado',compact('invitados','evento'));

    }

    public function reporteInvitadoConstancia(Request $request){
      $this->validate($request, [
        'evento_id' => 'required'
      ]);
      $invitados= Invitado::with(['persona' => function ($query) {
            $query->withTrashed();
            $query->with(['nacionalidad' => function ($query) {
                  $query->withTrashed();
            }]);
          },'evento' => function ($query) {
            $query->withTrashed();
          },'tipo_invitado' => function ($query) {
            $query->withTrashed();
          },'modalidad' => function ($query) {
            $query->withTrashed();
          }
      ])->where('evento_id', $request->evento_id)->get();

      $evento= Evento::withTrashed()->where('id_evento','=',$request->evento_id)->first();

      return view('reporte.invitado.reporte',compact('invitados','evento'));
    }

    public function generarConstanciaInvitado($id){
      $invitado= Invitado::withTrashed()->with(['persona' => function ($query) {
            $query->withTrashed();
            $query->with(['nacionalidad' => function ($query) {
                  $query->withTrashed();
            }]);
            $query->with(['documento' => function ($query) {
                  $query->withTrashed();
            }]);
          },'evento' => function ($query) {
            $query->withTrashed();
            $query->with(['tipo_evento' => function ($query) {
                  $query->withTrashed();
            }]);
            $query->with(['dependencia' => function ($query) {
                  $query->withTrashed();
                  $query->with(['firma_principal' => function ($query) {
                        $query->withTrashed();
                  }]);
                  $query->with(['firma_segundaria' => function ($query) {
                        $query->withTrashed();
                  }]);
            }]);
            $query->with(['lugar' => function ($query) {
                  $query->withTrashed();
            }]);
          },'tipo_invitado' => function ($query) {
            $query->withTrashed();
          },'modalidad' => function ($query) {
            $query->withTrashed();
          }
      ])->where('id_invitado', $id)->first();

      //dd($invitado);
      $id=$invitado->id_invitado;
      return view('reporte.invitado.rptconstancia',compact('invitado','id'));
    }

    public function generarCertificadoInvitado($id){
      $invitado= Invitado::withTrashed()->with(['persona' => function ($query) {
            $query->withTrashed();
            $query->with(['nacionalidad' => function ($query) {
                  $query->withTrashed();
            }]);
            $query->with(['documento' => function ($query) {
                  $query->withTrashed();
            }]);
          },'evento' => function ($query) {
            $query->withTrashed();
            $query->with(['tipo_evento' => function ($query) {
                  $query->withTrashed();
            }]);
            $query->with(['dependencia' => function ($query) {
                  $query->withTrashed();
                  $query->with(['firma_principal' => function ($query) {
                        $query->withTrashed();
                  }]);
                  $query->with(['firma_segundaria' => function ($query) {
                        $query->withTrashed();
                  }]);
            }]);
            $query->with(['lugar' => function ($query) {
                  $query->withTrashed();
            }]);
          },'tipo_invitado' => function ($query) {
            $query->withTrashed();
          },'modalidad' => function ($query) {
            $query->withTrashed();
          }
      ])->where('id_invitado', $id)->first();

      //dd($invitado
      $id=$invitado->id_invitado;
      return view('reporte.invitado.rptcertificado',compact('invitado','id'));
    }

    public function arbitro()
    {
        $eventos=Evento::select('id_evento as id','nombre_evento as nombre')->lists('nombre','id');
        return view('reporte.arbitro.buscar',compact('eventos'));
    }

    public function arbitroConstancia(){
      $eventos=Evento::select('id_evento as id','nombre_evento as nombre')->lists('nombre','id');
      return view('reporte.arbitro.constancia',compact('eventos'));
    }

    public function reporteArbitroConstancia(Request $request){
      $this->validate($request, [
        'evento_id' => 'required'
      ]);
      $arbitros= Arbitro::with(['persona' => function ($query) {
            $query->withTrashed();
            $query->with(['nacionalidad' => function ($query) {
                  $query->withTrashed();
            }]);
          },'evento' => function ($query) {
            $query->withTrashed();
          },'tipo_arbitro' => function ($query) {
            $query->withTrashed();
          },'modalidad' => function ($query) {
            $query->withTrashed();
          }
      ])->where('evento_id', $request->evento_id)->get();

      $evento= Evento::withTrashed()->where('id_evento','=',$request->evento_id)->first();

      return view('reporte.arbitro.reporte',compact('arbitros','evento'));
    }


    public function reporteArbitro(Request $request)
    {
      $this->validate($request, [
        'evento_id' => 'required'
      ]);
      $arbitros= Arbitro::with(['persona' => function ($query) {
            $query->withTrashed();
            $query->with(['nacionalidad' => function ($query) {
                  $query->withTrashed();
            }]);
          },'evento' => function ($query) {
            $query->withTrashed();
          },'tipo_arbitro' => function ($query) {
            $query->withTrashed();
          },'modalidad' => function ($query) {
            $query->withTrashed();
          }
      ])->where('evento_id', $request->evento_id)->get();

      $evento= Evento::withTrashed()->where('id_evento','=',$request->evento_id)->first();

      return view('reporte.arbitro.resultado',compact('arbitros','evento'));

    }

    public function generarConstanciaArbitro($id){
      $arbitro= Arbitro::withTrashed()->with(['persona' => function ($query) {
            $query->withTrashed();
            $query->with(['nacionalidad' => function ($query) {
                  $query->withTrashed();
            }]);
            $query->with(['documento' => function ($query) {
                  $query->withTrashed();
            }]);
          },'evento' => function ($query) {
            $query->withTrashed();
            $query->with(['tipo_evento' => function ($query) {
                  $query->withTrashed();
            }]);
            $query->with(['dependencia' => function ($query) {
                  $query->withTrashed();
                  $query->with(['firma_principal' => function ($query) {
                        $query->withTrashed();
                  }]);
                  $query->with(['firma_segundaria' => function ($query) {
                        $query->withTrashed();
                  }]);
            }]);
            $query->with(['lugar' => function ($query) {
                  $query->withTrashed();
            }]);
          },'tipo_arbitro' => function ($query) {
            $query->withTrashed();
          },'modalidad' => function ($query) {
            $query->withTrashed();
          }
      ])->where('id_arbitro', $id)->first();
      $date = new Date(Carbon::createFromFormat('d/m/Y', $arbitro->evento->fecha_inicio_evento)->toDateString());
      //$date->format('j F Y');
      return view('reporte.arbitro.rptconstancia',compact('arbitro','date','id'));
    }

    public function generarCertificadoArbitro($id){
      $arbitro= Arbitro::withTrashed()->with(['persona' => function ($query) {
            $query->withTrashed();
            $query->with(['nacionalidad' => function ($query) {
                  $query->withTrashed();
            }]);
            $query->with(['documento' => function ($query) {
                  $query->withTrashed();
            }]);
          },'evento' => function ($query) {
            $query->withTrashed();
            $query->with(['tipo_evento' => function ($query) {
                  $query->withTrashed();
            }]);
            $query->with(['dependencia' => function ($query) {
                  $query->withTrashed();
                  $query->with(['firma_principal' => function ($query) {
                        $query->withTrashed();
                  }]);
                  $query->with(['firma_segundaria' => function ($query) {
                        $query->withTrashed();
                  }]);
            }]);
            $query->with(['lugar' => function ($query) {
                  $query->withTrashed();
            }]);
          },'tipo_arbitro' => function ($query) {
            $query->withTrashed();
          },'modalidad' => function ($query) {
            $query->withTrashed();
          }
      ])->where('id_arbitro', $id)->first();

      //dd($invitado);
      $date = new Date(Carbon::createFromFormat('d/m/Y', $arbitro->evento->fecha_inicio_evento)->toDateString());
      return view('reporte.arbitro.rptcertificado',compact('arbitro','date','id'));
    }

    public function requerimiento()
    {
        $eventos=Evento::select('id_evento as id','nombre_evento as nombre')->lists('nombre','id');
        return view('reporte.requerimiento.buscar',compact('eventos'));
    }

    public function reporteRequerimiento(Request $request)
    {
      $this->validate($request, [
        'evento_id' => 'required'
      ]);
      $requerimientos= Requerimiento::with(['evento' => function ($query) {
            $query->withTrashed();
          },'tipo_requerimiento' => function ($query) {
            $query->withTrashed();
          },'responsable' => function ($query) {
            $query->withTrashed();
          }
      ])->where('evento_id', $request->evento_id)->get();

      $evento= Evento::withTrashed()->where('id_evento','=',$request->evento_id)->first();

          return view('reporte.requerimiento.resultado',compact('requerimientos','evento'));
    }

    public function arbitraje()
    {
        $eventos=Evento::select('id_evento as id','nombre_evento as nombre')->lists('nombre','id');
        return view('reporte.arbitraje.buscar',compact('eventos'));
    }

    public function reporteArbitraje(Request $request)
    {
      $this->validate($request, [
        'evento_id' => 'required'
      ]);
      $arbitrajes= Arbitraje::with(['evento' => function ($query) {
            $query->withTrashed();
          },'articulo' => function ($query) {
            $query->withTrashed();
          },'arbitro' => function ($query) {
            $query->withTrashed();
            $query->with(['persona' => function ($query) {
                  $query->withTrashed();
                  $query->with(['nacionalidad' => function ($query) {
                        $query->withTrashed();
                  }]);
            }]);
          },'estatu' => function ($query) {
            $query->withTrashed();
          }
      ])->where('evento_id', $request->evento_id)->get();

      //dd($arbitrajes);
      $evento= Evento::withTrashed()->where('id_evento','=',$request->evento_id)->first();

      return view('reporte.arbitraje.resultado',compact('arbitrajes','evento'));
    }

    public function articuloConstancia(){
      $eventos=Evento::select('id_evento as id','nombre_evento as nombre')->lists('nombre','id');
      return view('reporte.articulo.buscar',compact('eventos'));
    }

    public function reporteArticuloConstancia(Request $request){
      $this->validate($request, [
        'evento_id' => 'required'
      ]);
      $articulos= Articulo::with(['persona' => function ($query) {
            $query->withTrashed();
          },'evento' => function ($query) {
            $query->withTrashed();
            $query->with(['tipo_evento' => function ($query) {
                  $query->withTrashed();
            }]);
            $query->with(['dependencia' => function ($query) {
                  $query->withTrashed();
                  $query->with(['firma_principal' => function ($query) {
                        $query->withTrashed();
                  }]);
                  $query->with(['firma_segundaria' => function ($query) {
                        $query->withTrashed();
                  }]);
            }]);
          },'coautor_principal' => function ($query) {
            $query->withTrashed();
          },'coautor_segundario' => function ($query) {
            $query->withTrashed();
          }
      ])->where('evento_id', $request->evento_id)->get();

      $evento= Evento::withTrashed()->where('id_evento','=',$request->evento_id)->first();

      return view('reporte.articulo.resultado',compact('articulos','evento'));
    }

    public function generarConstanciaArticulo($id){
      $articulo= Articulo::withTrashed()->with(['persona' => function ($query) {
            $query->withTrashed();
          },'evento' => function ($query) {
            $query->withTrashed();
            $query->with(['tipo_evento' => function ($query) {
                  $query->withTrashed();
            }]);
            $query->with(['dependencia' => function ($query) {
                  $query->withTrashed();
                  $query->with(['firma_principal' => function ($query) {
                        $query->withTrashed();
                  }]);
                  $query->with(['firma_segundaria' => function ($query) {
                        $query->withTrashed();
                  }]);
            }]);
          },'coautor_principal' => function ($query) {
            $query->withTrashed();
          },'coautor_segundario' => function ($query) {
            $query->withTrashed();
          }
      ])->where('id_articulo', $id)->first();
      $date = new Date(Carbon::createFromFormat('d/m/Y', $articulo->evento->fecha_inicio_evento)->toDateString());
      //$date->format('j F Y');
      return view('reporte.articulo.rptconstancia',compact('articulo','date','id'));
    }


    //**** PDF
    public function pdfConstanciaInvitado($tipo,$id){
      //($tipo);
      if($tipo=='constancia'){
        $tipo='Constancia';
      }else{
        $tipo='Certificado';
      }

      $invitado= Invitado::withTrashed()->with(['persona' => function ($query) {
            $query->withTrashed();
            $query->with(['nacionalidad' => function ($query) {
                  $query->withTrashed();
            }]);
            $query->with(['documento' => function ($query) {
                  $query->withTrashed();
            }]);
          },'evento' => function ($query) {
            $query->withTrashed();
            $query->with(['tipo_evento' => function ($query) {
                  $query->withTrashed();
            }]);
            $query->with(['dependencia' => function ($query) {
                  $query->withTrashed();
                  $query->with(['firma_principal' => function ($query) {
                        $query->withTrashed();
                  }]);
                  $query->with(['firma_segundaria' => function ($query) {
                        $query->withTrashed();
                  }]);
            }]);
            $query->with(['lugar' => function ($query) {
                  $query->withTrashed();
            }]);
          },'tipo_invitado' => function ($query) {
            $query->withTrashed();
          },'modalidad' => function ($query) {
            $query->withTrashed();
          }
      ])->where('id_invitado', $id)->first();
      //dd($invitado);
      $pdf = PDF::loadView('reporte.invitado.pdfconstancia', [
           'invitado' => $invitado,
           'tipo'=>$tipo
       ]);

      $nombre=str_slug($tipo.'_invitado_'.$invitado->persona->nombre_persona.'-'.$invitado->persona->apellido_persona.'-'.$invitado->persona->identificacion_persona);
      return $pdf->stream($nombre.'.pdf');

      //return $pdf->download($nombre);


    }

    public function pdfConstanciaArticulo($id){
      $articulo= Articulo::withTrashed()->with(['persona' => function ($query) {
            $query->withTrashed();
          },'evento' => function ($query) {
            $query->withTrashed();
            $query->with(['tipo_evento' => function ($query) {
                  $query->withTrashed();
            }]);
            $query->with(['dependencia' => function ($query) {
                  $query->withTrashed();
                  $query->with(['firma_principal' => function ($query) {
                        $query->withTrashed();
                  }]);
                  $query->with(['firma_segundaria' => function ($query) {
                        $query->withTrashed();
                  }]);
            }]);
          },'coautor_principal' => function ($query) {
            $query->withTrashed();
          },'coautor_segundario' => function ($query) {
            $query->withTrashed();
          }
      ])->where('id_articulo', $id)->first();
      $date = new Date(Carbon::createFromFormat('d/m/Y', $articulo->evento->fecha_inicio_evento)->toDateString());
      //$date->format('j F Y');
      $pdf = PDF::loadView('reporte.articulo.pdfconstancia', [
           'articulo' => $articulo,
           'date'=>$date
       ]);

      $nombre=str_slug('constancia_articulo_'.$articulo->codigo_articulo);
      return $pdf->stream($nombre.'.pdf');
    }

    public function pdfConstanciaArbitro($id){
      $arbitro= Arbitro::withTrashed()->with(['persona' => function ($query) {
            $query->withTrashed();
            $query->with(['nacionalidad' => function ($query) {
                  $query->withTrashed();
            }]);
            $query->with(['documento' => function ($query) {
                  $query->withTrashed();
            }]);
          },'evento' => function ($query) {
            $query->withTrashed();
            $query->with(['tipo_evento' => function ($query) {
                  $query->withTrashed();
            }]);
            $query->with(['dependencia' => function ($query) {
                  $query->withTrashed();
                  $query->with(['firma_principal' => function ($query) {
                        $query->withTrashed();
                  }]);
                  $query->with(['firma_segundaria' => function ($query) {
                        $query->withTrashed();
                  }]);
            }]);
            $query->with(['lugar' => function ($query) {
                  $query->withTrashed();
            }]);
          },'tipo_arbitro' => function ($query) {
            $query->withTrashed();
          },'modalidad' => function ($query) {
            $query->withTrashed();
          }
      ])->where('id_arbitro', $id)->first();
      $date = new Date(Carbon::createFromFormat('d/m/Y', $arbitro->evento->fecha_inicio_evento)->toDateString());

      $pdf = PDF::loadView('reporte.arbitro.pdfconstancia', [
           'arbitro' => $arbitro,
           'date'=>$date
       ]);

      $nombre=str_slug('constancia_arbitro_'.$arbitro->persona->nombre_persona.'-'.$arbitro->persona->apellido_persona.'-'.$arbitro->persona->identificacion_persona);
      return $pdf->stream($nombre.'.pdf');

    }

    public function pdfConstanciaModalidadArbitro($id){
      $arbitro= Arbitro::withTrashed()->with(['persona' => function ($query) {
            $query->withTrashed();
            $query->with(['nacionalidad' => function ($query) {
                  $query->withTrashed();
            }]);
            $query->with(['documento' => function ($query) {
                  $query->withTrashed();
            }]);
          },'evento' => function ($query) {
            $query->withTrashed();
            $query->with(['tipo_evento' => function ($query) {
                  $query->withTrashed();
            }]);
            $query->with(['dependencia' => function ($query) {
                  $query->withTrashed();
                  $query->with(['firma_principal' => function ($query) {
                        $query->withTrashed();
                  }]);
                  $query->with(['firma_segundaria' => function ($query) {
                        $query->withTrashed();
                  }]);
            }]);
            $query->with(['lugar' => function ($query) {
                  $query->withTrashed();
            }]);
          },'tipo_arbitro' => function ($query) {
            $query->withTrashed();
          },'modalidad' => function ($query) {
            $query->withTrashed();
          }
      ])->where('id_arbitro', $id)->first();

      //dd($invitado);
      $date = new Date(Carbon::createFromFormat('d/m/Y', $arbitro->evento->fecha_inicio_evento)->toDateString());

      $pdf = PDF::loadView('reporte.arbitro.pdfconstancia', [
           'arbitro' => $arbitro,
           'date'=>$date
       ]);

      $nombre=str_slug('constancia_por_modalidad_'.$arbitro->persona->nombre_persona.'-'.$arbitro->persona->apellido_persona.'-'.$arbitro->persona->identificacion_persona);
      return $pdf->stream($nombre.'.pdf');
    }


    public function pdfReporte($tipo,$id){
      //($tipo);
      if($tipo=='requerimiento'){
        $titulo='Requerimiento';
        $consulta= Requerimiento::with(['evento' => function ($query) {
              $query->withTrashed();
            },'tipo_requerimiento' => function ($query) {
              $query->withTrashed();
            },'responsable' => function ($query) {
              $query->withTrashed();
            }
        ])->where('evento_id', $id)->get();

      }elseif($tipo=='arbitraje'){
        $titulo='Arbitraje';
        $consulta= Arbitraje::with(['evento' => function ($query) {
              $query->withTrashed();
            },'articulo' => function ($query) {
              $query->withTrashed();
            },'arbitro' => function ($query) {
              $query->withTrashed();
              $query->with(['persona' => function ($query) {
                    $query->withTrashed();
                    $query->with(['nacionalidad' => function ($query) {
                          $query->withTrashed();
                    }]);
              }]);
            },'estatu' => function ($query) {
              $query->withTrashed();
            }
        ])->where('evento_id', $id)->get();

      }elseif ($tipo=='arbitro'){
        $titulo='Arbitro';
        $consulta= Arbitro::with(['persona' => function ($query) {
              $query->withTrashed();
              $query->with(['nacionalidad' => function ($query) {
                    $query->withTrashed();
              }]);
            },'evento' => function ($query) {
              $query->withTrashed();
            },'tipo_arbitro' => function ($query) {
              $query->withTrashed();
            },'modalidad' => function ($query) {
              $query->withTrashed();
            }
        ])->where('evento_id', $id)->get();
      }elseif ($tipo=='invitado'){
        $titulo='Invitado';
        $consulta= Invitado::with(['persona' => function ($query) {
              $query->withTrashed();
              $query->with(['nacionalidad' => function ($query) {
                    $query->withTrashed();
              }]);
            },'evento' => function ($query) {
              $query->withTrashed();
            },'tipo_invitado' => function ($query) {
              $query->withTrashed();
            },'modalidad' => function ($query) {
              $query->withTrashed();
            }
        ])->where('evento_id', $id)->get();
      }

      $evento= Evento::withTrashed()->where('id_evento','=',$id)->first();

      $pdf = PDF::loadView('reporte.pdfReporte', [
           'consultas' => $consulta,
           'evento'=>$evento,
           'tipo'=>$tipo,
           'titulo'=>$titulo
       ]);

      $nombre=str_slug($tipo.'-'.$evento->nombre_evento);
      return $pdf->stream($nombre.'.pdf');

}

}
