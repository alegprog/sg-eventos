<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usuario;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
      return view('dashboard.index');
    }

    public function getLogin()
    {
      return view('auth.login');
    }

    public function postLogin(Request $request)
    {
      $this->validate($request, [
        'usuario' => 'required',
        'clave' => 'required'
      ]);

      if(Auth::attempt(['usuario'=>$request->usuario,'password'=>$request->clave,'deleted_at'=>null])){
            return redirect('/dashboard');
      }

      return redirect('/')
           ->with('mensaje', 'Los datos del usuario son incorrectos')
           ->with('color', 'danger')
           ->with('logo',"fa-times");
    }

    public function getRegistro()
    {
      return view('auth.register');
    }

    public function postRegistro(Request $request)
    {
      $this->validate($request, [
        'nombre' => 'required',
        'apellido' => 'required',
        'cedula' => 'required|unique:usuarios,cedula_usuario|min:7|numeric',
        'usuario' => 'required|unique:usuarios|min:4|alpha_dash',
        'correo' => 'required|email|unique:usuarios,correo_usuario',
        'clave'=> 'required|min:6|confirmed:clave',
        'clave_confirmation'=> 'required'
      ]);

      $usuario = new Usuario();
      $usuario->nombre_usuario=$request->nombre;
      $usuario->apellido_usuario=$request->apellido;
      $usuario->cedula_usuario=$request->cedula;
      $usuario->correo_usuario=$request->correo;
      $usuario->usuario=strtolower($request->usuario);
      $usuario->password=bcrypt($request->clave);
      $usuario->tipo_usuario='operador';
      $usuario->save();

      $ok='Excelente! se ha registrado satisfactoriamente';
      return redirect('/')->with('mensaje_ok',$ok);
    }

    public function getAjuste()
    {
      $id=auth::user()->id;
      $usuario= Usuario::withTrashed()->findOrFail($id);
      return view('usuario.ajuste',compact('usuario'));
    }

    public function postAjuste(Request $request)
    {
      $id=auth::user()->id;

      $this->validate($request, [
        'usuario' => 'required|unique:usuarios,usuario,'.$id.'|min:4|alpha_dash',
        'correo' => 'required|email|unique:usuarios,correo_usuario,'.$id.'',
      ]);

      $usuario=Usuario::withTrashed()->findOrFail($id);
      $usuario->correo_usuario=$request->correo;
      $usuario->usuario=strtolower($request->usuario);
      $usuario->save();

      $ok='Excelente! se ha realizado el cambio satisfactoriamente';
      return redirect()->route('dashboard.ajuste')->with('mensaje_ok',$ok);
    }

    public function getCambioClave()
    {
      return view('usuario.cambio_clave');
    }

    public function postCambioClave(Request $request)
    {
      $this->validate($request, [
        'clave'=> 'required|min:6|confirmed:clave',
        'clave_confirmation'=> 'required'
      ]);


      $id=auth::user()->id;

      $usuario=Usuario::withTrashed()->findOrFail($id);
      //$usuario->usuario=strtolower($request->usuario);
      $usuario->password=bcrypt($request->clave);
      $usuario->save();

      $ok='Excelente! se ha realizado el cambio de contraseña satisfactoriamente';
      return redirect()->route('dashboard.cambio_clave')->with('mensaje_ok',$ok);
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect('/');
    }
}
