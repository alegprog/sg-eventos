<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Invitado;
use App\Evento;
use App\TipoInvitado;
use App\Persona;
use App\Modalidad;
use Storage;
use File;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class InvitadoController extends Controller
{
  public function listado()
  {
    $invitados= Invitado::withTrashed()->with(['persona' => function ($query) {
          $query->withTrashed();
        },'evento' => function ($query) {
          $query->withTrashed();
        },'tipo_invitado' => function ($query) {
          $query->withTrashed();
        },'modalidad' => function ($query) {
          $query->withTrashed();
        }
    ])->get();

    //dd($invitados);

    return view('invitado.listado',compact('invitados'));
  }

  public function crear()
  {
      $eventos=Evento::select('id_evento as id','nombre_evento as nombre')->lists('nombre','id');
      $personas=Persona::select('id_persona as id',DB::raw('concat(identificacion_persona," - ",nombre_persona," ",apellido_persona) as nombre'))->lists('nombre','id');
      $tipo_invitados=TipoInvitado::select('id_tipo_invitado as id','nombre_tipo_invitado as nombre')->lists('nombre','id');
      $modalidad=Modalidad::select('id_modalidad as id','nombre_modalidad as nombre')->lists('nombre','id');
      //dd($tipo_identificaciones);
      return view('invitado.crear',compact('eventos', 'personas', 'tipo_invitados','modalidad'));
  }

  public function guardar(Request $request)
  {

    $this->validate($request, [
      'titulo' => 'required|max:200',
      'evento_id'=> 'required',
      'tipo_invitado_id'=> 'required',
      'persona_id'=> 'required|unique:invitados,persona_id,null,id_invitado,evento_id,'.$request->evento_id.'',
      'modalidad_id'=> 'required',
      'archivo'=>'min:1|max:2000'
    ]);

    $invitado = new Invitado();
    $invitado->titulo_ponencia_invitado=$request->titulo;
    $invitado->evento_id = $request->evento_id;
    $invitado->tipo_invitado_id = $request->tipo_invitado_id;
    $invitado->persona_id = $request->persona_id;
    $invitado->modalidad_id = $request->modalidad_id;
    $invitado->save();

    $file=$request->file('archivo');
    //dd($invitado);
    if(!empty($file)){
      $nombre_archivo = 'invitado-'.str_slug($invitado->titulo_ponencia_invitado).'-'.$invitado->id.'-'.str_random(10).'.'.$file->getClientOriginalExtension();
      //$nombre_archivo = str_random(40).'.'.$file->getClientOriginalExtension();
      //dd($nombre_archivo);
      Storage::disk('invitado')->put($nombre_archivo,File::get($file));
      $guardarArchivo = Invitado::withTrashed()->where('id_invitado','=',$invitado->id)
          ->update(['archivo' => $nombre_archivo]);

    }

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('invitado.listado')->with('mensaje_ok',$ok);
  }

  public function detalle($id)
  {
    $invitado= Invitado::withTrashed()->with(['persona' => function ($query) {
          $query->withTrashed();
        },'evento' => function ($query) {
          $query->withTrashed();
        },'tipo_invitado' => function ($query) {
          $query->withTrashed();
        },'modalidad' => function ($query) {
          $query->withTrashed();
        }
    ])->where('id_invitado', $id)->first();
    if(!empty($invitado->archivo)){
      $archivo=explode('.',$invitado->archivo);
      if($archivo[1]=='jpeg' || $archivo[1]=='jpg' || $archivo[1]=='bmp' || $archivo[1]=='png' || $archivo[1]=='gif'){
        $tipo_archivo='imagen';
      }else{
        $tipo_archivo='archivo';
      }
    }else{
      $tipo_archivo='vacio';
    }
    //dd($evento);
    return view('invitado.detalle',compact('invitado','tipo_archivo'));
  }

  public function eliminar($id){
    $invitado = Invitado::withTrashed()->where('id_invitado', $id)->delete();

    $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
    return redirect()->route('invitado.listado')->with('mensaje_ok',$ok);
  }

  public function editar($id)
  {
    $invitado= Invitado::withTrashed()->with(['persona' => function ($query) {
          $query->withTrashed();
        },'evento' => function ($query) {
          $query->withTrashed();
        },'tipo_invitado' => function ($query) {
          $query->withTrashed();
        },'modalidad' => function ($query) {
          $query->withTrashed();
        }
    ])->where('id_invitado', $id)->first();
    $eventos=Evento::select('id_evento as id','nombre_evento as nombre')->lists('nombre','id');
    $personas=Persona::select('id_persona as id',DB::raw('concat(identificacion_persona," - ",nombre_persona," ",apellido_persona) as nombre'))->lists('nombre','id');
    $tipo_invitados=TipoInvitado::select('id_tipo_invitado as id','nombre_tipo_invitado as nombre')->lists('nombre','id');
    $modalidad=Modalidad::select('id_modalidad as id','nombre_modalidad as nombre')->lists('nombre','id');
    return view('invitado.editar',compact('invitado','eventos', 'personas', 'tipo_invitados','modalidad'));
  }

  public function actualizar(Request $request, $id)
  {

    $this->validate($request, [
      'titulo' => 'required|max:200',
      'evento_id'=> '',
      'tipo_invitado_id'=> '',
      'persona_id'=> 'unique:invitados,persona_id,'.$id.',id_invitado,evento_id,'.$request->evento_id.'',
      'modalidad_id'=> '',
      'archivo'=>'min:1|max:2000'
      //'archivo'=>'mimes:jpeg,bmp,png,jpg|min:1|max:2000'
    ]);
     //dd($request->all());
    $invitado = Invitado::withTrashed()->where('id_invitado','=',$id)
        ->update([
          'titulo_ponencia_invitado' => $request->titulo
        ]);

    if(!empty($request->evento_id)){
      $evento_id = Invitado::withTrashed()->where('id_invitado','=',$id)
          ->update([
            'evento_id'=>$request->evento_id
          ]);
    }

    if(!empty($request->tipo_invitado_id)){
      $tipo_invitado_id = Invitado::withTrashed()->where('id_invitado','=',$id)
          ->update([
            'tipo_invitado_id'=>$request->tipo_invitado_id
          ]);
    }

    if(!empty($request->persona_id)){
      $persona_id = Invitado::withTrashed()->where('id_invitado','=',$id)
          ->update([
            'persona_id'=>$request->persona_id
          ]);
    }

    if(!empty($request->modalidad_id)){
      $modalidad_id = Invitado::withTrashed()->where('id_invitado','=',$id)
          ->update([
            'modalidad_id'=>$request->modalidad_id
          ]);
    }


    $file=$request->file('archivo');
    if(!empty($file)){
      $invitado=Invitado::withTrashed()->where('id_invitado', $id)->first();
      if(!empty($invitado->archivo)){
        $public_path = public_path();
        $url = $public_path.'/assets/file/invitado/'.$invitado->archivo;
        if(Storage::disk('invitado')->has($invitado->archivo)){
          Storage::disk('invitado')->delete($invitado->archivo);
          $invitado->archivo="";
        }
      }
      //dd($invitado);
      $nombre_archivo = 'invitado-'.str_slug($request->titulo).'-'.$id.'-'.str_random(10).'.'.$file->getClientOriginalExtension();

      Storage::disk('invitado')->put($nombre_archivo,File::get($file));

      $guardarArchivo = Invitado::withTrashed()->where('id_invitado','=',$id)
          ->update(['archivo' => $nombre_archivo]);
    }

    $ok='Excelente! se ha actualizado el registro satisfactoriamente';
    return redirect()->route('invitado.listado')->with('mensaje_ok',$ok);

  }

  public function activar($id){
    $invitado = Invitado::withTrashed()->where('id_invitado', $id)->restore();
    $ok='Excelente! se ha activado el registro satisfactoriamente';
    return redirect()->route('invitado.listado')->with('mensaje_ok',$ok);
  }

  public function download($archivo){
    $public_path = public_path();
    $url = $public_path.'/assets/file/invitado/'.$archivo;
    if(Storage::disk('invitado')->has($archivo)){
      return response()->download($url);
    }
  }


}
