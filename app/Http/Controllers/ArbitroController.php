<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Arbitro;
use App\Evento;
use App\TipoArbitro;
use App\Persona;
use App\Modalidad;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ArbitroController extends Controller
{
  public function listado()
  {
    $arbitros= Arbitro::withTrashed()->with(['persona' => function ($query) {
          $query->withTrashed();
        },'evento' => function ($query) {
          $query->withTrashed();
        },'tipo_arbitro' => function ($query) {
          $query->withTrashed();
        },'modalidad' => function ($query) {
          $query->withTrashed();
        }
    ])->get();

    //dd($invitados);

    return view('arbitro.listado',compact('arbitros'));
  }

  public function crear()
  {
      $eventos=Evento::select('id_evento as id','nombre_evento as nombre')->lists('nombre','id');
      $personas=Persona::select('id_persona as id',DB::raw('concat(identificacion_persona," - ",nombre_persona," ",apellido_persona) as nombre'))->lists('nombre','id');
      $tipo_arbitros=TipoArbitro::select('id_tipo_arbitro as id','nombre_tipo_arbitro as nombre')->lists('nombre','id');
      $modalidad=Modalidad::select('id_modalidad as id','nombre_modalidad as nombre')->lists('nombre','id');
      //dd($tipo_identificaciones);
      return view('arbitro.crear',compact('eventos', 'personas', 'tipo_arbitros','modalidad'));
  }


  public function guardar(Request $request)
  {

    $this->validate($request, [
      'evento_id'=> 'required',
      'tipo_arbitro_id'=> 'required',
      'persona_id'=> 'required|unique:arbitros,persona_id,null,id_arbitro,evento_id,'.$request->evento_id.'',
      'modalidad_id'=> 'required'
    ]);

    $arbitro = new Arbitro();
    $arbitro->evento_id = $request->evento_id;
    $arbitro->tipo_arbitro_id = $request->tipo_arbitro_id;
    $arbitro->persona_id = $request->persona_id;
    $arbitro->modalidad_id = $request->modalidad_id;
    $arbitro->save();

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('arbitro.listado')->with('mensaje_ok',$ok);
  }

  public function detalle($id)
  {
    $arbitro= Arbitro::withTrashed()->with(['persona' => function ($query) {
          $query->withTrashed();
        },'evento' => function ($query) {
          $query->withTrashed();
        },'tipo_arbitro' => function ($query) {
          $query->withTrashed();
        },'modalidad' => function ($query) {
          $query->withTrashed();
        }
    ])->where('id_arbitro', $id)->first();

    //dd($evento);
    return view('arbitro.detalle',compact('arbitro'));
  }

  public function editar($id)
  {
    $arbitro= Arbitro::withTrashed()->with(['persona' => function ($query) {
          $query->withTrashed();
        },'evento' => function ($query) {
          $query->withTrashed();
        },'tipo_arbitro' => function ($query) {
          $query->withTrashed();
        },'modalidad' => function ($query) {
          $query->withTrashed();
        }
    ])->where('id_arbitro', $id)->first();
    $eventos=Evento::select('id_evento as id','nombre_evento as nombre')->lists('nombre','id');
    $personas=Persona::select('id_persona as id',DB::raw('concat(identificacion_persona," - ",nombre_persona," ",apellido_persona) as nombre'))->lists('nombre','id');
    $tipo_arbitros=TipoArbitro::select('id_tipo_arbitro as id','nombre_tipo_arbitro as nombre')->lists('nombre','id');
    $modalidad=Modalidad::select('id_modalidad as id','nombre_modalidad as nombre')->lists('nombre','id');
    return view('arbitro.editar',compact('arbitro','eventos', 'personas', 'tipo_arbitros','modalidad'));
  }

  public function actualizar(Request $request, $id)
  {

    $this->validate($request, [
      'evento_id'=> 'required',
      'tipo_arbitro_id'=> 'required',
      'persona_id'=> 'required|unique:arbitros,persona_id,'.$id.',id_arbitro,evento_id,'.$request->evento_id.'',
      'modalidad_id'=> 'required'
      //'archivo'=>'mimes:jpeg,bmp,png,jpg|min:1|max:2000'
    ]);
     //dd($request->all());

    if(!empty($request->evento_id)){
      $evento_id = Arbitro::withTrashed()->where('id_arbitro','=',$id)
          ->update([
            'evento_id'=>$request->evento_id
          ]);
    }

    if(!empty($request->tipo_arbitro_id)){
      $tipo_arbitro_id = Arbitro::withTrashed()->where('id_arbitro','=',$id)
          ->update([
            'tipo_arbitro_id'=>$request->tipo_arbitro_id
          ]);
    }

    if(!empty($request->persona_id)){
      $persona_id = Arbitro::withTrashed()->where('id_arbitro','=',$id)
          ->update([
            'persona_id'=>$request->persona_id
          ]);
    }

    if(!empty($request->modalidad_id)){
      $modalidad_id = Arbitro::withTrashed()->where('id_arbitro','=',$id)
          ->update([
            'modalidad_id'=>$request->modalidad_id
          ]);
    }

    $ok='Excelente! se ha actualizado el registro satisfactoriamente';
    return redirect()->route('arbitro.listado')->with('mensaje_ok',$ok);

  }

  public function eliminar($id){
    $arbitro = Arbitro::withTrashed()->where('id_arbitro', $id)->delete();

    $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
    return redirect()->route('arbitro.listado')->with('mensaje_ok',$ok);
  }

  public function activar($id){
    $arbitro = Arbitro::withTrashed()->where('id_arbitro', $id)->restore();
    $ok='Excelente! se ha activado el registro satisfactoriamente';
    return redirect()->route('arbitro.listado')->with('mensaje_ok',$ok);
  }

}
