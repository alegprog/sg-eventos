<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TipoInvitado;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TipoInvitadoController extends Controller
{
  public function listado()
  {
      $tipo_invitados= TipoInvitado::withTrashed()->get();
      return view('tipo_invitado.listado',compact('tipo_invitados'));
  }

  public function crear()
  {
      return view('tipo_invitado.crear');
  }

  public function guardar(Request $request)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:tipo_invitados,nombre_tipo_invitado,null,id_tipo_invitado|max:255'
    ]);

    $tipo_invitado = new TipoInvitado();
    $tipo_invitado->nombre_tipo_invitado=$request->nombre;
    $tipo_invitado->save();

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('tipo_invitado.listado')->with('mensaje_ok',$ok);

  }

  public function editar($id)
  {
    $tipo_invitado= TipoInvitado::withTrashed()->where('id_tipo_invitado','=',$id)->first();
    return view('tipo_invitado.editar',compact('tipo_invitado'));
  }

  public function actualizar(Request $request, $id)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:tipo_invitados,nombre_tipo_invitado,'.$id.',id_tipo_invitado|max:255'
    ]);

    /*$tipo_invitado = TipoInvitado::withTrashed()->where('id','=',$id)->first();
    $tipo_invitado->nombre_invitado=$request->nombre;
    $tipo_invitado->save();*/

    $tipo_invitado = TipoInvitado::withTrashed()->where('id_tipo_invitado','=',$id)
        ->update(['nombre_tipo_invitado' => $request->nombre]);

    $ok='Excelente! se ha actualizado el registro satisfactoriamente';
    return redirect()->route('tipo_invitado.listado')->with('mensaje_ok',$ok);
  }

  public function eliminar($id){
    $tipo_invitado = TipoInvitado::withTrashed()->where('id_tipo_invitado', $id)->delete();
    //$tipo_invitado->delete();
    $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
    return redirect()->route('tipo_invitado.listado')->with('mensaje_ok',$ok);
  }

  public function activar($id){
    $tipo_invitado = TipoInvitado::withTrashed()->where('id_tipo_invitado', $id)->restore();
    $ok='Excelente! se ha activado el registro satisfactoriamente';
    return redirect()->route('tipo_invitado.listado')->with('mensaje_ok',$ok);
  }


}
