<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Evento;
use App\TipoEvento;
use App\Lugar;
use App\Dependencia;
use Storage;
use File;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class EventoController extends Controller
{
  public function listado()
  {
      $eventos= Evento::withTrashed()->with(['dependencia','tipo_evento' => function ($query) {
    $query->withTrashed();

}])->get();
      //dd($eventos);
      return view('evento.listado',compact('eventos'));
  }

  public function crear()
  {
      $tipo_eventos=TipoEvento::select('id_tipo_evento as id','nombre_tipo_evento as nombre')->lists('nombre','id');
      $lugares=Lugar::select('id_lugar as id','nombre_lugar as nombre')->lists('nombre','id');
      $dependencias=Dependencia::select('id_dependencia as id','nombre_dependencia as nombre')->lists('nombre','id');
      //dd($tipo_identificaciones);
      return view('evento.crear',compact('tipo_eventos', 'lugares', 'dependencias'));
  }

  public function guardar(Request $request)
  {

    $this->validate($request, [
      'nombre' => 'required|unique:eventos,nombre_evento,null,id_evento|max:200',
      'tipo_evento_id'=> 'required',
      'fecha_inicio'=> 'required',
      'lugar_id'=> 'required',
      'hora_inicio'=> 'required',
      'dependencia_id'=> 'required',
      'direccion'=> 'required',
      'duracion'=> 'required',
      'fecha_final'=> 'required',
      'hora_final'=> 'required',
      'archivo'=>'min:1|max:2000'
    ]);

    $evento = new Evento();
    $evento->nombre_evento=$request->nombre;
    $evento->tipo_evento_id = $request->tipo_evento_id;
    $evento->fecha_inicio_evento = $request->fecha_inicio;
    $evento->lugar_id = $request->lugar_id;
    $evento->hora_inicio_evento = $request->hora_inicio;
    $evento->dependencia_id = $request->dependencia_id;
    $evento->direccion_evento = $request->direccion;
    $evento->duracion_evento = $request->duracion;
    $evento->fecha_final_evento = $request->fecha_final;
    $evento->hora_final_evento = $request->hora_final;
    $evento->observacion_evento = $request->observaciones;
    $evento->save();

    $file=$request->file('archivo');
    //dd($file);
    if(!empty($file)){
      $nombre_archivo = 'evento-'.str_slug($evento->nombre_evento).'-'.$evento->id.'-'.str_random(10).'.'.$file->getClientOriginalExtension();
      //$nombre_archivo = str_random(40).'.'.$file->getClientOriginalExtension();
      Storage::disk('evento')->put($nombre_archivo,File::get($file));
      $guardarArchivo = Evento::withTrashed()->where('id_evento','=',$evento->id)
          ->update(['archivo' => $nombre_archivo]);

    }

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('evento.listado')->with('mensaje_ok',$ok);
  }

  public function detalle($id)
  {
    $evento= Evento::withTrashed()->where('id_evento','=',$id)->with(['dependencia' => function ($query) {
          $query->withTrashed();
        },'tipo_evento' => function ($query) {
          $query->withTrashed();
        },'lugar' => function ($query) {
          $query->withTrashed();
        }
    ])->first();
    if(!empty($evento->archivo)){
      $archivo=explode('.',$evento->archivo);
      if($archivo[1]=='jpeg' || $archivo[1]=='jpg' || $archivo[1]=='bmp' || $archivo[1]=='png' || $archivo[1]=='gif'){
        $tipo_archivo='imagen';
      }else{
        $tipo_archivo='archivo';
      }
    }else{
      $tipo_archivo='vacio';
    }
    //dd($evento);
    return view('evento.detalle',compact('evento','tipo_archivo'));
  }

  public function editar($id)
  {
    $evento= Evento::withTrashed()->with(['dependencia' => function ($query) {
          $query->withTrashed();
        },'tipo_evento' => function ($query) {
          $query->withTrashed();
        },'lugar' => function ($query) {
          $query->withTrashed();
        }
    ])->where('id_evento', $id)->first();
    $tipo_eventos=TipoEvento::select('id_tipo_evento as id','nombre_tipo_evento as nombre')->lists('nombre','id');
    $lugares=Lugar::select('id_lugar as id','nombre_lugar as nombre')->lists('nombre','id');
    $dependencias=Dependencia::select('id_dependencia as id','nombre_dependencia as nombre')->lists('nombre','id');
    return view('evento.editar',compact('evento','tipo_eventos', 'lugares', 'dependencias'));
  }

  public function actualizar(Request $request, $id)
  {
    /*$evento = Evento::withTrashed()->findOrFail($id);

    if(!empty($evento->archivo)){
      $public_path = public_path();
      $url = $public_path.'/file/evento/'.$evento->archivo;
      if(Storage::disk('evento')->has($evento->archivo)){
        Storage::disk('evento')->delete($evento->archivo);
        $evento->archivo="";
      }
    }*/
    $this->validate($request, [
      'nombre' => 'required|unique:eventos,nombre_evento,'.$id.',id_evento|max:255',
      'tipo_evento_id'=> '',
      'fecha_inicio'=> 'required',
      'lugar_id'=> '',
      'hora_inicio'=> 'required',
      'dependencia_id'=> '',
      'direccion'=> 'required',
      'duracion'=> 'required',
      'fecha_final'=> 'required',
      'hora_final'=> 'required',
      'archivo'=>'min:1|max:2000'
      //'archivo'=>'mimes:jpeg,bmp,png,jpg|min:1|max:2000'
    ]);
     //dd($request->all());
    $evento = Evento::withTrashed()->where('id_evento','=',$id)
        ->update([
          'nombre_evento' => $request->nombre,
          'direccion_evento'=>$request->direccion,
          'duracion_evento'=>$request->duracion,
          'fecha_inicio_evento'=>$this->setFecha($request->fecha_inicio),
          'fecha_final_evento'=>$this->setFecha($request->fecha_final),
          'hora_inicio_evento'=>$this->setHora($request->hora_inicio),
          'hora_final_evento'=>$this->setHora($request->hora_final),
          'observacion_evento'=>$request->observaciones
        ]);

    //dd($evento);

    //$evento = Evento::withTrashed()->findOrFail($id);
    //$evento->nombre_evento=$request->nombre;
    if(!empty($request->tipo_evento_id)){
      $evento_tipo_evento = Evento::withTrashed()->where('id_evento','=',$id)
          ->update([
            'tipo_evento_id'=>$request->tipo_evento_id
          ]);
    }
    //$evento->fecha_inicio_evento = $request->fecha_inicio;
    if(!empty($request->lugar_id)){
      $evento_lugar = Evento::withTrashed()->where('id_evento','=',$id)
          ->update([
            'lugar_id'=>$request->lugar_id
          ]);
    }
    //$evento->hora_inicio_evento = $request->hora_inicio;
    if(!empty($request->dependencia_id)){
      $evento_lugar = Evento::withTrashed()->where('id_evento','=',$id)
          ->update([
            'dependencia_id'=>$request->dependencia_id
          ]);
      ///$evento->dependencia_id = $request->dependencia_id;
    }
    //$evento->direccion_evento = $request->direccion;
    //$evento->duracion_evento = $request->duracion;
    //$evento->fecha_final_evento = $request->fecha_final;
    //$evento->hora_final_evento = $request->hora_final;
    //$evento->observacion_evento = $request->observaciones;
    //$evento->save();

    $file=$request->file('archivo');
    if(!empty($file)){
      $evento=Evento::withTrashed()->where('id_evento', $id)->first();

      if(!empty($evento->archivo)){
        $public_path = public_path();
        $url = $public_path.'/assets/file/evento/'.$evento->archivo;
        if(Storage::disk('evento')->has($evento->archivo)){
          Storage::disk('evento')->delete($evento->archivo);
          $evento->archivo="";
        }
      }
      //dd($evento);
      //$nombre_archivo = $string = str_random(40).'-'.$evento->id.'.jpg';
      $nombre_archivo = 'evento-'.str_slug($evento->nombre_evento).'-'.$evento->id_evento.'-'.str_random(10).'.'.$file->getClientOriginalExtension();
      Storage::disk('evento')->put($nombre_archivo,File::get($file));
      //dd($nombre_archivo);
      //$evento->archivo=$nombre_archivo;
      //$evento->save();
      $guardarArchivo = Evento::withTrashed()->where('id_evento','=',$evento->id_evento)
          ->update(['archivo' => $nombre_archivo]);
    }

    $ok='Excelente! se ha actualizado el registro satisfactoriamente';
    return redirect()->route('evento.listado')->with('mensaje_ok',$ok);

  }

  public function eliminar($id){
    $evento = Evento::withTrashed()->where('id_evento', $id)->delete();
    /*if(!empty($evento->archivo)){
      $public_path = public_path();
      $url = $public_path.'/file/evento/'.$evento->archivo;
      if(Storage::disk('evento')->has($evento->archivo)){
        Storage::disk('evento')->delete($evento->archivo);
        $evento->archivo="";
      }
    }*/
    //$evento->delete();
    $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
    return redirect()->route('evento.listado')->with('mensaje_ok',$ok);
  }

  public function activar($id){
    $evento = Evento::withTrashed()->where('id_evento', $id)->restore();
    $ok='Excelente! se ha activado el registro satisfactoriamente';
    return redirect()->route('evento.listado')->with('mensaje_ok',$ok);
  }

  public function download($archivo){
    $public_path = public_path();
    $url = $public_path.'/assets/file/evento/'.$archivo;
    if(Storage::disk('evento')->has($archivo)){
      return response()->download($url);
    }
  }

  private function setFecha($value)
  {
       return Carbon::createFromFormat('d/m/Y', $value)->toDateString();
  }

  private function setHora($value)
  {
       if(!empty($value)){
         $arrayHoraFinal=explode(' ',$value);
         $arrayHora=explode(':',$arrayHoraFinal[0]);
         if($arrayHoraFinal[1]=='PM'){
           $hora=($arrayHora[0]+12).':'.$arrayHora[1];
         }else{
           $hora=$arrayHoraFinal[0];
         }
       }else{
         $hora=$value;
       }

       return $hora;
  }

}
