<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Lugar;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class LugarController extends Controller
{
  public function listado()
  {
      $lugares= Lugar::withTrashed()->get();
      return view('lugar.listado',compact('lugares'));
  }

  public function crear()
  {
      return view('lugar.crear');
  }

  public function guardar(Request $request)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:lugares,nombre_lugar,null,id_lugar|max:255'
    ]);

    $lugar = new Lugar();
    $lugar->nombre_lugar=$request->nombre;
    $lugar->save();

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('lugar.listado')->with('mensaje_ok',$ok);

  }

  public function editar($id)
  {
    $lugar= Lugar::withTrashed()->where('id_lugar','=',$id)->first();
    return view('lugar.editar',compact('lugar'));
  }

  public function actualizar(Request $request, $id)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:lugares,nombre_lugar,'.$id.',id_lugar|max:255'
    ]);

    /*$lugar = Lugar::withTrashed()->where('id','=',$id)->first();
    $lugar->nombre_lugar=$request->nombre;
    $lugar->save();*/

    $lugar = Lugar::withTrashed()->where('id_lugar','=',$id)
        ->update(['nombre_lugar' => $request->nombre]);

    $ok='Excelente! se ha actualizado el registro satisfactoriamente';
    return redirect()->route('lugar.listado')->with('mensaje_ok',$ok);
  }

  public function eliminar($id){
    $lugar = Lugar::withTrashed()->where('id_lugar', $id)->delete();
    $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
    return redirect()->route('lugar.listado')->with('mensaje_ok',$ok);
  }

  public function activar($id){
    $lugar = Lugar::withTrashed()->where('id_lugar', $id)->restore();
    $ok='Excelente! se ha activado el registro satisfactoriamente';
    return redirect()->route('lugar.listado')->with('mensaje_ok',$ok);
  }
}
