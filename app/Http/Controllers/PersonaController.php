<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Persona;
use App\TipoIdentificacion;
use App\Nacionalidad;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PersonaController extends Controller
{
  public function listado()
  {
      $personas= Persona::withTrashed()->with(['documento'=> function ($query) {
        $query->withTrashed();
      },'nacionalidad' => function ($query) {
        $query->withTrashed();
      }])->get();
      //dd($personas);
      return view('persona.listado',compact('personas'));
  }

  public function crear()
  {
      $tipo_identificaciones=TipoIdentificacion::select('id_tipo_identificacion as id','nombre_tipo_identificacion as nombre')->lists('nombre','id');
      $nacionalidades=Nacionalidad::select('id_nacionalidad as id','nombre_nacionalidad as nombre')->lists('nombre','id');
      //dd($tipo_identificaciones);
      return view('persona.crear',compact('tipo_identificaciones','nacionalidades'));
  }

  public function guardar(Request $request)
  {
    $this->validate($request, [
      'nombre' => 'required',
      'apellido' => 'required',
      'identificacion' => 'required|unique:personas,identificacion_persona,null,id_persona',
      'tipo_identificacion_id' => 'required',
      'nacionalidad_id' => 'required',
      'correo' => 'required|email|unique:personas,correo_persona',
      'telefono_local'=> 'min:11|numeric',
      'telefono_celular'=> 'min:11|numeric'
    ]);

    $persona = new Persona();
    $persona->nombre_persona=$request->nombre;
    $persona->apellido_persona=$request->apellido;
    $persona->identificacion_persona=$request->identificacion;
    $persona->tipo_identificacion_id=$request->tipo_identificacion_id;
    $persona->nacionalidad_id=$request->nacionalidad_id;
    $persona->correo_persona=$request->correo;
    $persona->telefono_local_persona=$request->telefono_local;
    $persona->telefono_celular_persona=$request->telefono_celular;
    $persona->save();

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('persona.listado')->with('mensaje_ok',$ok);

  }

  public function editar($id)
  {
    $persona= Persona::withTrashed()->with(['documento'=> function ($query) {
      $query->withTrashed();
    },'nacionalidad' => function ($query) {
      $query->withTrashed();
    }])->where('id_persona', $id)->first();
    $tipo_identificaciones=TipoIdentificacion::select('id_tipo_identificacion as id','nombre_tipo_identificacion as nombre')->lists('nombre','id');
    $nacionalidades=Nacionalidad::select('id_nacionalidad as id','nombre_nacionalidad as nombre')->lists('nombre','id');
    return view('persona.editar',compact('persona','tipo_identificaciones','nacionalidades'));
  }

  public function detalle($id)
  {
    $persona= Persona::withTrashed()->with(['documento'=> function ($query) {
      $query->withTrashed();
    },'nacionalidad' => function ($query) {
      $query->withTrashed();
    }])->where('id_persona', $id)->first();

    return view('persona.detalle',compact('persona'));
  }

  public function actualizar(Request $request, $id)
  {
    $this->validate($request, [
      'nombre' => 'required',
      'apellido' => 'required',
      'identificacion' => 'required|unique:personas,identificacion_persona,'.$id.',id_persona',
      'tipo_identificacion_id' => '',
      'nacionalidad_id' => '',
      'correo' => 'required|email|unique:personas,correo_persona,'.$id.',id_persona',
      'telefono_local'=> 'min:11|numeric',
      'telefono_celular'=> 'min:11|numeric',
    ]);

    //dd('hola');

    $persona = Persona::withTrashed()->where('id_persona','=',$id)
        ->update([
          'nombre_persona' => $request->nombre,
          'apellido_persona'=>$request->apellido,
          'identificacion_persona'=>$request->identificacion,

          'correo_persona'=>$request->correo,
          'telefono_local_persona'=>$request->telefono_local,
          'telefono_celular_persona'=>$request->telefono_celular,
        ]);

        if(!empty($request->tipo_identificacion_id)){
          $persona = Persona::withTrashed()->where('id_persona','=',$id)
              ->update([
                'tipo_identificacion_id'=>$request->tipo_identificacion_id
              ]);
        }

        if(!empty($request->nacionalidad_id)){
          $persona = Persona::withTrashed()->where('id_persona','=',$id)
              ->update([
                'nacionalidad_id'=>$request->nacionalidad_id
              ]);
        }

    /*$persona = Persona::withTrashed()->where('id_persona', $id)->first();
    $persona->nombre_persona=$request->nombre;
    $persona->apellido_persona=$request->apellido;
    $persona->identificacion_persona=$request->identificacion;
    if(!empty($request->tipo_identificacion_id)){
      $persona->tipo_identificacion_id=$request->tipo_identificacion_id;
    }
    if(!empty($request->nacionalidad_id)){
      $persona->nacionalidad_id=$request->nacionalidad_id;
    }
    $persona->correo_persona=$request->correo;
    $persona->telefono_local_persona=$request->telefono_local;
    $persona->telefono_celular_persona=$request->telefono_celular;
    $persona->save();*/

    $ok='Excelente! se ha actualizado el registro satisfactoriamente';
    return redirect()->route('persona.listado')->with('mensaje_ok',$ok);
  }

  public function eliminar($id){
    $persona = Persona::withTrashed()->where('id_persona', $id)->delete();
    //$persona->delete();
    $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
    return redirect()->route('persona.listado')->with('mensaje_ok',$ok);
  }

  public function activar($id){
    $persona = Persona::withTrashed()->where('id_persona', $id)->restore();
    $ok='Excelente! se ha activado el registro satisfactoriamente';
    return redirect()->route('persona.listado')->with('mensaje_ok',$ok);
  }
}
