<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Arbitraje;
use App\Evento;
use App\Articulo;
use App\Arbitro;
use App\Estatu;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ArbitrajeController extends Controller
{
  public function listado()
  {
    $arbitrajes= Arbitraje::withTrashed()->with(['evento' => function ($query) {
          $query->withTrashed();
        },'articulo' => function ($query) {
          $query->withTrashed();
        },'arbitro' => function ($query) {
          $query->withTrashed();
        },'estatu' => function ($query) {
          $query->withTrashed();
        }
    ])->get();

    //dd($invitados);

    return view('arbitraje.listado',compact('arbitrajes'));
  }

  public function crear()
  {
      $eventos=Evento::select('id_evento as id','nombre_evento as nombre')->lists('nombre','id');
      $articulos=Articulo::select('id_articulo as id',DB::raw('concat(codigo_articulo," - ",nombre_articulo) as nombre'))->lists('nombre','id');
      $estatus=Estatu::select('id_estatu as id','nombre_estatu as nombre')->lists('nombre','id');

      $arbitros_data= Arbitro::with('persona')->get();
      $arbitros=[];
      foreach ($arbitros_data as  $value) {
        $arbitros[]=['nombre'=>$value->persona->identificacion_persona.
        ' - '.$value->persona->nombre_persona.' '.$value->persona->apellido_persona,
        'id'=>$value->id_arbitro];
      }

      $collection = collect($arbitros);

      $plucked = $collection->pluck('nombre','id');

      $arbitros=$plucked->all();

      //dd($arbitros);
      return view('arbitraje.crear',compact('eventos', 'articulos', 'estatus', 'arbitros'));
  }

  public function guardar(Request $request)
  {

    $this->validate($request, [
      'evento_id'=> 'required',
      'arbitro_id'=> 'required',
      'articulo_id'=> 'required',
      'estatu_id'=> 'required'
    ]);

    $arbitraje = new Arbitraje();
    $arbitraje->evento_id = $request->evento_id;
    $arbitraje->arbitro_id = $request->arbitro_id;
    $arbitraje->articulo_id = $request->articulo_id;
    $arbitraje->estatu_id = $request->estatu_id;
    $arbitraje->save();

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('arbitraje.listado')->with('mensaje_ok',$ok);
  }

  public function detalle($id)
  {
    $arbitraje= Arbitraje::withTrashed()->with(['evento' => function ($query) {
          $query->withTrashed();
        },'articulo' => function ($query) {
          $query->withTrashed();
        },'arbitro' => function ($query) {
          $query->withTrashed();
          $query->with(['persona' => function ($query) {
                $query->withTrashed();
          }]);
        },'estatu' => function ($query) {
          $query->withTrashed();
        }
    ])->where('id_arbitraje', $id)->first();

    //dd($arbitraje);
    return view('arbitraje.detalle',compact('arbitraje'));
  }

  public function editar($id)
  {
    $arbitraje= Arbitraje::withTrashed()->with(['evento' => function ($query) {
          $query->withTrashed();
        },'articulo' => function ($query) {
          $query->withTrashed();
        },'arbitro' => function ($query) {
          $query->withTrashed();
          $query->with(['persona' => function ($query) {
                $query->withTrashed();
          }]);
        },'estatu' => function ($query) {
          $query->withTrashed();
        }
    ])->where('id_arbitraje', $id)->first();

    $eventos=Evento::select('id_evento as id','nombre_evento as nombre')->lists('nombre','id');
    $articulos=Articulo::select('id_articulo as id',DB::raw('concat(codigo_articulo," - ",nombre_articulo) as nombre'))->lists('nombre','id');
    $estatus=Estatu::select('id_estatu as id','nombre_estatu as nombre')->lists('nombre','id');

    $arbitros_data= Arbitro::with('persona')->get();
    foreach ($arbitros_data as  $value) {
      $arbitros[]=['nombre'=>$value->persona->identificacion_persona.
      ' - '.$value->persona->nombre_persona.' '.$value->persona->apellido_persona,
      'id'=>$value->id_arbitro];
    }

    $collection = collect($arbitros);

    $plucked = $collection->pluck('nombre','id');

    $arbitros=$plucked->all();

    //dd($arbitros);
    return view('arbitraje.editar',compact('arbitraje','eventos', 'articulos', 'estatus', 'arbitros'));
  }

  public function actualizar(Request $request, $id)
  {


    if(!empty($request->evento_id)){
      $evento_id = Arbitraje::withTrashed()->where('id_arbitraje','=',$id)
          ->update([
            'evento_id'=>$request->evento_id
          ]);
    }

    if(!empty($request->arbitro_id)){
      $arbitro_id = Arbitraje::withTrashed()->where('id_arbitraje','=',$id)
          ->update([
            'arbitro_id'=>$request->arbitro_id
          ]);
    }

    if(!empty($request->articulo_id)){
      $articulo_id = Arbitraje::withTrashed()->where('id_arbitraje','=',$id)
          ->update([
            'articulo_id'=>$request->articulo_id
          ]);
    }

    if(!empty($request->estatu_id)){
      $estatu_id = Arbitraje::withTrashed()->where('id_arbitraje','=',$id)
          ->update([
            'estatu_id'=>$request->estatu_id
          ]);
    }

    $ok='Excelente! se ha actualizado el registro satisfactoriamente';
    return redirect()->route('arbitraje.listado')->with('mensaje_ok',$ok);

  }



  public function eliminar($id){
    $arbitraje = Arbitraje::withTrashed()->where('id_arbitraje', $id)->delete();

    $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
    return redirect()->route('arbitraje.listado')->with('mensaje_ok',$ok);
  }

  public function activar($id){
    $arbitraje = Arbitraje::withTrashed()->where('id_arbitraje', $id)->restore();
    $ok='Excelente! se ha activado el registro satisfactoriamente';
    return redirect()->route('arbitraje.listado')->with('mensaje_ok',$ok);
  }
}
