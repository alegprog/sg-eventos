<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TipoIdentificacion;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TipoIdentificacionController extends Controller
{
  public function listado()
  {
      $tipo_identificaciones= TipoIdentificacion::withTrashed()->get();
      return view('tipo_identificacion.listado',compact('tipo_identificaciones'));
  }

  public function crear()
  {
      return view('tipo_identificacion.crear');
  }

  public function guardar(Request $request)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:tipo_identificaciones,nombre_tipo_identificacion,null,id_tipo_identificacion|max:255'
    ]);

    $tipo_identificacion = new TipoIdentificacion();
    $tipo_identificacion->nombre_tipo_identificacion=$request->nombre;
    $tipo_identificacion->save();

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('tipo_identificacion.listado')->with('mensaje_ok',$ok);

  }

  public function editar($id)
  {
    $tipo_identificacion= TipoIdentificacion::withTrashed()->where('id_tipo_identificacion','=',$id)->first();
    return view('tipo_identificacion.editar',compact('tipo_identificacion'));
  }

  public function actualizar(Request $request, $id)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:tipo_identificaciones,nombre_tipo_identificacion,'.$id.',id_tipo_identificacion|max:255'
    ]);

    $tipo_identificacion = TipoIdentificacion::withTrashed()->where('id_tipo_identificacion','=',$id)
        ->update(['nombre_tipo_identificacion' => $request->nombre]);

    /*$tipo_identificacion = TipoIdentificacion::withTrashed()->where('id','=',$id)->first();
    $tipo_identificacion->nombre_tipo_identificacion=$request->nombre;
    $tipo_identificacion->save();*/

    $ok='Excelente! se ha actualizado el registro satisfactoriamente';
    return redirect()->route('tipo_identificacion.listado')->with('mensaje_ok',$ok);
  }

  public function eliminar($id){
    $tipo_identificacion = TipoIdentificacion::withTrashed()->where('id_tipo_identificacion', $id)->delete();
    //$tipo_identificacion->delete();
    $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
    return redirect()->route('tipo_identificacion.listado')->with('mensaje_ok',$ok);
  }

  public function activar($id){
    $tipo_identificacion = TipoIdentificacion::withTrashed()->where('id_tipo_identificacion', $id)->restore();
    $ok='Excelente! se ha activado el registro satisfactoriamente';
    return redirect()->route('tipo_identificacion.listado')->with('mensaje_ok',$ok);
  }
}
