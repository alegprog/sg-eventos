<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Evento;
use App\Persona;
use App\Articulo;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ArticuloController extends Controller
{
  public function listado()
  {
    $articulos= Articulo::withTrashed()->with(['persona' => function ($query) {
          $query->withTrashed();
        },'evento' => function ($query) {
          $query->withTrashed();
        }
    ])->get();

    //dd($invitados);

    return view('articulo.listado',compact('articulos'));
  }

  public function crear()
  {
      $eventos=Evento::select('id_evento as id','nombre_evento as nombre')->lists('nombre','id');
      $personas=Persona::select('id_persona as id',DB::raw('concat(identificacion_persona," - ",nombre_persona," ",apellido_persona) as nombre'))->lists('nombre','id');
      //dd($tipo_identificaciones);
      return view('articulo.crear',compact('eventos', 'personas'));
  }

  public function guardar(Request $request)
  {

    $this->validate($request, [
      'evento_id'=> 'required',
      'nombre'=> 'required',
      'persona_id'=> 'required',
      'coautor1_id'=> 'different:persona_id|different:coautor2_id',
      'coautor2_id'=> 'different:persona_id|different:coautor1_id',
      'codigo'=> 'required|unique:articulos,codigo_articulo,null,id_articulo',
    ]);

    /*if($request->persona_id == $request->coautor1_id){
      dd('error');
    }*/

    if(empty($request->coautor1_id)){
      $coautor_1_id=NULL;
    }else{
      $coautor_1_id=$request->coautor1_id;
    }

    if(empty($request->coautor2_id)){
      $coautor_2_id=NULL;
    }else{
      $coautor_2_id=$request->coautor2_id;
    }

    $articulo = new Articulo();
    $articulo->evento_id = $request->evento_id;
    $articulo->persona_id = $request->persona_id;
    $articulo->coautor_1_id = $coautor_1_id;
    $articulo->coautor_2_id = $coautor_2_id;
    $articulo->nombre_articulo = $request->nombre;
    $articulo->codigo_articulo = $request->codigo;
    $articulo->save();

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('articulo.listado')->with('mensaje_ok',$ok);
  }

  public function detalle($id)
  {
    $articulo= Articulo::withTrashed()->with(['persona' => function ($query) {
          $query->withTrashed();
        },'evento' => function ($query) {
          $query->withTrashed();
        },'coautor_principal' => function ($query) {
          $query->withTrashed();
        },'coautor_segundario' => function ($query) {
          $query->withTrashed();
        }
    ])->where('id_articulo', $id)->first();

    //dd($articulo);
    return view('articulo.detalle',compact('articulo'));
  }

  public function editar($id)
  {
    $articulo= Articulo::withTrashed()->with(['persona' => function ($query) {
          $query->withTrashed();
        },'evento' => function ($query) {
          $query->withTrashed();
        },'coautor_principal' => function ($query) {
          $query->withTrashed();
        },'coautor_segundario' => function ($query) {
          $query->withTrashed();
        }
    ])->where('id_articulo', $id)->first();
    $eventos=Evento::select('id_evento as id','nombre_evento as nombre')->lists('nombre','id');
    $personas=Persona::select('id_persona as id',DB::raw('concat(identificacion_persona," - ",nombre_persona," ",apellido_persona) as nombre'))->lists('nombre','id');

    return view('articulo.editar',compact('articulo','eventos', 'personas'));
  }

  public function actualizar(Request $request, $id)
  {

    $this->validate($request, [
      'evento_id'=> 'required',
      'nombre'=> 'required',
      'persona_id'=> 'required',
      'coautor1_id'=> 'different:persona_id|different:coautor2_id',
      'coautor2_id'=> 'different:persona_id|different:coautor1_id',
      'codigo'=> 'required|unique:articulos,codigo_articulo,'.$request->id.',id_articulo',
    ]);

    if(empty($request->coautor1_id)){
      $coautor_1_id=NULL;
    }else{
      $coautor_1_id=$request->coautor1_id;
    }

    if(empty($request->coautor2_id)){
      $coautor_2_id=NULL;
    }else{
      $coautor_2_id=$request->coautor2_id;
    }

    //dd($request->all());

    $articulo = Articulo::withTrashed()->where('id_articulo','=',$id)
        ->update([
          'nombre_articulo' => $request->nombre,
          'codigo_articulo' => $request->codigo,
          'coautor_1_id' => $coautor_1_id,
          'coautor_2_id' => $coautor_2_id,
        ]);

    if(!empty($request->evento_id)){
      $evento_id = Articulo::withTrashed()->where('id_articulo','=',$id)
          ->update([
            'evento_id'=>$request->evento_id
          ]);
    }

    if(!empty($request->persona_id)){
      $persona_id = Articulo::withTrashed()->where('id_articulo','=',$id)
          ->update([
            'persona_id'=>$request->persona_id
          ]);
    }

    $ok='Excelente! se ha actualizado el registro satisfactoriamente';
    return redirect()->route('articulo.listado')->with('mensaje_ok',$ok);

  }

  public function eliminar($id){
    $articulo = Articulo::withTrashed()->where('id_articulo', $id)->delete();

    $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
    return redirect()->route('articulo.listado')->with('mensaje_ok',$ok);
  }

  public function activar($id){
    $articulo = Articulo::withTrashed()->where('id_articulo', $id)->restore();
    $ok='Excelente! se ha activado el registro satisfactoriamente';
    return redirect()->route('articulo.listado')->with('mensaje_ok',$ok);
  }
}
