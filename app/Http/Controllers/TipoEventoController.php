<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TipoEvento;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TipoEventoController extends Controller
{
    public function listado()
    {
        $tipo_eventos=TipoEvento::withTrashed()->get();
        return view('tipo_evento.listado',compact('tipo_eventos'));
    }

    public function crear()
    {
        return view('tipo_evento.crear');
    }

    public function guardar(Request $request)
    {
      $this->validate($request, [
        'nombre' => 'required|unique:tipo_eventos,nombre_tipo_evento,null,id_tipo_evento|max:255'
      ]);

      $tipo_evento = new TipoEvento();
      $tipo_evento->nombre_tipo_evento=$request->nombre;
      $tipo_evento->save();

      $ok='Excelente! se ha creado satisfactoriamente';
      return redirect()->route('tipo_evento.listado')->with('mensaje_ok',$ok);

    }

    public function editar($id)
    {
      $tipo_evento= TipoEvento::withTrashed()->where('id_tipo_evento','=',$id)->first();
      return view('tipo_evento.editar',compact('tipo_evento'));
    }

    public function actualizar(Request $request, $id)
    {
      $this->validate($request, [
        'nombre' => 'required|unique:tipo_eventos,nombre_tipo_evento,'.$id.',id_tipo_evento|max:255'
      ]);

      /*$tipo_evento = TipoEvento::withTrashed()->where('id_tipo_evento','=',$id)->first();
      $tipo_evento->nombre_tipo_evento=$request->nombre;
      $tipo_evento->save();*/

      $tipo_evento = TipoEvento::withTrashed()->where('id_tipo_evento','=',$id)
          ->update(['nombre_tipo_evento' => $request->nombre]);

      $ok='Excelente! se ha actualizado el registro satisfactoriamente';
      return redirect()->route('tipo_evento.listado')->with('mensaje_ok',$ok);
    }

    public function eliminar($id){
      $tipo_evento = TipoEvento::withTrashed()->where('id_tipo_evento', $id)->delete();
      //dd($tipo_evento);
      //$tipo_evento;
      $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
      return redirect()->route('tipo_evento.listado')->with('mensaje_ok',$ok);
    }

    public function activar($id){
      $tipo_evento = TipoEvento::withTrashed()->where('id_tipo_evento', $id)->restore();
      //$tipo_evento->restore();
      $ok='Excelente! se ha activado el registro satisfactoriamente';
      return redirect()->route('tipo_evento.listado')->with('mensaje_ok',$ok);
    }

}
