<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Nacionalidad;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class NacionalidadController extends Controller
{
  public function listado()
  {
      $nacionalidades= Nacionalidad::withTrashed()->get();
      return view('nacionalidad.listado',compact('nacionalidades'));
  }

  public function crear()
  {
      return view('nacionalidad.crear');
  }

  public function guardar(Request $request)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:nacionalidades,nombre_nacionalidad,null,id_nacionalidad|max:255'
    ]);

    $nacionalidad = new Nacionalidad();
    $nacionalidad->nombre_nacionalidad=$request->nombre;
    $nacionalidad->save();

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('nacionalidad.listado')->with('mensaje_ok',$ok);

  }

  public function editar($id)
  {
    $nacionalidad= Nacionalidad::withTrashed()->where('id_nacionalidad','=',$id)->first();
    return view('nacionalidad.editar',compact('nacionalidad'));
  }

  public function actualizar(Request $request, $id)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:nacionalidades,nombre_nacionalidad,'.$id.',id_nacionalidad|max:255'
    ]);

    /*$nacionalidad = Nacionalidad::withTrashed()->where('id_nacionalidad','=',$id)->first();
    $nacionalidad->nombre_nacionalidad=$request->nombre;
    $nacionalidad->save();*/

    $nacionalidad = Nacionalidad::withTrashed()->where('id_nacionalidad','=',$id)
        ->update(['nombre_nacionalidad' => $request->nombre]);

    $ok='Excelente! se ha actualizado el registro satisfactoriamente';
    return redirect()->route('nacionalidad.listado')->with('mensaje_ok',$ok);
  }

  public function eliminar($id){
    $nacionalidad = Nacionalidad::withTrashed()->where('id_nacionalidad', $id)->delete();
    //$nacionalidad->delete();
    $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
    return redirect()->route('nacionalidad.listado')->with('mensaje_ok',$ok);
  }

  public function activar($id){
    $nacionalidad = Nacionalidad::withTrashed()->where('id_nacionalidad', $id)->restore();
    $ok='Excelente! se ha activado el registro satisfactoriamente';
    return redirect()->route('nacionalidad.listado')->with('mensaje_ok',$ok);
  }
}
