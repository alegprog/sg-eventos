<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Requerimiento;
use App\Evento;
use App\TipoRequerimiento;
use App\Responsable;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RequerimientoController extends Controller
{
  public function listado()
  {
    //$requerimientos= Requerimiento::all();
    $requerimientos= Requerimiento::withTrashed()->with(['evento' => function ($query) {
          $query->withTrashed();
        },'tipo_requerimiento' => function ($query) {
          $query->withTrashed();
        },'responsable' => function ($query) {
          $query->withTrashed();
        }
    ])->get();

    //dd($invitados);

    return view('requerimiento.listado',compact('requerimientos'));
  }

  public function crear()
  {
      $eventos=Evento::select('id_evento as id','nombre_evento as nombre')->lists('nombre','id');
      $tipo_requerimientos=TipoRequerimiento::select('id_tipo_requerimiento as id','nombre_tipo_requerimiento as nombre')->lists('nombre','id');
      $responsables=Responsable::select('id_responsable as id','nombre_responsable as nombre')->lists('nombre','id');


      //dd($arbitros);
      return view('requerimiento.crear',compact('eventos', 'tipo_requerimientos', 'responsables'));
  }

  public function guardar(Request $request)
  {

    $this->validate($request, [
      'descripcion'=> 'required',
      'cantidad'=> 'required',
      'evento_id'=> 'required',
      'tipo_requerimiento_id'=> 'required|unique:requerimientos,tipo_requerimiento_id,null,id_requerimiento,evento_id,'.$request->evento_id.'',
      'responsable_id'=> 'required'
    ]);

    $requerimiento = new Requerimiento();
    $requerimiento->descripcion_requerimiento = $request->descripcion;
    $requerimiento->evento_id = $request->evento_id;
    $requerimiento->cantidad_requerimiento = $request->cantidad;
    $requerimiento->tipo_requerimiento_id = $request->tipo_requerimiento_id;
    $requerimiento->responsable_id = $request->responsable_id;
    $requerimiento->save();

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('requerimiento.listado')->with('mensaje_ok',$ok);
  }

  public function actualizar(Request $request, $id)
  {

    $this->validate($request, [
      'descripcion'=> 'required',
      'cantidad'=> 'required'
    ]);

    $requerimiento = Requerimiento::withTrashed()->where('id_requerimiento','=',$id)
        ->update([
          'descripcion_requerimiento' => $request->descripcion,
          'cantidad_requerimiento' => $request->cantidad
        ]);

    //dd(Requerimiento::withTrashed()->where('id_requerimiento','=',$id));

    if(!empty($request->evento_id)){
      $evento_id = Requerimiento::withTrashed()->where('id_requerimiento','=',$id)
          ->update([
            'evento_id'=>$request->evento_id
          ]);
    }

    if(!empty($request->tipo_requerimiento_id)){
      $tipo_requerimiento_id = Requerimiento::withTrashed()->where('id_requerimiento','=',$id)
          ->update([
            'tipo_requerimiento_id'=>$request->tipo_requerimiento_id
          ]);
    }

    if(!empty($request->articulo_id)){
      $responsable_id = Requerimiento::withTrashed()->where('id_requerimiento','=',$id)
          ->update([
            'responsable_id'=>$request->responsable_id
          ]);
    }



    $ok='Excelente! se ha actualizado el registro satisfactoriamente';
    return redirect()->route('requerimiento.listado')->with('mensaje_ok',$ok);

  }

  public function detalle($id)
  {
    $requerimiento= Requerimiento::withTrashed()->with(['evento' => function ($query) {
          $query->withTrashed();
        },'tipo_requerimiento' => function ($query) {
          $query->withTrashed();
        },'responsable' => function ($query) {
          $query->withTrashed();
        }
    ])->where('id_requerimiento', $id)->first();

    //dd($arbitraje);
    return view('requerimiento.detalle',compact('requerimiento'));
  }

  public function editar($id)
  {
    $requerimiento= Requerimiento::withTrashed()->with(['evento' => function ($query) {
          $query->withTrashed();
        },'tipo_requerimiento' => function ($query) {
          $query->withTrashed();
        },'responsable' => function ($query) {
          $query->withTrashed();
        }
    ])->where('id_requerimiento', $id)->first();

    $eventos=Evento::select('id_evento as id','nombre_evento as nombre')->lists('nombre','id');
    $tipo_requerimientos=TipoRequerimiento::select('id_tipo_requerimiento as id','nombre_tipo_requerimiento as nombre')->lists('nombre','id');
    $responsables=Responsable::select('id_responsable as id','nombre_responsable as nombre')->lists('nombre','id');

    return view('requerimiento.editar',compact('requerimiento','eventos', 'tipo_requerimientos', 'responsables'));
  }

  public function eliminar($id){
    $requerimiento = Requerimiento::withTrashed()->where('id_requerimiento', $id)->delete();
    $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
    return redirect()->route('requerimiento.listado')->with('mensaje_ok',$ok);
  }

  public function activar($id){
    $requerimiento = Requerimiento::withTrashed()->where('id_requerimiento', $id)->restore();
    $ok='Excelente! se ha activado el registro satisfactoriamente';
    return redirect()->route('requerimiento.listado')->with('mensaje_ok',$ok);
  }


}
