<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TipoArbitro;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TipoArbitroController extends Controller
{
  public function listado()
  {
      $tipo_arbitros= TipoArbitro::withTrashed()->get();
      return view('tipo_arbitro.listado',compact('tipo_arbitros'));
  }

  public function crear()
  {
      return view('tipo_arbitro.crear');
  }

  public function guardar(Request $request)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:tipo_arbitros,nombre_tipo_arbitro,null,id_tipo_arbitro|max:255'
    ]);

    $tipo_arbitro = new TipoArbitro();
    $tipo_arbitro->nombre_tipo_arbitro=$request->nombre;
    $tipo_arbitro->save();

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('tipo_arbitro.listado')->with('mensaje_ok',$ok);

  }

  public function editar($id)
  {
    $tipo_arbitro= TipoArbitro::withTrashed()->where('id_tipo_arbitro','=',$id)->first();
    return view('tipo_arbitro.editar',compact('tipo_arbitro'));
  }

  public function actualizar(Request $request, $id)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:tipo_arbitros,nombre_tipo_arbitro,'.$id.',id_tipo_arbitro|max:255'
    ]);

    /*$tipo_arbitro = TipoArbitro::withTrashed()->where('id_tipo_evento','=',$id)->first();
    $tipo_arbitro->nombre_tipo_arbitro=$request->nombre;
    $tipo_arbitro->save();*/
    $tipo_arbitro = TipoArbitro::withTrashed()->where('id_tipo_arbitro','=',$id)
        ->update(['nombre_tipo_arbitro' => $request->nombre]);


    $ok='Excelente! se ha actualizado el registro satisfactoriamente';
    return redirect()->route('tipo_arbitro.listado')->with('mensaje_ok',$ok);
  }

  public function eliminar($id){
    $tipo_arbitro = TipoArbitro::withTrashed()->where('id_tipo_arbitro','=',$id)->delete();
    //$tipo_arbitro->delete();
    $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
    return redirect()->route('tipo_arbitro.listado')->with('mensaje_ok',$ok);
  }

  public function activar($id){
    $tipo_arbitro = TipoArbitro::withTrashed()->where('id_tipo_arbitro', $id)->restore();
    $ok='Excelente! se ha activado el registro satisfactoriamente';
    return redirect()->route('tipo_arbitro.listado')->with('mensaje_ok',$ok);
  }
}
