<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usuario;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsuarioController extends Controller
{
  public function listado()
  {
      $usuarios= Usuario::withTrashed()->get();
      return view('usuario.listado',compact('usuarios'));
  }

  public function crear()
  {
      return view('usuario.crear');
  }

  public function guardar(Request $request)
  {
    $this->validate($request, [
      'nombre' => 'required',
      'apellido' => 'required',
      'cedula' => 'required|unique:usuarios,cedula_usuario|min:7|numeric',
      'usuario' => 'required|unique:usuarios|min:4|alpha_dash',
      'correo' => 'required|email|unique:usuarios,correo_usuario',
      'clave'=> 'required|min:6|confirmed:clave',
      'clave_confirmation'=> 'required',
      'tipo'=> 'required',
    ]);

    $usuario = new Usuario();
    $usuario->nombre_usuario=$request->nombre;
    $usuario->apellido_usuario=$request->apellido;
    $usuario->cedula_usuario=$request->cedula;
    $usuario->correo_usuario=$request->correo;
    $usuario->usuario=strtolower($request->usuario);
    $usuario->password=bcrypt($request->clave);
    $usuario->tipo_usuario=$request->tipo;
    $usuario->save();

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('usuario.listado')->with('mensaje_ok',$ok);

  }

  public function editar($id)
  {
    $usuario= Usuario::withTrashed()->where('id','=',$id)->first();
    return view('usuario.editar',compact('usuario'));
  }

  public function detalle($id)
  {
    $usuario= Usuario::withTrashed()->where('id','=',$id)->first();
    return view('usuario.detalle',compact('usuario'));
  }

  public function actualizar(Request $request, $id)
  {
    $this->validate($request, [
      'nombre' => 'required',
      'apellido' => 'required',
      'cedula' => 'required|unique:usuarios,cedula_usuario,'.$id.'|min:7|numeric',
      'usuario' => 'required|unique:usuarios,usuario,'.$id.'|min:4|alpha_dash',
      'correo' => 'required|email|unique:usuarios,correo_usuario,'.$id.'',
      'clave'=> 'min:6|confirmed',
      'clave_confirmation'=> '',
      'tipo'=> 'required',
    ]);

    $usuario = Usuario::withTrashed()->where('id','=',$id)->first();
    $usuario->nombre_usuario=$request->nombre;
    $usuario->apellido_usuario=$request->apellido;
    $usuario->cedula_usuario=$request->cedula;
    $usuario->correo_usuario=$request->correo;
    $usuario->usuario=strtolower($request->usuario);
    if(!empty($request->clave)){
      $usuario->password=bcrypt($request->clave);
    }
    $usuario->tipo_usuario=$request->tipo;
    $usuario->save();

    $ok='Excelente! se ha actualizado el registro satisfactoriamente';
    return redirect()->route('usuario.listado')->with('mensaje_ok',$ok);
  }

  public function eliminar($id){
    $usuario =Usuario::withTrashed()->where('id','=',$id)->delete();
    //$usuario->delete();
    $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
    return redirect()->route('usuario.listado')->with('mensaje_ok',$ok);
  }

  public function activar($id){
    $usuario = Usuario::withTrashed()->where('id', $id)->restore();
    $ok='Excelente! se ha activado el registro satisfactoriamente';
    return redirect()->route('usuario.listado')->with('mensaje_ok',$ok);
  }

}
