<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TipoRequerimiento;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TipoRequerimientoController extends Controller
{
  public function listado()
  {
      $tipo_requerimientos= TipoRequerimiento::withTrashed()->get();
      return view('tipo_requerimiento.listado',compact('tipo_requerimientos'));
  }

  public function crear()
  {
      return view('tipo_requerimiento.crear');
  }

  public function guardar(Request $request)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:tipo_requerimientos,nombre_tipo_requerimiento,null,id_tipo_requerimiento|max:255'
    ]);

    $tipo_requerimiento = new TipoRequerimiento();
    $tipo_requerimiento->nombre_tipo_requerimiento=$request->nombre;
    $tipo_requerimiento->save();

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('tipo_requerimiento.listado')->with('mensaje_ok',$ok);

  }

  public function editar($id)
  {
    $tipo_requerimiento= TipoRequerimiento::withTrashed()->where('id_tipo_requerimiento','=',$id)->first();
    return view('tipo_requerimiento.editar',compact('tipo_requerimiento'));
  }

  public function actualizar(Request $request, $id)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:tipo_requerimientos,nombre_tipo_requerimiento,'.$id.',id_tipo_requerimiento|max:255'
    ]);

    /*$tipo_requerimiento = TipoRequerimiento::withTrashed()->where('id_tipo_requerimiento','=',$id)->first();
    $tipo_requerimiento->nombre_requerimiento=$request->nombre;
    $tipo_requerimiento->save();*/

    $tipo_requerimiento = TipoRequerimiento::withTrashed()->where('id_tipo_requerimiento','=',$id)
        ->update(['nombre_tipo_requerimiento' => $request->nombre]);

    $ok='Excelente! se ha actualizado el registro satisfactoriamente';
    return redirect()->route('tipo_requerimiento.listado')->with('mensaje_ok',$ok);
  }

  public function eliminar($id){
    $tipo_requerimiento = TipoRequerimiento::withTrashed()->where('id_tipo_requerimiento', $id)->delete();
    //$tipo_requerimiento->delete();
    $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
    return redirect()->route('tipo_requerimiento.listado')->with('mensaje_ok',$ok);
  }

  public function activar($id){
    $tipo_requerimiento = TipoRequerimiento::withTrashed()->where('id_tipo_requerimiento', $id)->restore();
    $ok='Excelente! se ha activado el registro satisfactoriamente';
    return redirect()->route('tipo_requerimiento.listado')->with('mensaje_ok',$ok);
  }

}
