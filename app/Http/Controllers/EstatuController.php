<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Estatu;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class EstatuController extends Controller
{
  public function listado()
  {
      $estatus= Estatu::withTrashed()->get();
      return view('estatu.listado',compact('estatus'));
  }

  public function crear()
  {
      return view('estatu.crear');
  }

  public function guardar(Request $request)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:estatus,nombre_estatu,null,id_estatu|max:255'
    ]);

    $estatu = new Estatu();
    $estatu->nombre_estatu=$request->nombre;
    $estatu->save();

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('estatu.listado')->with('mensaje_ok',$ok);

  }

  public function editar($id)
  {
    $estatu= Estatu::withTrashed()->where('id_estatu','=',$id)->first();
    return view('estatu.editar',compact('estatu'));
  }

  public function actualizar(Request $request, $id)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:estatus,nombre_estatu,'.$id.',id_estatu|max:255'
    ]);

    /*$estatu = Estatu::withTrashed()->where('id','=',$id)->first();
    $estatu->nombre_estatu=$request->nombre;
    $estatu->save();*/

    $estatu = Estatu::withTrashed()->where('id_estatu','=',$id)
        ->update(['nombre_estatu' => $request->nombre]);

    $ok='Excelente! se ha actualizado el registro satisfactoriamente';
    return redirect()->route('estatu.listado')->with('mensaje_ok',$ok);
  }

  public function eliminar($id){
    $estatu = Estatu::withTrashed()->where('id_estatu', $id)->delete();
    //$estatu->delete();
    $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
    return redirect()->route('estatu.listado')->with('mensaje_ok',$ok);
  }

  public function activar($id){
    $estatu = Estatu::withTrashed()->where('id_estatu', $id)->restore();
    $ok='Excelente! se ha activado el registro satisfactoriamente';
    return redirect()->route('estatu.listado')->with('mensaje_ok',$ok);
  }

}
