<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Persona;
use App\Dependencia;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DependenciaController extends Controller
{
  public function listado()
  {
      $dependencias= Dependencia::withTrashed()->get();
      return view('dependencia.listado',compact('dependencias'));
  }

  public function crear()
  {
      $personas=Persona::select('id_persona as id',DB::raw('concat(identificacion_persona," - ",nombre_persona," ",apellido_persona) as nombre'))->lists('nombre','id');
      return view('dependencia.crear',compact('personas'));
  }

  public function guardar(Request $request)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:dependencias,nombre_dependencia,null,id_dependencia|max:255',
      'firma_1_id'=> 'required|different:firma_2_id',
      'firma_2_id'=> 'required|different:firma_1_id',
      'firma_1_cargo'=> 'required',
      'firma_2_cargo'=> 'required',
    ]);

    $dependencia = new Dependencia();
    $dependencia->nombre_dependencia=$request->nombre;
    $dependencia->firma_1_id=$request->firma_1_id;
    $dependencia->firma_1_cargo=$request->firma_1_cargo;
    $dependencia->firma_1_antes=$request->firma_1_antes;
    $dependencia->firma_1_despues=$request->firma_1_despues;
    $dependencia->firma_2_id=$request->firma_2_id;
    $dependencia->firma_2_cargo=$request->firma_2_cargo;
    $dependencia->firma_2_antes=$request->firma_2_antes;
    $dependencia->firma_2_despues=$request->firma_2_despues;
    $dependencia->save();

    $ok='Excelente! se ha creado satisfactoriamente';
    return redirect()->route('dependencia.listado')->with('mensaje_ok',$ok);

  }

  public function editar($id)
  {
    $personas=Persona::select('id_persona as id',DB::raw('concat(identificacion_persona," - ",nombre_persona," ",apellido_persona) as nombre'))->lists('nombre','id');
    $dependencia= Dependencia::withTrashed()->where('id_dependencia','=',$id)->first();
    return view('dependencia.editar',compact('dependencia','personas'));
  }

  public function actualizar(Request $request, $id)
  {
    $this->validate($request, [
      'nombre' => 'required|unique:dependencias,nombre_dependencia,'.$id.',id_dependencia|max:255',
      'firma_1_id'=> 'required|different:firma_2_id',
      'firma_2_id'=> 'required|different:firma_1_id',
      'firma_1_cargo'=> 'required',
      'firma_2_cargo'=> 'required',
    ]);

    /*$dependencia = Dependencia::withTrashed()->where('id','=',$id)->first();
    $dependencia->nombre_dependencia=$request->nombre;
    $dependencia->save();*/

    $dependencia = Dependencia::withTrashed()->where('id_dependencia','=',$id)
        ->update(['nombre_dependencia' => $request->nombre,
        'firma_1_id'=>$request->firma_1_id,
        'firma_1_cargo'=>$request->firma_1_cargo,
        'firma_1_antes'=>$request->firma_1_antes,
        'firma_1_despues'=>$request->firma_1_despues,
        'firma_2_id'=>$request->firma_2_id,
        'firma_2_cargo'=>$request->firma_2_cargo,
        'firma_2_antes'=>$request->firma_2_antes,
        'firma_2_despues'=>$request->firma_2_despues]);

    $ok='Excelente! se ha actualizado el registro satisfactoriamente';
    return redirect()->route('dependencia.listado')->with('mensaje_ok',$ok);
  }

  public function eliminar($id){
    $dependencia = Dependencia::withTrashed()->where('id_dependencia', $id)->delete();
    //$dependencia->delete();
    $ok='Excelente! se ha bloqueado el registro satisfactoriamente';
    return redirect()->route('dependencia.listado')->with('mensaje_ok',$ok);
  }

  public function activar($id){
    $dependencia = Dependencia::withTrashed()->where('id_dependencia', $id)->restore();
    $ok='Excelente! se ha activado el registro satisfactoriamente';
    return redirect()->route('dependencia.listado')->with('mensaje_ok',$ok);
  }

}
