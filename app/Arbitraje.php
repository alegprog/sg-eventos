<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Arbitraje extends Model
{
    use SoftDeletes;

    public function evento()
    {
        return $this->belongsTo('App\Evento','evento_id','id_evento');
    }

    public function arbitro()
    {
        return $this->belongsTo('App\Arbitro','arbitro_id','id_arbitro');
    }

    public function articulo()
    {
        return $this->belongsTo('App\Articulo','articulo_id','id_articulo');
    }

    public function estatu()
    {
        return $this->belongsTo('App\Estatu','estatu_id','id_estatu');
    }
}
