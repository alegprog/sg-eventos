<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->increments('id_evento');
            $table->string('nombre_evento');
            $table->date('fecha_inicio_evento');
            $table->date('fecha_final_evento');
            $table->time('hora_inicio_evento');
            $table->time('hora_final_evento');
            $table->string('direccion_evento');
            $table->string('duracion_evento');
            $table->string('observacion_evento');
            $table->string('archivo');
            $table->integer('lugar_id')->unsigned();
            $table->foreign('lugar_id')->references('id_lugar')->on('lugares');
            $table->integer('tipo_evento_id')->unsigned();
            $table->foreign('tipo_evento_id')->references('id_tipo_evento')->on('tipo_eventos');
            $table->integer('dependencia_id')->unsigned();
            $table->foreign('dependencia_id')->references('id_dependencia')->on('dependencias');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('eventos');
    }
}
