<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCoautorArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articulos', function (Blueprint $table) {
          $table->integer('coautor_2_id')->unsigned()->nullable()->after('evento_id');
          $table->foreign('coautor_2_id')->references('id_persona')->on('personas');
          $table->integer('coautor_1_id')->unsigned()->nullable()->after('evento_id');
          $table->foreign('coautor_1_id')->references('id_persona')->on('personas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articulos', function (Blueprint $table) {
            $table->dropForeign(['coautor_2_id']);
            $table->dropForeign(['coautor_1_id']);
            $table->dropColumn(['coautor_1_id', 'coautor_2_id']);
        });
    }
}
