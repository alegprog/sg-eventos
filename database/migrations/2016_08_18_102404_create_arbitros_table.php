<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArbitrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arbitros', function (Blueprint $table) {
            $table->increments('id_arbitro');
            $table->integer('persona_id')->unsigned();
            $table->foreign('persona_id')->references('id_persona')->on('personas');
            /*$table->string('nombre');
            $table->string('apellido');
            $table->string('cedula');
            $table->string('correo');
            $table->string('telefono');*/
            //$table->string('procedencia_arbitro');
            $table->integer('tipo_arbitro_id')->unsigned();
            $table->foreign('tipo_arbitro_id')->references('id_tipo_arbitro')->on('tipo_arbitros');
            $table->integer('evento_id')->unsigned();
            $table->foreign('evento_id')->references('id_evento')->on('eventos');
            $table->integer('modalidad_id')->unsigned();
            $table->foreign('modalidad_id')->references('id_modalidad')->on('modalidades');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('arbitros');
    }
}
