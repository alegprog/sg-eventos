<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFirmaDependenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dependencias', function (Blueprint $table) {
          $table->integer('firma_2_id')->unsigned()->nullable()->after('nombre_dependencia');
          $table->foreign('firma_2_id')->references('id_persona')->on('personas');
          $table->string('firma_2_antes')->after('nombre_dependencia');
          $table->string('firma_2_despues')->after('nombre_dependencia');
          $table->string('firma_2_cargo')->after('nombre_dependencia');
          $table->integer('firma_1_id')->unsigned()->nullable()->after('nombre_dependencia');
          $table->foreign('firma_1_id')->references('id_persona')->on('personas');
          $table->string('firma_1_antes')->after('nombre_dependencia');
          $table->string('firma_1_despues')->after('nombre_dependencia');
          $table->string('firma_1_cargo')->after('nombre_dependencia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dependencias', function (Blueprint $table) {
          $table->dropForeign(['firma_2_id']);
          $table->dropForeign(['firma_1_id']);
          $table->dropColumn(['firma_1_id', 'firma_2_id','firma_1_antes','firma_2_antes']);
          $table->dropColumn(['firma_1_despues','firma_2_despues','firma_1_cargo','firma_2_cargo']);
        });
    }
}
