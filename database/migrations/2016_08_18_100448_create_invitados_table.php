<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitados', function (Blueprint $table) {
            $table->increments('id_invitado');
            $table->integer('persona_id')->unsigned();
            $table->foreign('persona_id')->references('id_persona')->on('personas');
            /*$table->string('nombre');
            $table->string('apellido');
            $table->string('identificacion');
            $table->string('correo');
            $table->string('telefono');*/
            $table->string('titulo_ponencia_invitado');
            //$table->string('procedencia_invitado');
            $table->string('archivo');
            $table->integer('tipo_invitado_id')->unsigned();
            $table->foreign('tipo_invitado_id')->references('id_tipo_invitado')->on('tipo_invitados');
            $table->integer('evento_id')->unsigned();
            $table->foreign('evento_id')->references('id_evento')->on('eventos');
            $table->integer('modalidad_id')->unsigned();
            $table->foreign('modalidad_id')->references('id_modalidad')->on('modalidades');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invitados');
    }
}
