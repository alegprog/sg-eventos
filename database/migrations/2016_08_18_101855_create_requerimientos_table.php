<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequerimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requerimientos', function (Blueprint $table) {
            $table->increments('id_requerimiento');
            $table->string('descripcion_requerimiento');
            $table->string('cantidad_requerimiento');
            $table->integer('evento_id')->unsigned();
            $table->foreign('evento_id')->references('id_evento')->on('eventos');
            $table->integer('tipo_requerimiento_id')->unsigned();
            $table->foreign('tipo_requerimiento_id')->references('id_tipo_requerimiento')->on('tipo_requerimientos');
            $table->integer('responsable_id')->unsigned();
            $table->foreign('responsable_id')->references('id_responsable')->on('responsables');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('requerimientos');
    }
}
