<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos', function (Blueprint $table) {
            $table->increments('id_articulo');
            $table->string('codigo_articulo');
            $table->string('nombre_articulo');
            //$table->string('procedencia_articulo');
            $table->integer('persona_id')->unsigned();
            $table->foreign('persona_id')->references('id_persona')->on('personas');
            $table->integer('evento_id')->unsigned();
            $table->foreign('evento_id')->references('id_evento')->on('eventos');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articulos');
    }
}
