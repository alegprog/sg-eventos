<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArbitrajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arbitrajes', function (Blueprint $table) {
            $table->increments('id_arbitraje');
            $table->integer('evento_id')->unsigned();
            $table->foreign('evento_id')->references('id_evento')->on('eventos');
            $table->integer('arbitro_id')->unsigned();
            $table->foreign('arbitro_id')->references('id_arbitro')->on('arbitros');
            $table->integer('articulo_id')->unsigned();
            $table->foreign('articulo_id')->references('id_articulo')->on('articulos');
            $table->integer('estatu_id')->unsigned();
            $table->foreign('estatu_id')->references('id_estatu')->on('estatus');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('arbitrajes');
    }
}
