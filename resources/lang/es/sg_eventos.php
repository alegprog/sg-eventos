<?php

  return array(
    'reporte'=>array(
      'constanciaInvitado'=>'dashboard/constancia-invitado/pdf/:id',
      'certificadoInvitado'=>'dashboard/certificado-invitado/pdf/:id',
      'constanciaArticulo'=>'dashboard/constancia-articulo/pdf/:id',
      'constanciaArbitro'=>'dashboard/constancia-arbitro/pdf/:id',
      'certificadoArbitro'=>'dashboard/constancia-por-modalidad-arbitro/pdf/:id',
    )
  );
