@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12">
             @if (session('mensaje_ok'))
             <div class="alert alert-success text-center">
                 {{ session('mensaje_ok') }}
             </div>
             @endif
          </div>
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('arbitraje.listado')}}" class="btn btn-warning">Regresar</a>
          </div>
          <h2 class="text-center f-35">Información del arbitraje</h2>
          <br>
          <div class="col-md-7">
              <table class="table table-bordered table-hover">
                <tr>
                  <th width="150">Evento:</th>
                  <td colspan="3">{{$arbitraje->evento->nombre_evento}}</td>
                <tr>
                <tr>
                    <th width="150">Articulo:</th>
                    <td colspan="3">{{$arbitraje->articulo->codigo_articulo}} - {{$arbitraje->articulo->nombre_articulo}}</td>
                <tr>
                <tr>
                    <th width="150">Estatu:</th>
                    <td colspan="3">{{$arbitraje->estatu->nombre_estatu}}</td>
                <tr>
                <tr>
                  <th width="150">Identificacion:</th>
                  <td colspan="3">{{$arbitraje->arbitro->persona->identificacion_persona}}</td>
                <tr>
                <tr>
                  <th width="160">Nombre del arbitro:</th>
                  <td colspan="3">{{$arbitraje->arbitro->persona->nombre_persona}} {{$arbitraje->arbitro->persona->apellido_persona}}</td>
                <tr>
                <tr>
                  <th width="150">Correo:</th>
                  <td colspan="3">{{$arbitraje->arbitro->persona->correo_persona}}</td>
                <tr>
                <tr>
                  <th width="150">Telefono Local:</th>
                  <td colspan="3">{{$arbitraje->arbitro->persona->telefono_local_persona}}</td>
                <tr>
                <tr>
                    <th width="150">Telefono Celular:</th>
                    <td colspan="3">{{$arbitraje->arbitro->persona->telefono_celular_persona}}</td>
                <tr>

              </table>
          </div>

        </div>
    </div>
@endsection
