@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('arbitraje.listado')}}" class="btn btn-warning">Mostrar listado</a>
            <hr/>
          </div>
          <h2 class="text-center f-35">Agregar arbitraje</h2>
          <div class="col-md-8 col-md-offset-2 p-b-50">
            <form method="POST" action="{{route('arbitraje.guardar')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                  {{ csrf_field() }}

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Arbitro</label>
                        {!!Form::select('arbitro_id', $arbitros, null, ['placeholder' => 'Seleccione una arbitro...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('arbitro_id')}}</span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Evento</label>
                      {!!Form::select('evento_id', $eventos, null, ['placeholder' => 'Seleccione el evento...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('evento_id')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Articulo</label>
                      {!!Form::select('articulo_id', $articulos, null, ['placeholder' => 'Seleccione el articulo...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('articulo_id')}}</span>
                  </div>
                </div>


                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Estatu</label>
                      {!!Form::select('estatu_id', $estatus, null, ['placeholder' => 'Seleccione la estatu...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('estatu_id')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12">
                  <div class="text-right">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                  </div>
                </div>

             </form>
          </div>

        </div>
    </div>
@endsection
@section('style')

@endsection
@section('script')
  <script type="text/javascript">
    $('.select-basic').select2();
  </script>
@endsection
