@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('arbitraje.listado')}}" class="btn btn-warning">Mostrar listado</a>
            <hr/>
          </div>
          <h2 class="text-center f-35">Editar arbitraje</h2>
          <div class="col-md-8 col-md-offset-2 p-b-50">
            <form method="POST" action="{{route('arbitraje.actualizar',['id'=>$arbitraje->id_arbitraje])}}" accept-charset="UTF-8" enctype="multipart/form-data">
                  {{ csrf_field() }}

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Arbitro</label> ({{$arbitraje->arbitro->persona->identificacion_persona}} - {{$arbitraje->arbitro->persona->nombre_persona}} {{$arbitraje->arbitro->persona->apellido_persona}})
                        {!!Form::select('arbitro_id', $arbitros, old('arbitro_id') ? old('arbitro_id') : $arbitraje->arbitro_id, ['placeholder' => 'Seleccione una arbitro...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('arbitro_id')}}</span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Evento</label> ({{$arbitraje->evento->nombre_evento}})
                      {!!Form::select('evento_id', $eventos, old('evento_id') ? old('evento_id') : $arbitraje->evento_id, ['placeholder' => 'Seleccione el evento...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('evento_id')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Articulo</label> ({{$arbitraje->articulo->codigo_articulo}} - {{$arbitraje->articulo->nombre_articulo}})
                      {!!Form::select('articulo_id', $articulos, old('articulo_id') ? old('articulo_id') : $arbitraje->articulo_id, ['placeholder' => 'Seleccione el articulo...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('articulo_id')}}</span>
                  </div>
                </div>


                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Estatu</label> ({{$arbitraje->estatu->nombre_estatu}})
                      {!!Form::select('estatu_id', $estatus, old('estatu_id') ? old('estatu_id') : $arbitraje->estatu_id, ['placeholder' => 'Seleccione la estatu...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('estatu_id')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12">
                  <div class="text-right">
                    <input type="hidden" name="id" value="{{$arbitraje->id_arbitraje}}">
                    <button type="submit" class="btn btn-primary">Modificar</button>
                  </div>
                </div>

             </form>
          </div>

        </div>
    </div>
@endsection
@section('style')

@endsection
@section('script')
  <script type="text/javascript">
    $('.select-basic').select2();
  </script>
@endsection
