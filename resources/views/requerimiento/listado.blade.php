@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12">
             @if (session('mensaje_ok'))
             <div class="alert alert-success text-center">
                 {{ session('mensaje_ok') }}
             </div>
             @endif
          </div>
          <h2 class="text-center f-35">Requerimientos</h2>
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('requerimiento.crear')}}" class="btn btn-primary">Agregar</a>
          </div>
          <div class="col-md-12">
            <table id="example" class="table text-center table-bordered">
              <thead>
              <tr>
                <th class="text-center">Evento</th>
                <th class="text-center">Tipo Requerimiento</th>
                <th class="text-center">Responsable</th>
                <th width="250" class="text-center">Acciones</th>
              </tr>
              </thead>
              <tbody>
              @foreach($requerimientos as $requerimiento)
              <tr>
                <td>{{$requerimiento->evento->nombre_evento}}</td>
                <td>{{$requerimiento->tipo_requerimiento->nombre_tipo_requerimiento}}</td>
                <td>{{$requerimiento->responsable->nombre_responsable}}</td>
                <td>
                  <a href="{{route('requerimiento.detalle',['id'=>$requerimiento->id_requerimiento])}}" class="btn btn-warning">Ver</a>
                  <a href="{{route('requerimiento.editar',['id'=>$requerimiento->id_requerimiento])}}" class="btn btn-info">Editar</a>
                  @if($requerimiento->deleted_at)
                  <a href="{{route('requerimiento.activar',['id'=>$requerimiento->id_requerimiento])}}" class="btn btn-danger">Bloqueado</a>
                  @else
                  <a href="{{route('requerimiento.eliminar',['id'=>$requerimiento->id_requerimiento])}}" class="btn btn-success">&nbsp;&nbsp;&nbsp;&nbsp;Activo&nbsp;&nbsp;&nbsp;&nbsp;</a>
                  @endif
                </td>
              </tr>
              @endforeach
              </tbody>
            </table>
          </div>
          <div class="p-b-30" >
          </div>
        </div>
    </div>
@endsection

@section('script')
 <script>
  $(document).ready(function() {
    //$('#example').DataTable();
    t=$('#example').DataTable({
       processing: true,
       serverSide: false,
       language: {
                       processing:     "Procesando ...",
                       search:         '<span class="glyphicon glyphicon-search"></span>',
                       searchPlaceholder: "BUSCAR",
                       lengthMenu:     "Mostrar _MENU_ Registros",
                       info:           "Mostrando _START_ a _END_ de _TOTAL_ Registros",
                       infoEmpty:      "Mostrando 0 a 0 de 0 Registros",
                       infoFiltered:   "(filtrada de _MAX_ registros en total)",
                       infoPostFix:    "",
                       loadingRecords: "...",
                       zeroRecords:    "No se encontraron registros coincidentes",
                       emptyTable:     "No hay datos disponibles en la tabla",
                       paginate: {
                           first:      "Primero",
                           previous:   "Anterior",
                           next:       "Siguiente",
                           last:       "Ultimo"
                       },
                       aria: {
                           sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
                           sortDescending: ": habilitado para ordenar la columna en orden descendente"
                       }
                   }
       });
  } );
 </script>
@endsection
