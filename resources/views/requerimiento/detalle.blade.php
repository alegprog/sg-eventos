@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12">
             @if (session('mensaje_ok'))
             <div class="alert alert-success text-center">
                 {{ session('mensaje_ok') }}
             </div>
             @endif
          </div>
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('requerimiento.listado')}}" class="btn btn-warning">Regresar</a>
          </div>
          <h2 class="text-center f-35">Información del requerimiento</h2>
          <br>
          <div class="col-md-7">
              <table class="table table-bordered table-hover">
                <tr>
                  <th width="150">Evento:</th>
                  <td colspan="3">{{$requerimiento->evento->nombre_evento}}</td>
                <tr>
                <tr>
                    <th width="150">Tipo Requerimiento:</th>
                    <td colspan="3">{{$requerimiento->tipo_requerimiento->nombre_tipo_requerimiento}}</td>
                <tr>
                <tr>
                  <th width="150">Descripción:</th>
                  <td colspan="3">{{$requerimiento->descripcion_requerimiento}}</td>
                <tr>
                <tr>
                  <th width="160">Cantidad:</th>
                  <td colspan="3">{{$requerimiento->cantidad_requerimiento}}</td>
                <tr>
                <tr>
                    <th width="150">Responsable:</th>
                    <td colspan="3">{{$requerimiento->responsable->nombre_responsable}}</td>
                <tr>

              </table>
          </div>

        </div>
    </div>
@endsection
