@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('requerimiento.listado')}}" class="btn btn-warning">Mostrar listado</a>
            <hr/>
          </div>
          <h2 class="text-center f-35">Editar requerimiento</h2>
          <div class="col-md-8 col-md-offset-2 p-b-50">
            <form method="POST" action="{{route('requerimiento.actualizar',['id'=>$requerimiento->id_requerimiento])}}" accept-charset="UTF-8" enctype="multipart/form-data">
                  {{ csrf_field() }}


                <div class="col-md-6">
                    <div class="form-group p-t-0">
                        <label class="f-15">Descripción</label>
                        <textarea type="text" class="form-control" name="descripcion" placeholder="Descripcion" >{{ old('descripcion') ? old('descripcion') : $requerimiento->descripcion_requerimiento }}</textarea>
                        <span class="text-danger" >{{$errors->first('descripcion')}}</span>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group p-t-0">
                        <label class="f-15">Cantidad</label>
                        <input type="text" class="form-control" name="cantidad" placeholder="Cantidad" value="{{ old('cantidad') ? old('cantidad') : $requerimiento->cantidad_requerimiento }}">
                        <span class="text-danger" >{{$errors->first('cantidad')}}</span>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Evento</label> ({{$requerimiento->evento->nombre_evento}})
                      {!!Form::select('evento_id', $eventos, old('evento_id') ? old('evento_id') : $requerimiento->evento_id, ['placeholder' => 'Seleccione el evento...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('evento_id')}}</span>
                  </div>
                </div>



                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Tipo de Requerimiento</label> ({{$requerimiento->tipo_requerimiento->nombre_tipo_requerimiento}})
                      {!!Form::select('tipo_requerimiento_id', $tipo_requerimientos, old('tipo_requerimiento_id') ? old('tipo_requerimiento_id') : $requerimiento->tipo_requerimiento_id, ['placeholder' => 'Seleccione el tipo requerimiento...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('requerimiento_id')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Responsable</label> ({{$requerimiento->responsable->nombre_responsable}})
                      {!!Form::select('responsable_id', $responsables, old('responsable_id') ? old('responsable_id') : $requerimiento->responsable_id, ['placeholder' => 'Seleccione los responsables...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('responsable_id')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12">
                  <div class="text-right">
                    <input type="hidden" name="id" value="{{$requerimiento->id_requerimiento}}">
                    <button type="submit" class="btn btn-primary">Modificar</button>
                  </div>
                </div>

             </form>
          </div>

        </div>
    </div>
@endsection
@section('style')

@endsection
@section('script')
  <script type="text/javascript">
    $('.select-basic').select2();
  </script>
@endsection
