@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('arbitraje.listado')}}" class="btn btn-warning">Mostrar listado</a>
            <hr/>
          </div>
          <h2 class="text-center f-35">Agregar requerimiento</h2>
          <div class="col-md-8 col-md-offset-2 p-b-50">
            <form method="POST" action="{{route('requerimiento.guardar')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                  {{ csrf_field() }}


                <div class="col-md-6">
                    <div class="form-group p-t-0">
                        <label class="f-15">Descripción</label>
                        <textarea type="text" class="form-control" name="descripcion" placeholder="Descripcion" >{{ old('descripcion') }}</textarea>
                        <span class="text-danger" >{{$errors->first('descripcion')}}</span>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group p-t-0">
                        <label class="f-15">Cantidad</label>
                        <input type="text" class="form-control" name="cantidad" placeholder="Cantidad" value="{{ old('cantidad') }}">
                        <span class="text-danger" >{{$errors->first('cantidad')}}</span>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Evento</label>
                      {!!Form::select('evento_id', $eventos, null, ['placeholder' => 'Seleccione el evento...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('evento_id')}}</span>
                  </div>
                </div>



                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Tipo de Requerimiento</label>
                      {!!Form::select('tipo_requerimiento_id', $tipo_requerimientos, null, ['placeholder' => 'Seleccione el tipo requerimiento...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('requerimiento_id')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Responsable</label>
                      {!!Form::select('responsable_id', $responsables, null, ['placeholder' => 'Seleccione los responsables...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('responsable_id')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12">
                  <div class="text-right">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                  </div>
                </div>

             </form>
          </div>

        </div>
    </div>
@endsection
@section('style')

@endsection
@section('script')
  <script type="text/javascript">
    $('.select-basic').select2();
  </script>
@endsection
