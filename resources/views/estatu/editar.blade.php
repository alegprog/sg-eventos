@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('estatu.listado')}}" class="btn btn-warning">Mostrar listado</a>
            <hr/>
          </div>
          <h2 class="text-center f-35">Editar estatu</h2>
          <div class="col-md-6 col-md-offset-3 p-b-50">
            <form method="POST" action="{{route('estatu.actualizar',['id'=>$estatu->id_estatu])}}">
                  {{ csrf_field() }}
                  <div class="form-group p-t-20">
                      <label class="f-15">Nombre</label>
                      <input type="text" class="form-control" name="nombre" placeholder="Nombre del tipo de estatu" value="{{ old('nombre') ? old('nombre') : $estatu->nombre_estatu }}">
                      <span class="text-danger" >{{$errors->first('nombre')}}</span>
                  </div>
                  <div class="text-right">
                    <input type="hidden" name="id" value="{{$estatu->id_estatu}}">
                    <button type="submit" class="btn btn-primary">Modificar</button>
                  </div>
             </form>
          </div>

        </div>
    </div>
@endsection
