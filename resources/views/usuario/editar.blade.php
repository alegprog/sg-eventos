@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('usuario.listado')}}" class="btn btn-warning">Mostrar listado</a>
            <hr/>
          </div>
          <h2 class="text-center f-35">Editar usuario</h2>
          <div class="col-md-8 col-md-offset-2 p-b-50">
            <form method="POST" action="{{route('usuario.actualizar',['id'=>$usuario->id])}}">
                  {{ csrf_field() }}
                  <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Nombre</label>
                      <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{ old('nombre') ? old('nombre') : $usuario->nombre_usuario }}">
                      <span class="text-danger" >{{$errors->first('nombre')}}</span>
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Apellido</label>
                      <input type="text" class="form-control" name="apellido" placeholder="Apellido" value="{{ old('apellido') ? old('apellido') : $usuario->apellido_usuario }}">
                      <span class="text-danger" >{{$errors->first('apellido')}}</span>
                  </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-md-6">
                    <div class="form-group p-t-0">
                        <label class="f-15">Cedula</label>
                        <input type="text" class="form-control" name="cedula" maxlength="8" placeholder="Cedula" value="{{ old('cedula') ? old('cedula') : $usuario->cedula_usuario }}">
                        <span class="text-danger" >{{$errors->first('cedula')}}</span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group p-t-0">
                        <label class="f-15">Usuario</label>
                        <input type="text" class="form-control" name="usuario" maxlength="20" placeholder="Usuario" value="{{ old('usuario') ? old('usuario') : $usuario->usuario }}">
                        <span class="text-danger" >{{$errors->first('usuario')}}</span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-md-6">
                    <div class="form-group p-t-0">
                        <label class="f-15">Contraseña</label>
                        <input type="password" class="form-control" autocomplete="false" name="clave" placeholder="Contraseña" value="">
                        <span class="text-danger" >{{$errors->first('clave')}}</span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group p-t-0">
                        <label class="f-15">Confirmar contraseña</label>
                        <input type="password" class="form-control" autocomplete="false" name="clave_confirmation" placeholder="Confirmar contraseña" value="">
                        <span class="text-danger" >{{$errors->first('clave_confirmation')}}</span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-md-6">
                    <div class="form-group p-t-0">
                        <label class="f-15">Correo</label>
                        <input type="text" class="form-control" name="correo" placeholder="Correo" value="{{ old('correo') ? old('correo') : $usuario->correo_usuario }}">
                        <span class="text-danger" >{{$errors->first('correo')}}</span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group p-t-0">
                        <label class="f-15">Tipo</label>
                        {!!Form::select('tipo', ['operador'=>'Operador','admin'=>'Admin'], old('tipo') ? old('tipo') : $usuario->tipo_usuario, ['placeholder' => 'Seleccione el tipo de usuario...', 'class'=>'form-control']) !!}
                        <span class="text-danger" >{{$errors->first('tipo')}}</span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="text-right">
                    <input type="hidden" name="id" value="{{$usuario->id}}">
                    <button type="submit" class="btn btn-primary">Modificar</button>
                  </div>
             </form>
          </div>

        </div>
    </div>
@endsection
