@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12">
             @if (session('mensaje_ok'))
             <div class="alert alert-success text-center">
                 {{ session('mensaje_ok') }}
             </div>
             @endif
          </div>
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('usuario.listado')}}" class="btn btn-warning">Regresar</a>
          </div>
          <h2 class="text-center f-35">Información del usuario</h2>
          <br>
          <div class="col-md-7">
              <table class="table table-bordered table-hover">
                <tr>
                  <th width="150">Cedula:</th>
                  <td colspan="3">{{$usuario->cedula_usuario}}</td>
                <tr>
                <tr>
                  <th width="150">Nombre:</th>
                  <td colspan="3">{{$usuario->nombre_usuario}} {{$usuario->apellido_usuario}}</td>
                <tr>
                <tr>
                  <th width="160">Usuario:</th>
                  <td colspan="3">{{$usuario->usuario}}</td>
                <tr>
                <tr>
                  <th width="150">Correo:</th>
                  <td colspan="3">{{$usuario->correo_usuario}}</td>
                <tr>
                <tr>
                  <th width="150">Tipo:</th>
                  <td colspan="3">{{$usuario->tipo_usuario}}</td>
                <tr>
              </table>
          </div>

        </div>
    </div>
@endsection
