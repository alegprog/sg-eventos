@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12">
             @if (session('mensaje_ok'))
             <div class="alert alert-success text-center">
                 {{ session('mensaje_ok') }}
             </div>
             @endif
          </div>
          <h2 class="text-center f-35">Cambio de contraseña</h2>
          <div class="col-md-8 col-md-offset-2 p-b-50">
            <form method="POST" action="{{route('dashboard.cambio_clave')}}">
                  {{ csrf_field() }}
                  <div class="col-md-6">
                    <div class="form-group p-t-0">
                        <label class="f-15">Contraseña</label>
                        <input type="password" class="form-control" autocomplete="false" name="clave" placeholder="Contraseña" value="">
                        <span class="text-danger" >{{$errors->first('clave')}}</span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group p-t-0">
                        <label class="f-15">Confirmar contraseña</label>
                        <input type="password" class="form-control" autocomplete="false" name="clave_confirmation" placeholder="Confirmar contraseña" value="">
                        <span class="text-danger" >{{$errors->first('clave_confirmation')}}</span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="text-right">
                    <button type="submit" class="btn btn-primary">Cambiar contraseña</button>
                  </div>
             </form>
          </div>

        </div>
    </div>
@endsection
