@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12">
          <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img alt="900x500" src="{{asset('assets/image/banner.png')}}"  >
      <div class="carousel-caption">
        <h2>!Hola <strong>{{Auth::user()->nombre_usuario.' '.Auth::user()->apellido_usuario}}</strong></h2>
      </div>
    </div>
    <div class="item">
      <img alt="900x500" src="{{asset('assets/image/banner.png')}}" >
      <div class="carousel-caption">
        <h2>Bienvenido a una Nueva Sesión!</h2>
      </div>
    </div>

  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

</div>


          <!--<p class="bg-success p-10 text-center f-18">!Hola <strong>{{Auth::user()->nombre_usuario.' '.Auth::user()->apellido_usuario}}</strong> Bienvenido a una Nueva Sesión!</p>-->
          <br><br><br>
          <h2 class="text-center f-35 p-l-r-60 p-t-b-60">Bienvenido al Sistema de Gestión de Eventos de la Universidad Jose Gregorio Hernández</h2>
        </div>
    </div>
@endsection
