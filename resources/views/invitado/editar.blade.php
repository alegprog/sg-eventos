@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('invitado.listado')}}" class="btn btn-warning">Mostrar listado</a>
            <hr/>
          </div>
          <h2 class="text-center f-35">Editar invitado</h2>
          <div class="col-md-8 col-md-offset-2 p-b-50">
            <form method="POST" action="{{route('invitado.actualizar',['id'=>$invitado->id_invitado])}}" accept-charset="UTF-8" enctype="multipart/form-data">
                  {{ csrf_field() }}

                <div class="col-md-6">
                    <div class="form-group p-t-0">
                        <label class="f-15">Titulo de ponencia</label>
                          <input type="text" class="form-control" name="titulo" placeholder="Titulo" value="{{ old('titulo') ? old('titulo') : $invitado->titulo_ponencia_invitado }}">
                        <span class="text-danger" >{{$errors->first('titulo')}}</span>
                    </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Persona</label> ({{$invitado->persona->identificacion_persona}} - {{$invitado->persona->nombre_persona}} {{$invitado->persona->apellido_persona}})
                        {!!Form::select('persona_id', $personas,  old('persona_id') ? old('persona_id') : $invitado->persona_id, ['placeholder' => 'Seleccione una persona...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('persona_id')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Tipo Invitado</label> ({{$invitado->tipo_invitado->nombre_tipo_invitado}})
                      {!!Form::select('tipo_invitado_id', $tipo_invitados, old('tipo_invitado_id') ? old('tipo_invitado_id') : $invitado->tipo_invitado_id, ['placeholder' => 'Seleccione el tipo de invitado...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('tipo_invitado_id')}}</span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Evento</label> ({{$invitado->evento->nombre_evento}})
                      {!!Form::select('evento_id', $eventos, old('evento_id') ? old('evento_id') : $invitado->evento_id, ['placeholder' => 'Seleccione el evento...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('evento_id')}}</span>
                  </div>
                </div>
                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Modalidad</label> ({{$invitado->modalidad->nombre_modalidad}})
                      {!!Form::select('modalidad_id', $modalidad, old('modalidad_id') ? old('modalidad_id') : $invitado->modalidad_id, ['placeholder' => 'Seleccione la modalidad...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('modalidad_id')}}</span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Archivo</label>
                      <input type="file" class="" name="archivo" placeholder="Seleciona un archivo" >
                      <span class="text-danger" >{{$errors->first('archivo')}}</span>
                  </div>
                </div>



                <div class="col-md-12">
                  <div class="text-right">
                    <input type="hidden" name="id" value="{{$invitado->id_invitado}}">
                    <button type="submit" class="btn btn-primary">Modificar</button>
                  </div>
                </div>

             </form>
          </div>

        </div>
    </div>
@endsection
@section('style')

@endsection
@section('script')
  <script type="text/javascript">
    $('.select-basic').select2();
  </script>
@endsection
