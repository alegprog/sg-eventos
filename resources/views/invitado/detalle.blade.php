@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12">
             @if (session('mensaje_ok'))
             <div class="alert alert-success text-center">
                 {{ session('mensaje_ok') }}
             </div>
             @endif
          </div>
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('invitado.listado')}}" class="btn btn-warning">Regresar</a>
          </div>
          <h2 class="text-center f-35">Información del invitado</h2>
          <br>
          <div class="col-md-7">
              <table class="table table-bordered table-hover">
                <tr>
                  <th width="160">Nombre del invitado:</th>
                  <td colspan="3">{{$invitado->persona->nombre_persona}} {{$invitado->persona->apellido_persona}}</td>
                <tr>
                <tr>
                  <th width="150">Identificacion:</th>
                  <td colspan="3">{{$invitado->persona->identificacion_persona}}</td>
                <tr>
                <tr>
                  <th width="150">Correo:</th>
                  <td colspan="3">{{$invitado->persona->correo_persona}}</td>
                <tr>
                <tr>
                  <th width="150">Telefono Local:</th>
                  <td colspan="3">{{$invitado->persona->telefono_local_persona}}</td>
                <tr>
                <tr>
                  <th width="150">Telefono Celular:</th>
                  <td colspan="3">{{$invitado->persona->telefono_celular_persona}}</td>
                <tr>
                <tr>
                  <th width="150">Evento:</th>
                  <td colspan="3">{{$invitado->evento->nombre_evento}}</td>
                <tr>
                <tr>
                    <th width="150">Tipo Invitado:</th>
                    <td colspan="3">{{$invitado->tipo_invitado->nombre_tipo_invitado}}</td>
                <tr>
                <tr>
                    <th width="150">Titulo de la ponencia:</th>
                    <td colspan="3">{{$invitado->titulo_ponencia_invitado}}</td>
                <tr>
                <tr>
                    <th width="150">Modalidad:</th>
                    <td colspan="3">{{$invitado->modalidad->nombre_modalidad}}</td>
                <tr>
              </table>
          </div>
          <div class="col-md-5">
            <div class="thumbnail">
              <div class="caption">
                @if($tipo_archivo=='imagen')
                <label>Imagen</label>
                <hr>
                <p>
                  <img width="100%" src="{{asset('assets/file/invitado/'.$invitado->archivo)}}" alt="No se encuentra imagen asociada" class="img-responsive" >
                </p>
                @elseif($tipo_archivo=='archivo')
                  <label>Archivo</label>
                  <hr>
                  <a class="btn btn-primary" href="{{route('invitado.download',$invitado->archivo)}}">Descargar<a>
                @else
                  <label>No se encuentra ningun archivo asociado</label>
                  <hr>
                @endif
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection
