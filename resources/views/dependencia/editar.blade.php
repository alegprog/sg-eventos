@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('dependencia.listado')}}" class="btn btn-warning">Mostrar listado</a>
            <hr/>
          </div>
          <h2 class="text-center f-35">Editar dependencia</h2>
          <div class="col-md-6 col-md-offset-3 p-b-50">
            <form method="POST" action="{{route('dependencia.actualizar',['id'=>$dependencia->id_dependencia])}}">
                  {{ csrf_field() }}
                  <div class="form-group p-t-20">
                      <label class="f-15">Nombre</label>
                      <input type="text" class="form-control" name="nombre" placeholder="Nombre de la dependencia" value="{{ old('nombre') ? old('nombre') : $dependencia->nombre_dependencia }}">
                      <span class="text-danger" >{{$errors->first('nombre')}}</span>
                  </div>
                  <div class="row">
                    <div class="form-group p-t-20 col-md-3">
                       <label class="f-15">&nbsp;</label>
                       <input type="text" class="form-control" name="firma_1_antes" placeholder="ING." value="{{ old('firma_1_antes') ? old('firma_1_antes') : $dependencia->firma_1_antes}}">
                       <span class="text-danger" >{{$errors->first('firma_1_antes')}}</span>
                    </div>
                    <div class="form-group p-t-20 col-md-6">
                      <label class="f-15">Firma #1</label>
                      {!!Form::select('firma_1_id', $personas, old('firma_1_id') ? old('firma_1_id') : $dependencia->firma_1_id, ['placeholder' => 'Seleccione una persona...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('firma_1_id')}}</span>
                    </div>
                    <div class="form-group p-t-20 col-md-3">
                        <label class="f-15">&nbsp;</label>
                        <input type="text" class="form-control" name="firma_1_despues" placeholder="M.Sc." value="{{ old('firma_1_despues') ? old('firma_1_despues') : $dependencia->firma_1_despues }}">
                        <span class="text-danger" >{{$errors->first('firma_1_despues')}}</span>
                    </div>
                  </div>

                  <div class="form-group p-t-20">
                      <label class="f-15">Cargo</label>
                      <input type="text" class="form-control" name="firma_1_cargo" placeholder="Cargo" value="{{ old('firma_1_cargo') ? old('firma_1_cargo') : $dependencia->firma_1_cargo }}">
                      <span class="text-danger" >{{$errors->first('firma_1_cargo')}}</span>
                  </div>

                  <div class="clearfix"></div>

                  <div class="row">
                    <div class="form-group p-t-20 col-md-3">
                       <label class="f-15">&nbsp;</label>
                       <input type="text" class="form-control" name="firma_2_antes" placeholder="ING." value="{{ old('firma_2_antes')  ? old('firma_2_antes') : $dependencia->firma_2_antes }}">
                       <span class="text-danger" >{{$errors->first('firma_2_antes')}}</span>
                    </div>
                    <div class="form-group p-t-20 col-md-6">
                      <label class="f-15">Firma #2</label>
                      {!!Form::select('firma_2_id', $personas, old('firma_2_id') ? old('firma_2_id') : $dependencia->firma_2_id, ['placeholder' => 'Seleccione una persona...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('firma_2_id')}}</span>
                    </div>
                    <div class="form-group p-t-20 col-md-3">
                        <label class="f-15">&nbsp;</label>
                        <input type="text" class="form-control" name="firma_2_despues" placeholder="M.Sc." value="{{ old('firma_2_despues')  ? old('firma_2_despues') : $dependencia->firma_2_despues }}">
                        <span class="text-danger" >{{$errors->first('firma_2_despues')}}</span>
                    </div>
                  </div>

                  <div class="clearfix"></div>

                  <div class="form-group p-t-20">
                      <label class="f-15">Cargo</label>
                      <input type="text" class="form-control" name="firma_2_cargo" placeholder="Cargo" value="{{ old('firma_2_cargo')  ? old('firma_2_cargo') : $dependencia->firma_2_cargo }}">
                      <span class="text-danger" >{{$errors->first('firma_2_cargo')}}</span>
                  </div>

                  <div class="clearfix"></div>

                  <div class="text-right">
                    <input type="hidden" name="id" value="{{$dependencia->id_dependencia}}">
                    <button type="submit" class="btn btn-primary">Modificar</button>
                  </div>
             </form>
          </div>

        </div>
    </div>
@endsection

@section('script')
  <script type="text/javascript">
    $('.select-basic').select2();
  </script>
@endsection
