@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('dependencia.listado')}}" class="btn btn-warning">Mostrar listado</a>
            <hr/>
          </div>
          <h2 class="text-center f-35">Agregar dependencia</h2>
          <div class="col-md-8 col-md-offset-2 p-b-50">
            <form method="POST" action="{{route('dependencia.guardar')}}">
                  {{ csrf_field() }}
                  <div class="form-group p-t-20">
                      <label class="f-15">Nombre</label>
                      <input type="text" class="form-control" name="nombre" placeholder="Nombre de la dependencia" value="{{ old('nombre') }}">
                      <span class="text-danger" >{{$errors->first('nombre')}}</span>
                  </div>

                  <div class="row">
                    <div class="form-group p-t-20 col-md-3">
                       <label class="f-15">&nbsp;</label>
                       <input type="text" class="form-control" name="firma_1_antes" placeholder="ING." value="{{ old('firma_1_antes') }}">
                       <span class="text-danger" >{{$errors->first('firma_1_antes')}}</span>
                    </div>
                    <div class="form-group p-t-20 col-md-6">
                      <label class="f-15">Firma #1</label>
                      {!!Form::select('firma_1_id', $personas, null, ['placeholder' => 'Seleccione una persona...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('firma_1_id')}}</span>
                    </div>
                    <div class="form-group p-t-20 col-md-3">
                        <label class="f-15">&nbsp;</label>
                        <input type="text" class="form-control" name="firma_1_despues" placeholder="M.Sc." value="{{ old('firma_1_despues') }}">
                        <span class="text-danger" >{{$errors->first('firma_1_despues')}}</span>
                    </div>
                  </div>

                  <div class="form-group p-t-20">
                      <label class="f-15">Cargo</label>
                      <input type="text" class="form-control" name="firma_1_cargo" placeholder="Cargo" value="{{ old('firma_1_cargo') }}">
                      <span class="text-danger" >{{$errors->first('firma_1_cargo')}}</span>
                  </div>

                  <div class="clearfix"></div>

                  <div class="row">
                    <div class="form-group p-t-20 col-md-3">
                       <label class="f-15">&nbsp;</label>
                       <input type="text" class="form-control" name="firma_2_antes" placeholder="ING." value="{{ old('firma_2_antes') }}">
                       <span class="text-danger" >{{$errors->first('firma_2_antes')}}</span>
                    </div>
                    <div class="form-group p-t-20 col-md-6">
                      <label class="f-15">Firma #2</label>
                      {!!Form::select('firma_2_id', $personas, null, ['placeholder' => 'Seleccione una persona...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('firma_2_id')}}</span>
                    </div>
                    <div class="form-group p-t-20 col-md-3">
                        <label class="f-15">&nbsp;</label>
                        <input type="text" class="form-control" name="firma_2_despues" placeholder="M.Sc." value="{{ old('firma_2_despues') }}">
                        <span class="text-danger" >{{$errors->first('firma_2_despues')}}</span>
                    </div>
                  </div>

                  <div class="clearfix"></div>

                  <div class="form-group p-t-20">
                      <label class="f-15">Cargo</label>
                      <input type="text" class="form-control" name="firma_2_cargo" placeholder="Cargo" value="{{ old('firma_2_cargo') }}">
                      <span class="text-danger" >{{$errors->first('firma_2_cargo')}}</span>
                  </div>

                  <div class="clearfix"></div>

                  <div class="text-right">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                  </div>
             </form>
          </div>

        </div>
    </div>
@endsection
@section('script')
  <script type="text/javascript">
    $('.select-basic').select2();
  </script>
@endsection
