@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('articulo.listado')}}" class="btn btn-warning">Mostrar listado</a>
            <hr/>
          </div>
          <h2 class="text-center f-35">Agregar articulo</h2>
          <div class="col-md-8 col-md-offset-2 p-b-50">
            <form method="POST" action="{{route('articulo.guardar')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                  {{ csrf_field() }}

                <div class="col-md-6">
                    <div class="form-group p-t-0">
                        <label class="f-15">Codigo</label>
                          <input type="text" class="form-control" name="codigo" placeholder="Codigo" value="{{ old('codigo') }}">
                        <span class="text-danger" >{{$errors->first('codigo')}}</span>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group p-t-0">
                        <label class="f-15">Nombre</label>
                          <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{ old('nombre') }}">
                        <span class="text-danger" >{{$errors->first('nombre')}}</span>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Persona</label>
                        {!!Form::select('persona_id', $personas, null, ['placeholder' => 'Seleccione una persona...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('persona_id')}}</span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Persona 2</label>
                        {!!Form::select('coautor1_id', $personas, null, ['placeholder' => 'Seleccione una persona...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('coautor1_id')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Persona 3</label>
                        {!!Form::select('coautor2_id', $personas, null, ['placeholder' => 'Seleccione una persona...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('coautor2_id')}}</span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Evento</label>
                      {!!Form::select('evento_id', $eventos, null, ['placeholder' => 'Seleccione el evento...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('evento_id')}}</span>
                  </div>
                </div>
                <div class="clearfix"></div>

                <div class="col-md-12">
                  <div class="text-right">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                  </div>
                </div>

             </form>
          </div>

        </div>
    </div>
@endsection
@section('style')

@endsection
@section('script')
  <script type="text/javascript">
    $('.select-basic').select2();
  </script>
@endsection
