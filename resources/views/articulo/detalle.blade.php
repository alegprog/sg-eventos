@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12">
             @if (session('mensaje_ok'))
             <div class="alert alert-success text-center">
                 {{ session('mensaje_ok') }}
             </div>
             @endif
          </div>
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('articulo.listado')}}" class="btn btn-warning">Regresar</a>
          </div>
          <h2 class="text-center f-35">Información del articulo</h2>
          <br>
          <div class="col-md-7">
              <table class="table table-bordered table-hover">
                <tr>
                  <th width="150">Codigo:</th>
                  <td colspan="3">{{$articulo->codigo_articulo}}</td>
                <tr>
                <tr>
                  <th width="150">Articulo:</th>
                  <td colspan="3">{{$articulo->nombre_articulo}}</td>
                <tr>
                <tr>
                  <th width="150">Autor:</th>
                  <td colspan="3">
                   <b>Identificacion:</b> {{$articulo->persona->identificacion_persona}}
                  <br>
                   <b>Nombre:</b> {{$articulo->persona->nombre_persona}} {{$articulo->persona->apellido_persona}}
                  <br>
                   <b>Correo:</b> {{$articulo->persona->correo_persona}}
                  <br>
                   <b>Telefono Local:</b> {{$articulo->persona->telefono_local_persona}}
                  <br>
                   <b>Telefono Celular:</b> {{$articulo->persona->telefono_celular_persona}}
                </td>
                <tr>
                @if($articulo->coautor_principal)
                  <tr>
                    <th width="150">Coautor #1:</th>
                    <td colspan="3">
                      <b>Identificacion:</b> {{$articulo->coautor_principal->identificacion_persona}}
                     <br>
                      <b>Nombre:</b> {{$articulo->coautor_principal->nombre_persona}} {{$articulo->coautor_principal->apellido_persona}}
                     <br>
                      <b>Correo:</b> {{$articulo->coautor_principal->correo_persona}}
                     <br>
                      <b>Telefono Local:</b> {{$articulo->coautor_principal->telefono_local_persona}}
                     <br>
                      <b>Telefono Celular:</b> {{$articulo->coautor_principal->telefono_celular_persona}}
                    </td>
                  <tr>
                @endif
                @if($articulo->coautor_segundario)
                  <tr>
                    <th width="150">Coautor #2:</th>
                    <td colspan="3">
                      <b>Identificacion:</b> {{$articulo->coautor_segundario->identificacion_persona}}
                     <br>
                      <b>Nombre:</b> {{$articulo->coautor_segundario->nombre_persona}} {{$articulo->coautor_segundario->apellido_persona}}
                     <br>
                      <b>Correo:</b> {{$articulo->coautor_segundario->correo_persona}}
                     <br>
                      <b>Telefono Local:</b> {{$articulo->coautor_segundario->telefono_local_persona}}
                     <br>
                      <b>Telefono Celular:</b> {{$articulo->coautor_segundario->telefono_celular_persona}}
                    </td>
                  <tr>
                @endif
                <tr>
                  <th width="150">Evento:</th>
                  <td colspan="3">{{$articulo->evento->nombre_evento}}</td>
                <tr>

              </table>
          </div>

        </div>
    </div>
@endsection
