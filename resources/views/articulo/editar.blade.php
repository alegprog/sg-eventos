@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('articulo.listado')}}" class="btn btn-warning">Mostrar listado</a>
            <hr/>
          </div>
          <h2 class="text-center f-35">Editar articulo</h2>
          <div class="col-md-8 col-md-offset-2 p-b-50">
            <form method="POST" action="{{route('articulo.actualizar',['id'=>$articulo->id_articulo])}}" accept-charset="UTF-8" enctype="multipart/form-data">
                  {{ csrf_field() }}

                <div class="col-md-6">
                    <div class="form-group p-t-0">
                        <label class="f-15">Codigo</label>
                          <input type="text" class="form-control" name="codigo" placeholder="Codigo" value="{{ old('codigo') ? old('codigo') : $articulo->codigo_articulo }}">
                        <span class="text-danger" >{{$errors->first('codigo')}}</span>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group p-t-0">
                        <label class="f-15">Nombre</label>
                          <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{ old('nombre') ? old('nombre') : $articulo->nombre_articulo }}">
                        <span class="text-danger" >{{$errors->first('nombre')}}</span>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Persona</label> ({{$articulo->persona->identificacion_persona}} - {{$articulo->persona->nombre_persona}} {{$articulo->persona->apellido_persona}})
                        {!!Form::select('persona_id', $personas, old('persona_id') ? old('persona_id') : $articulo->persona_id, ['placeholder' => 'Seleccione una persona...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('persona_id')}}</span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Persona 2</label> @if($articulo->coautor_principal) ({{$articulo->coautor_principal->identificacion_persona}} - {{$articulo->coautor_principal->nombre_persona}} {{$articulo->coautor_principal->apellido_persona}}) @endif
                        {!!Form::select('coautor1_id', $personas, old('coautor1_id') ? old('coautor1_id') : $articulo->coautor_1_id, ['placeholder' => 'Seleccione una persona...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('coautor1_id')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Persona 3</label> @if($articulo->coautor_segundario) ({{$articulo->coautor_segundario->identificacion_persona}} - {{$articulo->coautor_segundario->nombre_persona}} {{$articulo->coautor_segundario->apellido_persona}}) @endif
                        {!!Form::select('coautor2_id', $personas, old('coautor2_id') ? old('coautor2_id') : $articulo->coautor_2_id, ['placeholder' => 'Seleccione una persona...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('coautor2_id')}}</span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Evento</label> ({{$articulo->evento->nombre_evento}})
                      {!!Form::select('evento_id', $eventos, old('evento_id') ? old('evento_id') : $articulo->evento_id, ['placeholder' => 'Seleccione el evento...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('evento_id')}}</span>
                  </div>
                </div>
                <div class="clearfix"></div>

                <div class="col-md-12">
                  <div class="text-right">
                    <input type="hidden" name="id" value="{{$articulo->id_articulo}}">
                    <button type="submit" class="btn btn-primary">Modificar</button>
                  </div>
                </div>

             </form>
          </div>

        </div>
    </div>
@endsection
@section('style')

@endsection
@section('script')
  <script type="text/javascript">
    $('.select-basic').select2();
  </script>
@endsection
