@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('evento.listado')}}" class="btn btn-warning">Mostrar listado</a>
            <hr/>
          </div>
          <h2 class="text-center f-35">Agregar evento</h2>
          <div class="col-md-8 col-md-offset-2 p-b-50">
            <form method="POST" action="{{route('evento.guardar')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                  {{ csrf_field() }}
                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Nombre</label>
                      <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{ old('nombre') }}">
                      <span class="text-danger" >{{$errors->first('nombre')}}</span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Tipo de evento</label>
                      {!!Form::select('tipo_evento_id', $tipo_eventos, null, ['placeholder' => 'Seleccione el tipo de envento...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('tipo_evento_id')}}</span>
                  </div>
                </div>
                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Fecha inicio</label>
                      <div class='input-group date' id='date_fecha_inicio'>
                        <input type="text" class="form-control" name="fecha_inicio" placeholder="Fecha inicio" value="{{ old('fecha_inicio') }}">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>
                      <span class="text-danger" >{{$errors->first('fecha_inicio')}}</span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Lugar</label>
                      {!!Form::select('lugar_id', $lugares, null, ['placeholder' => 'Seleccione el lugar...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('lugar_id')}}</span>
                  </div>
                </div>
                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Hora inicio</label>
                    <div class='input-group date' id='time_hora_inicio'>
                      <input type="text" class="form-control" name="hora_inicio" placeholder="Hora inicio" value="{{ old('hora_inicio') }}">
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                      </span>
                    </div>
                      <span class="text-danger" >{{$errors->first('hora_inicio')}}</span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Dependencia</label>
                      {!!Form::select('dependencia_id', $dependencias, null, ['placeholder' => 'Seleccione la dependencia...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('dependencia_id')}}</span>
                  </div>
                </div>
                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Dirección</label>
                      <input type="text" class="form-control" name="direccion" placeholder="Dirección" value="{{ old('direccion') }}">
                      <span class="text-danger" >{{$errors->first('direccion')}}</span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Duración</label>
                      <input type="text" class="form-control" name="duracion" placeholder="Duración" value="{{ old('duracion') }}">
                      <span class="text-danger" >{{$errors->first('duracion')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Fecha final</label>
                      <div class='input-group date' id='date_fecha_final'>
                        <input type="text" class="form-control" name="fecha_final" placeholder="Fecha final" value="{{ old('fecha_final') }}">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>
                      <span class="text-danger" >{{$errors->first('fecha_final')}}</span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Hora final</label>
                      <div class='input-group date' id='time_hora_final'>
                        <input type="text" class="form-control" name="hora_final" placeholder="Hora final" value="{{ old('hora_final') }}">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-time"></span>
                        </span>
                      </div>
                      <span class="text-danger" >{{$errors->first('hora_final')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12">
                  <div class="form-group p-t-0">
                      <label class="f-15">Observaciones</label>
                      <input type="text" class="form-control" name="observaciones" placeholder="Observaciones" value="{{ old('observaciones') }}">
                      <span class="text-danger" >{{$errors->first('observaciones')}}</span>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group p-t-0">
                      <label class="f-15">Archivo</label>
                      <input type="file" class="" name="archivo" placeholder="Seleciona un archivo" >
                      <span class="text-danger" >{{$errors->first('archivo')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>


                <div class="col-md-12">
                  <div class="text-right">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                  </div>
                </div>

             </form>
          </div>

        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript">
    $(function () {
      $('#date_fecha_inicio').datepicker();
      $('#date_fecha_final').datepicker();
      $('#time_hora_inicio').datetimepicker({
          format: 'LT'
      });
      $('#time_hora_final').datetimepicker({
          format: 'LT'
      });
    });

</script>
@endsection
