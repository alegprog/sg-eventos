@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12">
             @if (session('mensaje_ok'))
             <div class="alert alert-success text-center">
                 {{ session('mensaje_ok') }}
             </div>
             @endif
          </div>
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('evento.listado')}}" class="btn btn-warning">Regresar</a>
          </div>
          <h2 class="text-center f-35">Información del evento</h2>
          <br>
          <div class="col-md-7">
              <table class="table table-bordered table-hover">
                <tr>
                  <th width="160">Nombre del evento:</th>
                  <td colspan="3">{{$evento->nombre_evento}}</td>
                <tr>
                <tr>
                  <th width="150">Tipo de evento:</th>
                  <td colspan="3">{{$evento->tipo_evento->nombre_tipo_evento}}</td>
                <tr>
                <tr>
                  <th width="150">Fecha inicio:</th>
                  <td>{{$evento->fecha_inicio_evento}}</td>
                  <th width="150">Fecha final:</th>
                  <td>{{$evento->fecha_final_evento}}</td>
                <tr>
                <tr>
                  <th width="150">Lugar:</th>
                  <td colspan="3">{{$evento->lugar->nombre_lugar}}</td>
                <tr>
                <tr>
                    <th width="150">Hora inicio:</th>
                    <td>{{$evento->hora_inicio_evento}}</td>
                    <th width="150">Hora final:</th>
                    <td>{{$evento->hora_final_evento}}</td>
                <tr>
                <tr>
                    <th width="150">Duración:</th>
                    <td colspan="3">{{$evento->duracion_evento}}</td>
                <tr>
                <tr>
                    <th width="150">Dirección:</th>
                    <td colspan="3">{{$evento->direccion_evento}}</td>
                <tr>
                <tr>
                    <th width="150">Dependencia:</th>
                    <td colspan="3">{{$evento->dependencia->nombre_dependencia}}</td>
                <tr>
                <tr>
                      <th width="150">Observaciones:</th>
                      <td colspan="3">{{$evento->observacion_evento}}</td>
                <tr>
              </table>
          </div>
          <div class="col-md-5">
            <div class="thumbnail">
              <div class="caption">
                @if($tipo_archivo=='imagen')
                <label>Imagen</label>
                <hr>
                <p>
                  <img width="100%" src="{{asset('assets/file/evento/'.$evento->archivo)}}" alt="No se encuentra imagen asociada" class="img-responsive" >
                </p>
                @elseif($tipo_archivo=='archivo')
                  <label>Archivo</label>
                  <hr>
                  <a class="btn btn-primary" href="{{route('evento.download',$evento->archivo)}}">Descargar<a>
                @else
                  <label>No se encuentra ningun archivo asociado</label>
                  <hr>
                @endif
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection
