@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12">
             @if (session('mensaje_ok'))
             <div class="alert alert-success text-center">
                 {{ session('mensaje_ok') }}
             </div>
             @endif
          </div>
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('arbitro.listado')}}" class="btn btn-warning">Regresar</a>
          </div>
          <h2 class="text-center f-35">Información del arbitro</h2>
          <br>
          <div class="col-md-7">
              <table class="table table-bordered table-hover">
                <tr>
                  <th width="150">Identificacion:</th>
                  <td colspan="3">{{$arbitro->persona->identificacion_persona}}</td>
                <tr>
                <tr>
                  <th width="160">Nombre del arbitro:</th>
                  <td colspan="3">{{$arbitro->persona->nombre_persona}} {{$arbitro->persona->apellido_persona}}</td>
                <tr>
                <tr>
                  <th width="150">Correo:</th>
                  <td colspan="3">{{$arbitro->persona->correo_persona}}</td>
                <tr>
                <tr>
                  <th width="150">Telefono Local:</th>
                  <td colspan="3">{{$arbitro->persona->telefono_local_persona}}</td>
                <tr>
                <tr>
                    <th width="150">Telefono Celular:</th>
                    <td colspan="3">{{$arbitro->persona->telefono_celular_persona}}</td>
                <tr>
                <tr>
                  <th width="150">Evento:</th>
                  <td colspan="3">{{$arbitro->evento->nombre_evento}}</td>
                <tr>
                <tr>
                    <th width="150">Tipo Arbitro:</th>
                    <td colspan="3">{{$arbitro->tipo_arbitro->nombre_tipo_arbitro}}</td>
                <tr>
                <tr>
                    <th width="150">Modalidad:</th>
                    <td colspan="3">{{$arbitro->modalidad->nombre_modalidad}}</td>
                <tr>
              </table>
          </div>

        </div>
    </div>
@endsection
