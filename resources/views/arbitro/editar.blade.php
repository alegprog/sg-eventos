@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('arbitro.listado')}}" class="btn btn-warning">Mostrar listado</a>
            <hr/>
          </div>
          <h2 class="text-center f-35">Editar arbitro</h2>
          <div class="col-md-8 col-md-offset-2 p-b-50">
            <form method="POST" action="{{route('arbitro.actualizar',['id'=>$arbitro->id_arbitro])}}" accept-charset="UTF-8" enctype="multipart/form-data">
                  {{ csrf_field() }}

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Persona</label> ({{$arbitro->persona->identificacion_persona}} - {{$arbitro->persona->nombre_persona}} {{$arbitro->persona->apellido_persona}})
                        {!!Form::select('persona_id', $personas,  old('persona_id') ? old('persona_id') : $arbitro->persona_id, ['placeholder' => 'Seleccione una persona...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('persona_id')}}</span>
                  </div>
                </div>



                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Tipo Arbitro</label> ({{$arbitro->tipo_arbitro->nombre_tipo_arbitro}})
                      {!!Form::select('tipo_arbitro_id', $tipo_arbitros, old('tipo_arbitro_id') ? old('tipo_arbitro_id') : $arbitro->tipo_arbitro_id, ['placeholder' => 'Seleccione el tipo de arbitro...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('tipo_arbitro_id')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Evento</label> ({{$arbitro->evento->nombre_evento}})
                      {!!Form::select('evento_id', $eventos, old('evento_id') ? old('evento_id') : $arbitro->evento_id, ['placeholder' => 'Seleccione el evento...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('evento_id')}}</span>
                  </div>
                </div>


                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Modalidad</label> ({{$arbitro->modalidad->nombre_modalidad}})
                      {!!Form::select('modalidad_id', $modalidad, old('modalidad_id') ? old('modalidad_id') : $arbitro->modalidad_id, ['placeholder' => 'Seleccione la modalidad...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('modalidad_id')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12">
                  <div class="text-right">
                    <input type="hidden" name="id" value="{{$arbitro->id_arbitro}}">
                    <button type="submit" class="btn btn-primary">Modificar</button>
                  </div>
                </div>

             </form>
          </div>

        </div>
    </div>
@endsection
@section('style')

@endsection
@section('script')
  <script type="text/javascript">
    $('.select-basic').select2();
  </script>
@endsection
