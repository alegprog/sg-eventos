@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12">
             @if (session('mensaje_ok'))
             <div class="alert alert-success text-center">
                 {{ session('mensaje_ok') }}
             </div>
             @endif
         </div>
          <h2 class="text-center f-35">Personas</h2>
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('persona.crear')}}" class="btn btn-primary">Agregar</a>
          </div>
          <div class="col-md-12">
            <table id="example" class="table text-center table-bordered">
              <thead>
              <tr>
                <th class="text-center">Identificación</th>
                <th class="text-center" width="160" >Nacionalidad</th>
                <th class="text-center" width="170" >Tipo identificación</th>
                <th class="text-center" width="300">Nombre</th>
                <th width="250" class="text-center">Acciones</th>
              </tr>
              </thead>
              <tbody>
              @foreach($personas as $persona)
              <tr>
                <td>{{$persona->identificacion_persona}}</td>
                <td>{{$persona->nacionalidad->nombre_nacionalidad}}</td>
                <td>{{$persona->documento->nombre_tipo_identificacion}}</td>
                <td>{{$persona->nombre_persona.' '.$persona->apellido_persona}}</td>
                <td>
                  <a href="{{route('persona.detalle',['id'=>$persona->id_persona])}}" class="btn btn-warning">Ver</a>
                  <a href="{{route('persona.editar',['id'=>$persona->id_persona])}}" class="btn btn-info">Editar</a>
                  @if($persona->deleted_at)
                  <a href="{{route('persona.activar',['id'=>$persona->id_persona])}}" class="btn btn-danger">Bloqueado</a>
                  @else
                  <a href="{{route('persona.eliminar',['id'=>$persona->id_persona])}}" class="btn btn-success">&nbsp;&nbsp;&nbsp;&nbsp;Activo&nbsp;&nbsp;&nbsp;&nbsp;</a>
                  @endif
                </td>
              </tr>
              @endforeach
              </tbody>
            </table>
          </div>
          <div class="p-b-30" >
          </div>
        </div>
    </div>
@endsection

@section('script')
 <script>
  $(document).ready(function() {
    //$('#example').DataTable();
    t=$('#example').DataTable({
       processing: true,
       serverSide: false,
       language: {
                       processing:     "Procesando ...",
                       search:         '<span class="glyphicon glyphicon-search"></span>',
                       searchPlaceholder: "BUSCAR",
                       lengthMenu:     "Mostrar _MENU_ Registros",
                       info:           "Mostrando _START_ a _END_ de _TOTAL_ Registros",
                       infoEmpty:      "Mostrando 0 a 0 de 0 Registros",
                       infoFiltered:   "(filtrada de _MAX_ registros en total)",
                       infoPostFix:    "",
                       loadingRecords: "...",
                       zeroRecords:    "No se encontraron registros coincidentes",
                       emptyTable:     "No hay datos disponibles en la tabla",
                       paginate: {
                           first:      "Primero",
                           previous:   "Anterior",
                           next:       "Siguiente",
                           last:       "Ultimo"
                       },
                       aria: {
                           sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
                           sortDescending: ": habilitado para ordenar la columna en orden descendente"
                       }
                   }
       });
  } );
 </script>
@endsection
