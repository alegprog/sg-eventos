@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('persona.listado')}}" class="btn btn-warning">Mostrar listado</a>
            <hr/>
          </div>
          <h2 class="text-center f-35">Editar persona</h2>
          <div class="col-md-8 col-md-offset-2 p-b-50">
            <form method="POST" action="{{route('persona.actualizar',['id'=>$persona->id_persona])}}">
                  {{ csrf_field() }}
                  <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Nombre</label>
                      <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{ old('nombre') ? old('nombre') : $persona->nombre_persona }}">
                      <span class="text-danger" >{{$errors->first('nombre')}}</span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Apellido</label>
                      <input type="text" class="form-control" name="apellido" placeholder="Apellido" value="{{ old('apellido') ? old('apellido') : $persona->apellido_persona }}">
                      <span class="text-danger" >{{$errors->first('apellido')}}</span>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Tipo identificación: </label> ({{$persona->documento->nombre_tipo_identificacion}})
                      {!!Form::select('tipo_identificacion_id', $tipo_identificaciones, old('tipo_identificacion_id') ? old('tipo_identificacion_id') : $persona->tipo_identificacion_id, ['placeholder' => 'Seleccione el tipo de identificación...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('tipo_identificacion_id')}}</span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Identificación</label>
                      <input type="text" class="form-control" name="identificacion" placeholder="Identificación" value="{{ old('identificacion') ? old('identificacion') : $persona->identificacion_persona }}">
                      <span class="text-danger" >{{$errors->first('identificacion')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Nacionalidad: </label> ({{$persona->nacionalidad->nombre_nacionalidad}})
                      {!!Form::select('nacionalidad_id', $nacionalidades, old('nacionalidad_id') ? old('nacionalidad_id') : $persona->nacionalidad_id, ['placeholder' => 'Seleccione la nacionalidad...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('nacionalidad_id')}}</span>
                  </div>
                </div>


                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Correo</label>
                      <input type="text" class="form-control" name="correo" placeholder="Correo" value="{{ old('correo') ? old('correo') : $persona->correo_persona }}">
                      <span class="text-danger" >{{$errors->first('correo')}}</span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Teléfono local</label>
                      <input type="text" class="form-control" name="telefono_local" placeholder="Teléfono local" value="{{ old('telefono_local') ? old('telefono_local') : $persona->telefono_local_persona }}">
                      <span class="text-danger" >{{$errors->first('telefono_local')}}</span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Teléfono celular</label>
                      <input type="text" class="form-control" name="telefono_celular" placeholder="Teléfono celular" value="{{ old('telefono_celular') ? old('telefono_celular') : $persona->telefono_celular_persona }}">
                      <span class="text-danger" >{{$errors->first('telefono_celular')}}</span>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">
                  <div class="text-right">
                    <input type="hidden" name="id" value="{{$persona->id_persona}}">
                    <button type="submit" class="btn btn-primary">Modificar</button>
                  </div>
                </div>
             </form>
          </div>

        </div>
    </div>
@endsection
