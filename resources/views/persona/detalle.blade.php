@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12">
             @if (session('mensaje_ok'))
             <div class="alert alert-success text-center">
                 {{ session('mensaje_ok') }}
             </div>
             @endif
          </div>
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('persona.listado')}}" class="btn btn-warning">Regresar</a>
          </div>
          <h2 class="text-center f-35">Información de la persona</h2>
          <br>
          <div class="col-md-7">
              <table class="table table-bordered table-hover">
                <tr>
                  <th width="150">Tipo de identificación:</th>
                  <td colspan="3">{{$persona->documento->nombre_tipo_identificacion}}</td>
                <tr>
                <tr>
                  <th width="150">Identificación:</th>
                  <td colspan="3">{{$persona->nacionalidad->nombre_nacionalidad}}-{{$persona->identificacion_persona}}</td>
                <tr>
                <tr>
                  <th width="160">Nombre del arbitro:</th>
                  <td colspan="3">{{$persona->nombre_persona}} {{$persona->apellido_persona}}</td>
                <tr>
                <tr>
                  <th width="150">Correo:</th>
                  <td colspan="3">{{$persona->correo_persona}}</td>
                <tr>
                <tr>
                  <th width="150">Telefono Local:</th>
                  <td colspan="3">{{$persona->telefono_local_persona}}</td>
                <tr>
                <tr>
                    <th width="150">Telefono Celular:</th>
                    <td colspan="3">{{$persona->telefono_celular_persona}}</td>
                <tr>

              </table>
          </div>

        </div>
    </div>
@endsection
