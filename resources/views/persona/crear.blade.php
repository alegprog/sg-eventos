@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('persona.listado')}}" class="btn btn-warning">Mostrar listado</a>
            <hr/>
          </div>
          <h2 class="text-center f-35">Agregar persona</h2>
          <div class="col-md-8 col-md-offset-2 p-b-50">
            <form method="POST" action="{{route('persona.guardar')}}">
                  {{ csrf_field() }}
                 <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Nombre</label>
                      <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{ old('nombre') }}">
                      <span class="text-danger" >{{$errors->first('nombre')}}</span>
                  </div>
                </div>

                  <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Apellido</label>
                      <input type="text" class="form-control" name="apellido" placeholder="Apellido" value="{{ old('apellido') }}">
                      <span class="text-danger" >{{$errors->first('apellido')}}</span>
                  </div>
                </div>
                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Tipo identificación</label>
                      {!!Form::select('tipo_identificacion_id', $tipo_identificaciones, null, ['placeholder' => 'Seleccione el tipo de identificación...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('tipo_identificacion_id')}}</span>
                  </div>
                </div>

                  <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Identificación</label>
                      <input type="text" class="form-control" name="identificacion" placeholder="Identificación" value="{{ old('identificacion') }}">
                      <span class="text-danger" >{{$errors->first('identificacion')}}</span>
                  </div>
                </div>
                <div class="clearfix"></div>

                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Nacionalidad</label>
                      {!!Form::select('nacionalidad_id', $nacionalidades, null, ['placeholder' => 'Seleccione la nacionalidad...', 'class'=>'form-control']) !!}
                      <span class="text-danger" >{{$errors->first('nacionalidad_id')}}</span>
                  </div>
                </div>

                  <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Correo</label>
                      <input type="text" class="form-control" name="correo" placeholder="Correo" value="{{ old('correo') }}">
                      <span class="text-danger" >{{$errors->first('correo')}}</span>
                  </div>
                </div>

                <div class="col-md-6">
                <div class="form-group p-t-0">
                      <label class="f-15">Teléfono local</label>
                      <input type="text" class="form-control" name="telefono_local" placeholder="Teléfono local" value="{{ old('telefono_local') }}">
                      <span class="text-danger" >{{$errors->first('telefono_local')}}</span>
                  </div>
                </div>


                <div class="col-md-6">
                  <div class="form-group p-t-0">
                      <label class="f-15">Teléfono celular</label>
                      <input type="text" class="form-control" name="telefono_celular" placeholder="Teléfono celular" value="{{ old('telefono_celular') }}">
                      <span class="text-danger" >{{$errors->first('telefono_celular')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12">
                  <div class="text-right">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                  </div>
                </div>

             </form>
          </div>

        </div>
    </div>
@endsection
