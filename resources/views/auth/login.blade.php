@extends('layouts.master_main')
@section('content')
    <div class="container">
        <div class="row">

            <h2 class="text-center f-35 p-l-r-60">Sistema de Gestión de Eventos de la <br>Universidad Jose Gregorio Hernández</h2>
            <hr>
            <div class="col-md-6 col-md-offset-3">

              @if (Session::has('errors'))
              <div class="alert alert-warning" role="alert">
                <strong>Ups! Algo salió mal : </strong>
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
              </div>
              @endif

              @if (Session::has('mensaje'))
              <div class="alert alert-warning" role="alert">
                <strong>Ups! Algo salió mal : </strong>
                <ul>
                      <li>{{ Session::get('mensaje') }}</li>
                </ul>
              </div>
              @endif

              @if (session('mensaje_ok'))
              <div class="alert alert-success text-center">
                  {{ session('mensaje_ok') }}
              </div>
              @endif

                <h2 class="text-center text-primary f-25">Inicio de Sesión</h2>
                <div class="panel panel-default">
                    <div class="panel-body">
                      <form method="POST" action="auth/login">
                            {{ csrf_field() }}
                            <br>
                            <div class="form-group">
                                <label class="f-15">Usuario</label>
                                <input type="text" class="form-control" autocomplete="false" name="usuario" placeholder="Usuario">
                            </div>
                            <div class="form-group">
                                <label class="f-15" >Contraseña</label>
                                <input type="password" class="form-control" autocomplete="false" name="clave" placeholder="Contraseña">
                            </div>
                            <div>
                              <button type="submit" class="btn btn-primary btn-block">Iniciar Sesión</button>
                            </div>
                            <hr>
                            <div>
                              <a href="{{route('registro.create')}}" class="btn btn-primary btn-block">Registrarme</a>
                              <input type="reset" value="Cancelar" class="btn btn-danger btn-block">
                            </div>
                     </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
