@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12">
             @if (session('mensaje_ok'))
             <div class="alert alert-success text-center">
                 {{ session('mensaje_ok') }}
             </div>
             @endif
         </div>
          <h2 class="text-center f-35">Nacionalidades</h2>
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('nacionalidad.crear')}}" class="btn btn-primary">Agregar</a>
          </div>
          <div class="col-md-12">
            <table id="example" class="table text-center table-bordered">
              <thead>
              <tr>
                <th class="text-center">Nombre</th>
                <th width="250" class="text-center">Acciones</th>
              </tr>
              </thead>
              <tbody>
              @foreach($nacionalidades as $nacionalidad)
              <tr>
                <td>{{$nacionalidad->nombre_nacionalidad}}</td>
                <td>
                  <a href="{{route('nacionalidad.editar',['id'=>$nacionalidad->id_nacionalidad])}}" class="btn btn-info">Editar</a>
                  @if($nacionalidad->deleted_at)
                  <a href="{{route('nacionalidad.activar',['id'=>$nacionalidad->id_nacionalidad])}}" class="btn btn-danger">Bloqueado</a>
                  @else
                  <a href="{{route('nacionalidad.eliminar',['id'=>$nacionalidad->id_nacionalidad])}}" class="btn btn-success">&nbsp;&nbsp;&nbsp;&nbsp;Activo&nbsp;&nbsp;&nbsp;&nbsp;</a>
                  @endif
                </td>
              </tr>
              @endforeach
              </tbody>
            </table>
          </div>
          <div class="p-b-30" >
          </div>
        </div>
    </div>
@endsection

@section('script')
 <script>
  $(document).ready(function() {
    //$('#example').DataTable();
    t=$('#example').DataTable({
       processing: true,
       serverSide: false,
       language: {
                       processing:     "Procesando ...",
                       search:         '<span class="glyphicon glyphicon-search"></span>',
                       searchPlaceholder: "BUSCAR",
                       lengthMenu:     "Mostrar _MENU_ Registros",
                       info:           "Mostrando _START_ a _END_ de _TOTAL_ Registros",
                       infoEmpty:      "Mostrando 0 a 0 de 0 Registros",
                       infoFiltered:   "(filtrada de _MAX_ registros en total)",
                       infoPostFix:    "",
                       loadingRecords: "...",
                       zeroRecords:    "No se encontraron registros coincidentes",
                       emptyTable:     "No hay datos disponibles en la tabla",
                       paginate: {
                           first:      "Primero",
                           previous:   "Anterior",
                           next:       "Siguiente",
                           last:       "Ultimo"
                       },
                       aria: {
                           sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
                           sortDescending: ": habilitado para ordenar la columna en orden descendente"
                       }
                   }
       });
  } );
 </script>
@endsection
