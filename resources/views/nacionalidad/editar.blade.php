@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">
          <div class="col-md-12 text-right p-t-b-10">
            <a href="{{route('nacionalidad.listado')}}" class="btn btn-warning">Mostrar listado</a>
            <hr/>
          </div>
          <h2 class="text-center f-35">Editar nacionalidad</h2>
          <div class="col-md-6 col-md-offset-3 p-b-50">
            <form method="POST" action="{{route('nacionalidad.actualizar',['id'=>$nacionalidad->id_nacionalidad])}}">
                  {{ csrf_field() }}
                  <div class="form-group p-t-20">
                      <label class="f-15">Nombre</label>
                      <input type="text" class="form-control" name="nombre" placeholder="Nombre de la nacionalidad" value="{{ old('nombre') ? old('nombre') : $nacionalidad->nombre_nacionalidad }}">
                      <span class="text-danger" >{{$errors->first('nombre')}}</span>
                  </div>
                  <div class="text-right">
                    <input type="hidden" name="id" value="{{$nacionalidad->id_nacionalidad}}">
                    <button type="submit" class="btn btn-primary">Modificar</button>
                  </div>
             </form>
          </div>

        </div>
    </div>
@endsection
