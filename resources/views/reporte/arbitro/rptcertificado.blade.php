@extends('layouts.master_rpt')
@section('content')
<div class="container">
  <p><br></p>
  <h1 class="text-center f-50 p-t-0">Constancia</h1>
  <p><br><br></p>
  <p class="text-justify f-25">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Quien suscribe, {{$arbitro->evento->dependencia->firma_1_antes}} {{$arbitro->evento->dependencia->firma_principal->nombre_persona}} {{$arbitro->evento->dependencia->firma_principal->apellido_persona}}, Cedula de Identidad No. V- {{$arbitro->evento->dependencia->firma_principal->identificacion_persona}}, {{$arbitro->evento->dependencia->firma_1_cargo}}, por medio de la presente hace constar que el (la) Prof. (a):</p>
  <p><br><br></p>
  <p class="text-center f-30"><b>{{$arbitro->persona->apellido_persona}}, {{$arbitro->persona->nombre_persona}}</b></p>
  <p class="text-center f-25">{{$arbitro->persona->documento->nombre_tipo_identificacion}} No. {{$arbitro->persona->nacionalidad->nombre_nacionalidad}}-{{$arbitro->persona->identificacion_persona}}</p>
  <p><br><br></p>
  <p class="text-justify f-25">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Participó en calidad de <b>{{$arbitro->modalidad->nombre_modalidad}} en las {{$arbitro->evento->nombre_evento}}</b>, efectuada el día {{$date->format('j')}} de {{$date->format('F')}} de {{$date->format('Y')}}.</p>
  <p><br><br></p>
  <p class="text-justify f-25">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Constancia que se expide en Maracaibo - Venezuela, {{$date->format('j')}} de {{$date->format('F')}} de {{$date->format('Y')}}</p>
  <p><br><br><br><br><br><br><br></p>
  <b>
    <p class="text-center f-25">{{$arbitro->evento->dependencia->firma_1_antes}} {{$arbitro->evento->dependencia->firma_principal->nombre_persona}} {{$arbitro->evento->dependencia->firma_principal->apellido_persona}} {{$arbitro->evento->dependencia->firma_1_despues}}</p>
    <p class="text-center f-25">{{$arbitro->evento->dependencia->firma_1_cargo}}</p>
  </b>
  <p><br><br><br><br></p>
  <b>
    <p class="text-center f-25">{{$arbitro->evento->dependencia->firma_2_antes}} {{$arbitro->evento->dependencia->firma_segundaria->nombre_persona}} {{$arbitro->evento->dependencia->firma_segundaria->apellido_persona}} {{$arbitro->evento->dependencia->firma_2_despues}}</p>
    <p class="text-center f-25">{{$arbitro->evento->dependencia->firma_2_cargo}}</p>
  </b>
<div>
@endsection
