@extends('layouts.master_dashboard')
@section('content')
  <div class="container">
    <div class="col-md-12">

      <a id="boton" class="btn btn-success pull-right" href="javascript:imprSelec('muestra')">
        <i class="glyphicon glyphicon-print"></i> Imprimir
      </a>

      <a id="boton" style="margin-right:5px;" target="_blank" class="btn btn-success pull-right" href="{{route('reporte.pdfReporte',['tipo'=>'requerimiento','id'=>$evento->id_evento])}}">
        <i class="glyphicon glyphicon-pdf"></i> PDF
      </a>

      <br>
      <div id="muestra">
      <h3 style="text-align:center;" class="text-center">Listado de Requerimiento <br>Evento ({{$evento->nombre_evento}})</h3>
      <table style="text-align:center;border: 1px solid #ddd;width: 100%;max-width: 100%;margin-bottom: 20px;border-spacing: 0;border-collapse: collapse;" id="example" class="table text-center table-bordered">
        <thead>
        <tr>
          <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Tipo de requerimiento</th>
          <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Descripcion</th>
          <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Cantidad</th>
          <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Responsable</th>
        </tr>
        <thead>
        <tbody>
        @foreach($requerimientos as $requerimiento)
        <tr>
          <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$requerimiento->tipo_requerimiento->nombre_tipo_requerimiento}}</td>
          <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$requerimiento->descripcion_requerimiento}}</td>
          <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$requerimiento->cantidad_requerimiento}}</td>
          <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$requerimiento->responsable->nombre_responsable}}</td>
        </tr>
        @endforeach
       </tbody>
      </table>
    </div>
    </div>
  </div>
@endsection
