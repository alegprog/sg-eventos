<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Reporte</title>
    <style>
      body{
        font-family: sans-serif;
        font-size: 12px;
      }
      .f-50{
        font-size: 50px !important;
      }
      .f-35{
        font-size: 35px !important;
      }
      .f-20{
        font-size: 20px !important;
      }
      .f-25{
        font-size: 25px !important;
      }
      .f-30{
        font-size: 30px !important;
      }
      img{
        text-align: center;
      }
      h1{
        text-align: center;
        padding: 2px !important;
        margin: 0px !important;
      }
      p{
        text-align: center;
        padding: 2px !important;
        margin: 0px !important;
      }
    </style>

  </head>
  <body>

<div style="text-align:center;">
  <h3 style="text-align:center;" class="text-center">Listado de {{$titulo}} <br>Evento ({{$evento->nombre_evento}})</h3>

  @if($tipo=='requerimiento')
  <table style="text-align:center;border: 1px solid #ddd;width: 100%;max-width: 100%;margin-bottom: 20px;border-spacing: 0;border-collapse: collapse;" id="example" class="table text-center table-bordered">
    <thead>
    <tr>
      <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Tipo de requerimiento</th>
      <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Descripcion</th>
      <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Cantidad</th>
      <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Responsable</th>
    </tr>
    <thead>
    <tbody>
    @foreach($consultas as $requerimiento)
    <tr>
      <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$requerimiento->tipo_requerimiento->nombre_tipo_requerimiento}}</td>
      <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$requerimiento->descripcion_requerimiento}}</td>
      <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$requerimiento->cantidad_requerimiento}}</td>
      <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$requerimiento->responsable->nombre_responsable}}</td>
    </tr>
    @endforeach
   </tbody>
  </table>
  @endif

  @if($tipo=='arbitraje')
  <table style="text-align:center;border: 1px solid #ddd;width: 100%;max-width: 100%;margin-bottom: 20px;border-spacing: 0;border-collapse: collapse;" id="example" class="table text-center table-bordered">
    <thead>
    <tr>
      <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Articulo</th>
      <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Estatu</th>
      <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Identifiación</th>
      <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Nombre</th>
    </tr>
    <thead>
    <tbody>
    @foreach($consultas as $arbitraje)
    <tr>
      <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$arbitraje->articulo->codigo_articulo}} - {{$arbitraje->articulo->nombre_articulo}}</td>
      <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$arbitraje->estatu->nombre_estatu}}</td>
      <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$arbitraje->arbitro->persona->nacionalidad->nombre_nacionalidad}} - {{$arbitraje->arbitro->persona->identificacion_persona}}</td>
      <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$arbitraje->arbitro->persona->nombre_persona}} {{$arbitraje->arbitro->persona->apellido_persona}}</td>
    </tr>
    @endforeach
   </tbody>
  </table>
  @endif

  @if($tipo=='invitado')
  <table style="text-align:center;border: 1px solid #ddd;width: 100%;max-width: 100%;margin-bottom: 20px;border-spacing: 0;border-collapse: collapse;" id="example" class="table text-center table-bordered">
    <thead>
    <tr>
      <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Identificacion</th>
      <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Nombre</th>
      <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Titulo de ponencia</th>
      <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Tipo</th>
      <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Modalidad</th>
    </tr>
    <thead>
    <tbody>
    @foreach($consultas as $invitado)
    <tr>
      <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$invitado->persona->nacionalidad->nombre_nacionalidad}} - {{$invitado->persona->identificacion_persona}}</td>
      <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$invitado->persona->nombre_persona}} {{$invitado->persona->apellido_persona}}</td>
      <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$invitado->titulo_ponencia_invitado}}</td>
      <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$invitado->tipo_invitado->nombre_tipo_invitado}}</td>
      <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$invitado->modalidad->nombre_modalidad}}</td>
    </tr>
    @endforeach
   </tbody>
  </table>
  @endif

  @if($tipo=='arbitro')
  <table style="text-align:center;border: 1px solid #ddd;width: 100%;max-width: 100%;margin-bottom: 20px;border-spacing: 0;border-collapse: collapse;" id="example" class="table text-center table-bordered">
    <thead>
    <tr>
      <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Identificacion</th>
      <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Nombre</th>
      <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Tipo</th>
      <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Modalidad</th>
    </tr>
    <thead>
    <tbody>
    @foreach($consultas as $arbitro)
    <tr>
      <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;" >{{$arbitro->persona->nacionalidad->nombre_nacionalidad}} - {{$arbitro->persona->identificacion_persona}}</td>
      <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$arbitro->persona->nombre_persona}} {{$arbitro->persona->apellido_persona}}</td>
      <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$arbitro->tipo_arbitro->nombre_tipo_arbitro}}</td>
      <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$arbitro->modalidad->nombre_modalidad}}</td>
    </tr>
    @endforeach
   </tbody>
  </table>
  @endif

<div>
</body>
</html>
