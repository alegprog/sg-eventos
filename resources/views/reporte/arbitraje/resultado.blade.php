@extends('layouts.master_dashboard')
@section('content')
  <div class="container">
    <div class="col-md-12">
      <a id="boton" class="btn btn-success pull-right" href="javascript:imprSelec('muestra')">
        <i class="glyphicon glyphicon-print"></i> Imprimir
      </a>
      <a id="boton" style="margin-right:5px;" target="_blank" class="btn btn-success pull-right" href="{{route('reporte.pdfReporte',['tipo'=>'arbitraje','id'=>$evento->id_evento])}}">
        <i class="glyphicon glyphicon-pdf"></i> PDF
      </a>
      <br>
      <div id="muestra">
      <h3 class="text-center" style="text-align:center;">Listado de Arbitraje <br>Evento ({{$evento->nombre_evento}})</h3>
      <table style="text-align:center;border: 1px solid #ddd;width: 100%;max-width: 100%;margin-bottom: 20px;border-spacing: 0;border-collapse: collapse;" id="example" class="table text-center table-bordered">
        <thead>
        <tr>
          <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Articulo</th>
          <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Estatu</th>
          <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Identifiación</th>
          <th style="border-bottom-width: 2px;border: 1px solid #ddd;padding: 8px;line-height: 1.42857143;font-weight: bold;" class="text-center">Nombre</th>
        </tr>
        <thead>
        <tbody>
        @foreach($arbitrajes as $arbitraje)
        <tr>
          <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$arbitraje->articulo->codigo_articulo}} - {{$arbitraje->articulo->nombre_articulo}}</td>
          <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$arbitraje->estatu->nombre_estatu}}</td>
          <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$arbitraje->arbitro->persona->nacionalidad->nombre_nacionalidad}} - {{$arbitraje->arbitro->persona->identificacion_persona}}</td>
          <td style="padding-left:5px;border: 1px solid #ddd;line-height: 1.42857143;">{{$arbitraje->arbitro->persona->nombre_persona}} {{$arbitraje->arbitro->persona->apellido_persona}}</td>
        </tr>
        @endforeach
       </tbody>
      </table>
     </div>
    </div>
  </div>
@endsection
