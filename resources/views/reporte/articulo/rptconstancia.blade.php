@extends('layouts.master_rpt')
@section('content')
<div class="container">
  <p><br></p>
  <h1 class="text-center f-50 p-t-0">Constancia</h1>
  <p><br><br></p>
  <p class="text-justify f-25">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Quien suscribe hace constar que el trabajo titulado
    <b>{{$articulo->nombre_articulo}}</b>.,
    identificado con el código <b>{{$articulo->codigo_articulo}}</b>,
    presentado por <b>{{$articulo->persona->nombre_persona}} {{$articulo->persona->apellido_persona}},
    @if($articulo->coautor_principal){{$articulo->coautor_principal->nombre_persona}} {{$articulo->coautor_principal->apellido_persona}},@endif
    @if($articulo->coautor_segundario){{$articulo->coautor_segundario->nombre_persona}} {{$articulo->coautor_segundario->apellido_persona}},@endif
    </b>
    bajo la modalidad de ponencia Oral en las
    {{$articulo->evento->nombre_evento}},
    fue arbitrado por especialistas en el área.</p>
  <p><br><br></p>
  <p class="text-justify f-25">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cabe destacar que dicho evento se realizó el día {{$date->format('j')}} de {{$date->format('F')}} de {{$date->format('Y')}}.</p>
  <p><br><br></p>
  <p class="text-justify f-25">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Constancia que se expide en Maracaibo - Venezuela, {{$date->format('j')}} de {{$date->format('F')}} de {{$date->format('Y')}}</p>
  <p><br><br><br><br><br><br><br></p>
  <b>
    <p class="text-center f-25">{{$articulo->evento->dependencia->firma_1_antes}} {{$articulo->evento->dependencia->firma_principal->nombre_persona}} {{$articulo->evento->dependencia->firma_principal->apellido_persona}} {{$articulo->evento->dependencia->firma_1_despues}}</p>
    <p class="text-center f-25">{{$articulo->evento->dependencia->firma_1_cargo}}</p>
  </b>
  <p><br><br><br><br></p>
  <b>
    <p class="text-center f-25">{{$articulo->evento->dependencia->firma_2_antes}} {{$articulo->evento->dependencia->firma_segundaria->nombre_persona}} {{$articulo->evento->dependencia->firma_segundaria->apellido_persona}} {{$articulo->evento->dependencia->firma_2_despues}}</p>
    <p class="text-center f-25">{{$articulo->evento->dependencia->firma_2_cargo}}</p>
  </b>
<div>
@endsection
