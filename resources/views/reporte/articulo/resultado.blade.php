@extends('layouts.master_dashboard')
@section('content')
  <div class="container">
    <div class="col-md-12">
      <h3 class="text-center">Listado de articulos <br>Evento ({{$evento->nombre_evento}})</h3>
      <table id="example" class="table text-center table-bordered">
        <thead>
          <tr>
            <th class="text-center">Codigo</th>
            <th class="text-center">Articulo</th>
            <th width="250" class="text-center">Acciones</th>
          </tr>
          </thead>
          <tbody>
          @foreach($articulos as $articulo)
          <tr>
            <td>{{$articulo->codigo_articulo}}</td>
            <td>{{$articulo->nombre_articulo}}</td>
          <td>
            <!-- Single button -->
            <div class="btn-group">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Opciones <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                <li><a target="_blank" href="{{route('reporte.constanciaArticulo',['id'=>$articulo->id_articulo])}}">Constancia</a></li>
              </ul>
            </div>
          </td>
        </tr>
        @endforeach
       </tbody>
      </table>
    </div>
  </div>
@endsection

@section('script')
 <script>
  $(document).ready(function() {
    //$('#example').DataTable();
    t=$('#example').DataTable({
       processing: true,
       serverSide: false,
       language: {
                       processing:     "Procesando ...",
                       search:         '<span class="glyphicon glyphicon-search"></span>',
                       searchPlaceholder: "BUSCAR",
                       lengthMenu:     "Mostrar _MENU_ Registros",
                       info:           "Mostrando _START_ a _END_ de _TOTAL_ Registros",
                       infoEmpty:      "Mostrando 0 a 0 de 0 Registros",
                       infoFiltered:   "(filtrada de _MAX_ registros en total)",
                       infoPostFix:    "",
                       loadingRecords: "...",
                       zeroRecords:    "No se encontraron registros coincidentes",
                       emptyTable:     "No hay datos disponibles en la tabla",
                       paginate: {
                           first:      "Primero",
                           previous:   "Anterior",
                           next:       "Siguiente",
                           last:       "Ultimo"
                       },
                       aria: {
                           sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
                           sortDescending: ": habilitado para ordenar la columna en orden descendente"
                       }
                   }
       });
  } );
 </script>
@endsection
