@extends('layouts.master_dashboard')
@section('content')
    <div class="container">
        <div class="row">

          <h2 class="text-center f-35">Buscar Invitado</h2>
          <div class="col-md-8 col-md-offset-2 p-b-50">
            <form method="POST" action="{{route('reporte.rpt_invitado')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                  {{ csrf_field() }}

                <div class="clearfix"></div>

                <div class="col-md-12">
                  <div class="form-group p-t-0">
                      <label class="f-15">Evento</label>
                      {!!Form::select('evento_id', $eventos, null, ['placeholder' => 'Seleccione el evento...', 'class'=>'form-control select-basic']) !!}
                      <span class="text-danger" >{{$errors->first('evento_id')}}</span>
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12">
                  <div class="text-right">
                    <button type="submit" class="btn btn-warning">Buscar</button>
                  </div>
                </div>

             </form>
          </div>

        </div>
    </div>
@endsection

@section('script')
  <script type="text/javascript">
    $('.select-basic').select2();
  </script>
@endsection
