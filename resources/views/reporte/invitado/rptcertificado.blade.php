@extends('layouts.master_rpt')
@section('content')
<div class="container">
  <h1 class="text-center f-50 p-t-0">Certificado</h1>
  <br><br><br>
  <p class="text-center f-25">Que se otorga a:</p>
  <h1 class="text-center">{{$invitado->persona->apellido_persona}}, {{$invitado->persona->nombre_persona}}</h1>
  <p class="text-center f-25">Con {{$invitado->persona->documento->nombre_tipo_identificacion}}: {{$invitado->persona->nacionalidad->nombre_nacionalidad}}-{{$invitado->persona->identificacion_persona}}</p>
  <br><br>
  <p class="text-center f-25">Por haber presentado la ponencia titulada:</p>
  <p class="text-center f-30"><b>“{{$invitado->titulo_ponencia_invitado}}”</b>, en el evento titulado:</p>
  <div class="col-md-12">
    <br><br>
  </div>
  <p class="text-center f-50"><b>{{$invitado->evento->nombre_evento}}</b></p>
  <div class="col-md-12">
    <br><br>
  </div>
  <p class="text-center f-30">Tipo de evento: {{$invitado->evento->tipo_evento->nombre_tipo_evento}}</p>
  <p class="text-center f-25">Maracaibo - Venezuela, {{$invitado->evento->fecha_inicio->format('j')}} de {{$invitado->evento->fecha_inicio->format('F')}} de {{$invitado->evento->fecha_inicio->format('Y')}}</p>
  <div class="col-md-12">
    <br><br><br><br><br><br><br><br>
  </div>
  <div class="f-20" style="width:40%;display:inline-block;">
   <p class="text-center">{{$invitado->evento->dependencia->firma_1_antes}} {{$invitado->evento->dependencia->firma_principal->nombre_persona}} {{$invitado->evento->dependencia->firma_principal->apellido_persona}} {{$invitado->evento->dependencia->firma_1_despues}}</p>
   <p class="text-center">{{$invitado->evento->dependencia->firma_1_cargo}}</p>
  </div>
  <div style="width:18%;display:inline-block;">
    &nbsp;
  </div>
  <div class="f-20" style="width:40%;display:inline-block;">
    <p class="text-center">{{$invitado->evento->dependencia->firma_2_antes}} {{$invitado->evento->dependencia->firma_segundaria->nombre_persona}} {{$invitado->evento->dependencia->firma_segundaria->apellido_persona}} {{$invitado->evento->dependencia->firma_2_despues}}</p>
    <p class="text-center">{{$invitado->evento->dependencia->firma_2_cargo}}</p>
  </div>
<div>
@endsection
