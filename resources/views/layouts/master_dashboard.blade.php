<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>SG-Eventos</title>

    <!-- Bootstrap -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/bootstrap.min.css')}}" media="print" rel="stylesheet">
    {{--<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">--}}
    <link href="{{asset('assets/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/select2/select2.min.css')}}" rel="stylesheet">
    {{--<link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('assets/css/datatable/datatables.min.css')}}" rel="stylesheet">--}}
    <link href="{{asset('assets/css/datatable/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    @yield('style')
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <!-- Latest compiled and minified CSS -->
    <!--<link rel="stylesheet"
    href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
    integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
    crossorigin="anonymous">-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container-fluid">
      <div class="col-xs-6">
        <img src="{{asset('assets/image/logo_ujgh.gif')}}" width="300" >
      </div>

      <div class="col-xs-6">
        <img class="pull-right p-t-20" src="{{asset('assets/image/logo.png')}}" width="250" >
      </div>
    </div>
    <ul class="nav nav-pills p-t-20 p-l-50 bold">
      <li><!--<img class="p-l-r-0" src="{{asset('assets/image/logo.png')}}" width="170" >--></li>
      <li role="presentation" title="Ir a inicio"><a href="{{route('dashboard')}}">Inicio</a></li>
      <li role="presentation"><a href="{{route('evento.listado')}}" title="Ver listado de evento">Eventos</a></li>
      <li role="presentation"><a href="{{route('invitado.listado')}}" title="Ver listado de invitado">Invitados</a></li>
      <li role="presentation"><a href="{{route('requerimiento.listado')}}" title="Ver listado de requisito">Requerimiento</a></li>
      <li role="presentation"><a href="{{route('arbitro.listado')}}" title="Ver listado de arbitro">Arbitros</a></li>
      <li role="presentation"><a href="{{route('articulo.listado')}}" title="Ver listado de articulo">Articulos</a></li>
      <li role="presentation"><a href="{{route('arbitraje.listado')}}" title="Ver listado de arbitraje">Arbitrajes</a></li>
      <li role="presentation"><a href="{{route('persona.listado')}}" title="Ver listado de persona">Persona</a></li>
      <li role="presentation" class="dropdown" title="Listado de reporte">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
          Reporte <span class="caret"></span> </a>
        <ul class="dropdown-menu">
          <li><a href="{{route('reporte.invitado')}}">Invitado</a></li>
          <li><a href="{{route('reporte.arbitro')}}">Arbitro</a></li>
          <li><a href="{{route('reporte.requerimiento')}}">Requerimiento</a></li>
          <li><a href="{{route('reporte.arbitraje')}}">Arbitraje</a></li>
          <li><a href="{{route('reporte.articulo.constancia')}}">Constancia publicación</a></li>
          <li><a href="{{route('reporte.invitado.constancia')}}">Constancia invitado</a></li>
          <li><a href="{{route('reporte.arbitro.constancia')}}">Constancia arbitro</a></li>
          <!--<li><a href="#">Another action</a></li>
          <li><a href="#">Something else here</a></li>
          <li role="separator" class="divider"></li>
          <li><a href="#">Separated link</a></li>-->
        </ul>
      </li>
      @if (Auth::check())
         @if (Auth::user()->tipo_usuario=='admin')
          <li role="presentation" class="dropdown" title="Opciones administrativa">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
              Administrador <span class="caret"></span>  </a>
            <ul class="dropdown-menu">
              <li><a href="{{route('tipo_evento.listado')}}">Tipo Evento</a></li>
              <li><a href="{{route('tipo_invitado.listado')}}">Tipo Invitado</a></li>
              <li><a href="{{route('tipo_arbitro.listado')}}">Tipo Arbitro</a></li>
              <li><a href="{{route('tipo_requerimiento.listado')}}">Tipo Requerimiento</a></li>
              <li><a href="{{route('tipo_identificacion.listado')}}">Tipo Identificación</a></li>
              <li><a href="{{route('responsable.listado')}}">Responsable</a></li>
              <li><a href="{{route('nacionalidad.listado')}}">Nacionalidad</a></li>
              <li><a href="{{route('modalidad.listado')}}">Modalidad</a></li>
              <li><a href="{{route('dependencia.listado')}}">Dependencia</a></li>
              <li><a href="{{route('lugar.listado')}}">Lugar</a></li>
              <li><a href="{{route('estatu.listado')}}">Estatus</a></li>
              <li><a href="{{route('usuario.listado')}}">Usuario</a></li>
              {{--<li><a href="{{route('usuario.listado')}}">Firma</a></li>--}}
            </ul>
          </li>
        @endif
      @endif
      <li role="presentation" class="dropdown" title="Opciones de perfil">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
          Perfil <span class="caret"></span> </a>
        <ul class="dropdown-menu">
          <li><a href="{{route('dashboard.ajuste')}}">Ajuste</a></li>
          <li><a href="{{route('dashboard.cambio_clave')}}">Cambio de clave</a></li>
          <li role="separator" class="divider"></li>
          <li><a href="{{route('auth/logout')}}">Cerrar sessión</a></li>
        </ul>
      </li>
      <!--<li><img class="p-l-r-0" src="{{asset('assets/image/logo_ujgh.gif')}}" width="150" ></li>-->
    </ul>

    <div class="franja-azul">

    </div>

    @yield('content')
    <div>
      <hr>
      <div class="col-md-6 col-md-offset-3">
        <p class="text-center bold f-20">
          <marquee style="color:#0E284E; text-shadow:5px 5px 5px #0E284E;">Realizado por William Morantes y Luis Viloria</marquee>
        </p>
      </div>
      <div class="col-md-12">
        <p class="text-center bold f-15">© 2016 Universidad Dr. José Gregorio Hernández, Todos Los Derechos Reservados.</p>
      </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{asset('assets/js/jquery/jquery.min.js')}}" ></script>
    {{--<script src="//code.jquery.com/jquery-1.12.3.js" ></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" ></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js" ></script>--}}
    <script src="{{asset('assets/js/moment.min.js')}}" ></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}" ></script>
    <script src="{{asset('assets/js/bootstrap-datepicker.js')}}" ></script>
    <script src="{{asset('assets/js/bootstrap-datetimepicker.js')}}" ></script>
    <script src="{{asset('assets/js/es.js')}}" ></script>
    <script src="{{asset('assets/js/select2/select2.min.js')}}" ></script>
    <script src="{{asset('assets/js/datatable/jquery.dataTables.min.js')}}" ></script>
    <script src="{{asset('assets/js/datatable/dataTables.bootstrap.min.js')}}" ></script>


    @yield('script')
    <script>
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
    function imprSelec(muestra)
    {

      //print();
      var ficha=document.getElementById(muestra);
      var ventimp=window.open(' ','popimpr');
      ventimp.document.write(ficha.innerHTML);
      ventimp.document.close();
      ventimp.print();
      ventimp.close();
    }
    </script>
    <!-- Latest compiled and minified JavaScript -->
    <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
     crossorigin="anonymous"></script>-->
  </body>
  </html>
